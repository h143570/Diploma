package org.gabs.statics.solver.resultbuilder.postprocessor;

import org.gabs.statics.model.result.Reaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RoundingPostProcessorTest {

    private static final int PRECISION = 14;
    private static final String ELEMENT_1 = "element 1";
    private static final String ELEMENT_2 = "element 2";

    @InjectMocks private RoundingPostProcessor postProcessor;
    private Map<String, Collection<Reaction>> reactions;
    @Mock private RoundingStrategy roundingStrategy;
    @Mock private Reaction reaction1;
    @Mock private Reaction reaction2;
    @Mock private Reaction roundedReaction1;
    @Mock private Reaction roundedReaction2;

    @Test
    public void testPostProcessShouldRoundEveryReaction() {
        whenCalledWithReactionMapping();
        thenEveryReactionShouldBeRounded();
    }

    private void whenCalledWithReactionMapping() {
        reactions = new HashMap<>(2, 1);
        reactions.put(ELEMENT_1, Arrays.asList(reaction1, reaction2));
        reactions.put(ELEMENT_2, Arrays.asList(reaction2));
        when(roundingStrategy.round(reaction1, PRECISION)).thenReturn(roundedReaction1);
        when(roundingStrategy.round(reaction2, PRECISION)).thenReturn(roundedReaction2);
        postProcessor.postProcess(reactions, PRECISION);
    }

    private void thenEveryReactionShouldBeRounded() {
        assertThat(reactions.keySet(), containsInAnyOrder(ELEMENT_1, ELEMENT_2));
        assertThat(reactions.get(ELEMENT_1), containsInAnyOrder(roundedReaction1, roundedReaction2));
        assertThat(reactions.get(ELEMENT_2), contains(roundedReaction2));
    }

}
