package org.gabs.statics.solver;

import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.problem.*;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.model.result.Ground;
import org.gabs.statics.model.result.Reaction;
import org.gabs.statics.model.result.Result;
import org.gabs.statics.model.result.StaticalDeterminacy;
import org.junit.Test;

import java.util.Collection;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class ProblemSolverImplCantileverTest {

    private ProblemSolver solver;
    private Joint fixedJoint;
    private Beam beam;
    private Result result;

    @Test
    public void testSolveCantilever() {
        givenProblemSolver();
        whenCalledWithCantilever();
        thenResultHasCorrectReactions();
        thenResultHasDeterminateStructuralDeterminacy();
        thenResultHasDeterminateProblemDeterminacy();
        thenResultHasNullError();
    }

    private void givenProblemSolver() {
        solver = new ProblemSolverFactoryImpl().create();
    }

    private void whenCalledWithCantilever() {
        Problem problem = createCantilever();
        result = solver.solve(problem);
    }

    private Problem createCantilever() {
        fixedJoint = createFixedJoint();
        beam = createBeam();
        return new Problem.Builder().addBeam(fixedJoint, beam).build();
    }

    private Joint createFixedJoint() {
        Constraints constraints = new Constraints.Builder().constrainTx().constrainTy().constrainRz().build();
        Constraints support = new Constraints.Builder().constrainTx().constrainTy().constrainRz().build();
        return new Joint.Builder("fixed joint", new Vector(0, 0, 0), constraints).withSupport(support).build();
    }

    private Beam createBeam() {
        ConcentratedForce concentratedForce = new ConcentratedForce(new Vector(2, 2, 0), new Vector(-2, 2, 0));
        Moment moment = new Moment(new Vector(0, 0, 5));
        return new Beam.Builder("beam").addConcentratedForce(concentratedForce).addMoment(moment).build();
    }

    private void thenResultHasCorrectReactions() {
        Reaction expectedFixedSupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(2, -2, 0)),
                new Moment(new Vector(0, 0, -13.0)));
        Reaction expectedFixedFromBeamReaction = new Reaction(beam.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(-2, 2, 0)),
                new Moment(new Vector(0, 0, 13.0)));
        Reaction expectedBeamFromFixedReaction = new Reaction(fixedJoint.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(2, -2, 0)),
                new Moment(new Vector(0, 0, -13.0)));

        Map<String, Collection<Reaction>> reactions = result.getReactions();
        assertThat(reactions.keySet(), containsInAnyOrder(fixedJoint.getId(), beam.getId()));
        assertThat(reactions.get(fixedJoint.getId()), containsInAnyOrder(expectedFixedSupportReaction, expectedFixedFromBeamReaction));
        assertThat(reactions.get(beam.getId()), containsInAnyOrder(expectedBeamFromFixedReaction));
    }

    private void thenResultHasDeterminateStructuralDeterminacy() {
        assertThat(result.getStructureDeterminacy(), equalTo(StaticalDeterminacy.DETERMINATE));
    }

    private void thenResultHasDeterminateProblemDeterminacy() {
        assertThat(result.getProblemDeterminacy(), equalTo(StaticalDeterminacy.DETERMINATE));
    }

    private void thenResultHasNullError() {
        assertThat(result.getError(), nullValue());
    }

}
