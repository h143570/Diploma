package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.solver.model.Effect;
import org.gabs.statics.solver.model.Element;
import org.gabs.statics.solver.model.Problem;
import org.gabs.statics.solver.model.Structure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProblemConverterTest {

    @InjectMocks private ProblemConverter converter;
    @Mock private ElementMappingBuilder elementMappingBuilder;
    @Mock private LoadMappingBuilder loadMappingBuilder;
    @Mock private StructureBuilder structureBuilder;
    @Mock private org.gabs.statics.model.problem.Problem problem;
    @Mock private Map<org.gabs.statics.model.problem.Element, Element> elementMapping;
    @Mock private Map<Element, Collection<Effect>> load;
    @Mock private Structure structure;
    private Problem result;

    @Test
    public void testConvertShouldHaveCorrectResult() {
        whenCalledWithProblem();
        thenResultShouldHaveLoad();
        thenResultShouldHaveStructure();
    }

    private void whenCalledWithProblem() {
        when(elementMappingBuilder.build(problem)).thenReturn(elementMapping);
        when(loadMappingBuilder.build(problem, elementMapping)).thenReturn(load);
        when(structureBuilder.build(problem, elementMapping)).thenReturn(structure);
        result = converter.convert(problem);
    }

    private void thenResultShouldHaveLoad() {
        assertThat(result.getLoad(), equalTo(load));
    }

    private void thenResultShouldHaveStructure() {
        assertThat(result.getStructure(), equalTo(structure));
    }

}
