package org.gabs.statics.solver.matrix.builder.processor.constraint;

import org.gabs.statics.solver.data.Vector;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.ReferenceFrame;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Map;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConstraintProcessorImplTest {

    private static final boolean JOINT = true;
    private static final int INDEX = 2;
    private static final ReferenceFrame LOCAL_REFERENCE_FRAME = new ReferenceFrame(new Vector(0, -1, 0), new Vector(1, 0, 0), new Vector(0, 0, 1));

    private ConstraintProcessorImpl processor;
    @Mock private ConstraintProcessorStrategy globalConstraintProcessorStrategy1;
    @Mock private ConstraintProcessorStrategy globalConstraintProcessorStrategy2;
    @Mock private ConstraintProcessorStrategy localConstraintProcessorStrategy1;
    @Mock private ConstraintProcessorStrategy localConstraintProcessorStrategy2;
    @Mock private Constraint constraint;
    @Mock private Map<Dof, double[]> coefficients;

    @Test
    public void testProcessWithGlobalConstraintShouldCallProcessOnGlobalStrategies() {
        givenConstraintProcessorImpl();
        whenCalledWithGlobalConstraint();
        thenProcessorShouldCallProcessOnGlobalStrategies();
    }

    @Test
    public void testProcessWithGlobalConstraintShouldNotCallProcessOnLocalStrategies() {
        givenConstraintProcessorImpl();
        whenCalledWithGlobalConstraint();
        thenProcessorShouldNotCallProcessOnLocalStrategies();
    }

    @Test
    public void testProcessWithLocalConstraintShouldCallProcessOnLocalStrategies() {
        givenConstraintProcessorImpl();
        whenCalledWithLocalConstraint();
        thenProcessorShouldCallProcessOnLocalStrategies();
    }

    @Test
    public void testProcessWithLocalConstraintShouldNotCallProcessOnGlobalStrategies() {
        givenConstraintProcessorImpl();
        whenCalledWithLocalConstraint();
        thenProcessorShouldNotCallProcessOnGlobalStrategies();
    }

    private void givenConstraintProcessorImpl() {
        processor = new ConstraintProcessorImpl();
        processor.setGlobalConstraintProcessorStrategies(Arrays.asList(globalConstraintProcessorStrategy1, globalConstraintProcessorStrategy2));
        processor.setLocalConstraintProcessorStrategies(Arrays.asList(localConstraintProcessorStrategy1, localConstraintProcessorStrategy2));
    }

    private void whenCalledWithGlobalConstraint() {
        when(constraint.getLocalReferenceFrame()).thenReturn(ReferenceFrame.GLOBAL);
        processor.process(constraint, INDEX, JOINT, coefficients);
    }

    private void thenProcessorShouldCallProcessOnGlobalStrategies() {
        verify(globalConstraintProcessorStrategy1).process(constraint, INDEX, JOINT, coefficients);
        verify(globalConstraintProcessorStrategy2).process(constraint, INDEX, JOINT, coefficients);
    }

    private void thenProcessorShouldNotCallProcessOnLocalStrategies() {
        verifyZeroInteractions(localConstraintProcessorStrategy1, localConstraintProcessorStrategy2);
    }

    private void whenCalledWithLocalConstraint() {
        when(constraint.getLocalReferenceFrame()).thenReturn(LOCAL_REFERENCE_FRAME);
        processor.process(constraint, INDEX, JOINT, coefficients);
    }

    private void thenProcessorShouldCallProcessOnLocalStrategies() {
        verify(localConstraintProcessorStrategy1).process(constraint, INDEX, JOINT, coefficients);
        verify(localConstraintProcessorStrategy2).process(constraint, INDEX, JOINT, coefficients);
    }

    private void thenProcessorShouldNotCallProcessOnGlobalStrategies() {
        verifyZeroInteractions(globalConstraintProcessorStrategy1, globalConstraintProcessorStrategy2);
    }

}
