package org.gabs.statics.solver.resultbuilder;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.model.result.Ground;
import org.gabs.statics.model.result.Reaction;
import org.gabs.statics.model.result.Result;
import org.gabs.statics.model.result.StaticalDeterminacy;
import org.gabs.statics.solver.converter.result.ResultConverterModule;
import org.gabs.statics.solver.data.Analysis;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Determinacy;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.Effect;
import org.gabs.statics.solver.model.Element;
import org.gabs.statics.solver.model.Problem;
import org.gabs.statics.solver.model.ReferenceFrame;
import org.gabs.statics.solver.model.Structure;
import org.gabs.statics.solver.registry.ConstraintRegistry;
import org.gabs.statics.solver.registry.ConstraintRegistryModule;
import org.gabs.statics.solver.resultbuilder.postprocessor.ReactionPostProcessorModule;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class ResultBuilderComponentTest {

    private static final int PRECISION = 14;
    private static final double SQRT2 = Math.sqrt(2);
    private static final double SQRT2DIV2 = SQRT2 / 2.0d;
    private static final String ERROR = "error";
    private static final org.gabs.statics.solver.data.Vector INTERNAL_POS_A = new org.gabs.statics.solver.data.Vector(0, 0, 0);
    private static final org.gabs.statics.solver.data.Vector INTERNAL_POS_B = new org.gabs.statics.solver.data.Vector(0, 2, 0);
    private static final Vector POS_A = new Vector(0, 0, 0);
    private static final Vector POS_B = new Vector(0, 2, 0);

    private ResultBuilder builder;
    private ConstraintRegistry constraintRegistry;
    private Element rollerJoint;
    private Element pinnedJoint;
    private Element fixedJoint;
    private Element beam;
    private Constraint rollerXSupport;
    private Constraint rollerX;
    private Constraint pinnedXSupport;
    private Constraint pinnedYSupport;
    private Constraint pinnedX;
    private Constraint pinnedY;
    private Constraint fixedXSupport;
    private Constraint fixedYSupport;
    private Constraint fixedZZSupport;
    private Constraint fixedX;
    private Constraint fixedY;
    private Constraint fixedZZ;
    private Result result;

    @Test
    public void testBuildWithSolvedSimpleSupportedBeam() {
        givenResultBuilder();
        whenCalledWithSolutionForSimplySupportedBeam();
        thenResultHasCorrectReactionsForSimplySupportedBeam();
        thenResultHasCorrectStructuralDeterminacy();
        thenResultHasCorrectProblemDeterminacy();
        thenResultHasNullError();
    }

    @Test
    public void testBuildWithSolvedCantilever() {
        givenResultBuilder();
        whenCalledWithSolutionForCantilever();
        thenResultHasCorrectReactionsForCantilever();
        thenResultHasCorrectStructuralDeterminacy();
        thenResultHasCorrectProblemDeterminacy();
        thenResultHasNullError();
    }

    @Test
    public void testBuildWithSolvedSimpleSupportedBeamWithJointsInLocalReferenceFrame() {
        givenResultBuilder();
        whenCalledWithSolutionForSimplySupportedBeamWithJointsInLocalReferenceFrame();
        thenResultHasCorrectReactionsForSimplySupportedBeamWithJointsInLocalReferenceFrame();
        thenResultHasCorrectStructuralDeterminacy();
        thenResultHasCorrectProblemDeterminacy();
        thenResultHasNullError();
    }

    @Test
    public void testBuildWithError() {
        givenResultBuilder();
        whenCalledWithError();
        thenResultHasNullReactions();
        thenResultHasNullStructuralDeterminacy();
        thenResultHasNullProblemDeterminacy();
        thenResultHasCorrectError();
    }

    private void givenResultBuilder() {
        Injector injector = Guice.createInjector(new ResultBuilderModule(), new ResultConverterModule(), new ReactionPostProcessorModule(),
                new ConstraintRegistryModule());
        constraintRegistry = injector.getInstance(ConstraintRegistry.class);
        builder = injector.getInstance(ResultBuilder.class);
    }

    private void whenCalledWithSolutionForSimplySupportedBeam() {
        Analysis analysis = createAnalysisForDeterminateProblem();
        Problem problem = createSimplySupportedBeamProblem();
        double[] solution = createSolutionForSimplySupportedBeam();
        result = builder.build(solution, analysis, problem, constraintRegistry);
    }

    private Analysis createAnalysisForDeterminateProblem() {
        return new Analysis(Determinacy.DETERMINATE, Determinacy.DETERMINATE);
    }

    private Problem createSimplySupportedBeamProblem() {
        Structure structure = createSimplySupportedBeam();
        Map<Element, Collection<Effect>> load = createLoadForSimplySupportedBeam();
        return new Problem(structure, load, PRECISION);
    }

    private Structure createSimplySupportedBeam() {
        rollerJoint = new Element("roller joint", true);
        pinnedJoint = new Element("pinned joint", true);
        beam = new Element("beam", false);
        rollerXSupport = new Constraint("roller X support", Dof.X, INTERNAL_POS_A);
        rollerX = new Constraint("roller X", Dof.X, INTERNAL_POS_A);
        pinnedXSupport = new Constraint("pinned X support", Dof.X, INTERNAL_POS_B);
        pinnedYSupport = new Constraint("pinned Y support", Dof.Y, INTERNAL_POS_B);
        pinnedX = new Constraint("pinned X", Dof.X, INTERNAL_POS_B);
        pinnedY = new Constraint("pinned Y", Dof.Y, INTERNAL_POS_B);

        initializeConstraintRegistryForSimplySupportedBeam();

        Structure result = new Structure();
        result.addExternalConstraints(rollerJoint, Collections.singleton(rollerXSupport));
        result.addInternalConstraints(rollerJoint, beam, Collections.singleton(rollerX));
        result.addExternalConstraints(pinnedJoint, Arrays.asList(pinnedXSupport, pinnedYSupport));
        result.addInternalConstraints(pinnedJoint, beam, Arrays.asList(pinnedX, pinnedY));
        result.addInternalConstraints(beam, rollerJoint, Collections.singleton(rollerX));
        result.addInternalConstraints(beam, pinnedJoint, Arrays.asList(pinnedX, pinnedY));
        return result;
    }

    private void initializeConstraintRegistryForSimplySupportedBeam() {
        constraintRegistry.register(rollerXSupport);
        constraintRegistry.register(rollerX);
        constraintRegistry.register(pinnedXSupport);
        constraintRegistry.register(pinnedYSupport);
        constraintRegistry.register(pinnedX);
        constraintRegistry.register(pinnedY);
    }

    private Map<Element, Collection<Effect>> createLoadForSimplySupportedBeam() {
        Effect concentratedForce = new org.gabs.statics.solver.model.ConcentratedForce(new org.gabs.statics.solver.data.Vector(0, 1, 0),
                new org.gabs.statics.solver.data.Vector(-2, -2, 0));
        Effect concentratedMoment = new org.gabs.statics.solver.model.ConcentratedMoment(new org.gabs.statics.solver.data.Vector(0, 0, 5));
        return Collections.<Element, Collection<Effect>>singletonMap(beam, Arrays.asList(concentratedForce, concentratedMoment));
    }

    private double[] createSolutionForSimplySupportedBeam() {
        double[] result = new double[6];
        result[constraintRegistry.getIndex(rollerXSupport)] = -1.5;
        result[constraintRegistry.getIndex(rollerX)] = 1.5;
        result[constraintRegistry.getIndex(pinnedXSupport)] = 3.5;
        result[constraintRegistry.getIndex(pinnedX)] = -3.5;
        result[constraintRegistry.getIndex(pinnedYSupport)] = 2;
        result[constraintRegistry.getIndex(pinnedY)] = -2;
        return result;
    }

    private void thenResultHasCorrectReactionsForSimplySupportedBeam() {
        Reaction expectedRollerSupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(POS_A, new Vector(-1.5, 0, 0)), null);
        Reaction expectedRollerFromBeamReaction = new Reaction(beam.getId(), new ConcentratedForce(POS_A, new Vector(1.5, 0, 0)), null);
        Reaction expectedPinnedSupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(POS_B, new Vector(3.5, 2, 0)), null);
        Reaction expectedPinnedFromBeamReaction = new Reaction(beam.getId(), new ConcentratedForce(POS_B, new Vector(-3.5, -2, 0)), null);
        Reaction expectedBeamFromRollerReaction = new Reaction(rollerJoint.getId(), new ConcentratedForce(POS_A, new Vector(-1.5, 0, 0)), null);
        Reaction expectedBeamFromPinnedReaction = new Reaction(pinnedJoint.getId(), new ConcentratedForce(POS_B, new Vector(3.5, 2, 0)), null);

        Map<String, Collection<Reaction>> reactions = result.getReactions();
        assertThat(reactions.keySet(), containsInAnyOrder(rollerJoint.getId(), pinnedJoint.getId(), beam.getId()));
        assertThat(reactions.get(rollerJoint.getId()), containsInAnyOrder(expectedRollerSupportReaction, expectedRollerFromBeamReaction));
        assertThat(reactions.get(pinnedJoint.getId()), containsInAnyOrder(expectedPinnedSupportReaction, expectedPinnedFromBeamReaction));
        assertThat(reactions.get(beam.getId()), containsInAnyOrder(expectedBeamFromRollerReaction, expectedBeamFromPinnedReaction));
    }

    private void thenResultHasCorrectStructuralDeterminacy() {
        assertThat(result.getStructureDeterminacy(), equalTo(StaticalDeterminacy.DETERMINATE));
    }

    private void thenResultHasCorrectProblemDeterminacy() {
        assertThat(result.getProblemDeterminacy(), equalTo(StaticalDeterminacy.DETERMINATE));
    }

    private void thenResultHasNullError() {
        assertThat(result.getError(), nullValue());
    }

    private void whenCalledWithSolutionForCantilever() {
        Analysis analysis = createAnalysisForDeterminateProblem();
        Problem problem = createCantileverProblem();
        double[] solution = createSolutionForCantilever();
        result = builder.build(solution, analysis, problem, constraintRegistry);
    }

    private Problem createCantileverProblem() {
        Structure structure = createCantilever();
        Map<Element, Collection<Effect>> load = createLoadForCantilever();
        return new Problem(structure, load, PRECISION);
    }

    private Structure createCantilever() {
        fixedJoint = new Element("fixed joint", true);
        beam = new Element("beam", false);
        fixedXSupport = new Constraint("fixed X support", Dof.X, INTERNAL_POS_A);
        fixedYSupport = new Constraint("fixed Y support", Dof.Y, INTERNAL_POS_A);
        fixedZZSupport = new Constraint("fixed ZZ support", Dof.ZZ, INTERNAL_POS_A);
        fixedX = new Constraint("fixed X", Dof.X, INTERNAL_POS_A);
        fixedY = new Constraint("fixed Y", Dof.Y, INTERNAL_POS_A);
        fixedZZ = new Constraint("fixed ZZ", Dof.ZZ, INTERNAL_POS_A);

        initializeConstraintRegistryForCantilever();

        Structure result = new Structure();
        result.addExternalConstraints(fixedJoint, Arrays.asList(fixedXSupport, fixedYSupport, fixedZZSupport));
        result.addInternalConstraints(fixedJoint, beam, Arrays.asList(fixedX, fixedY, fixedZZ));
        result.addInternalConstraints(beam, fixedJoint, Arrays.asList(fixedX, fixedY, fixedZZ));
        return result;
    }

    private void initializeConstraintRegistryForCantilever() {
        constraintRegistry.register(fixedXSupport);
        constraintRegistry.register(fixedYSupport);
        constraintRegistry.register(fixedZZSupport);
        constraintRegistry.register(fixedX);
        constraintRegistry.register(fixedY);
        constraintRegistry.register(fixedZZ);
    }

    private Map<Element, Collection<Effect>> createLoadForCantilever() {
        Effect concentratedForce = new org.gabs.statics.solver.model.ConcentratedForce(new org.gabs.statics.solver.data.Vector(2, 2, 0),
                new org.gabs.statics.solver.data.Vector(-2, 2, 0));
        Effect concentratedMoment = new org.gabs.statics.solver.model.ConcentratedMoment(new org.gabs.statics.solver.data.Vector(0, 0, 5));
        return Collections.<Element, Collection<Effect>>singletonMap(beam, Arrays.asList(concentratedForce, concentratedMoment));
    }

    private double[] createSolutionForCantilever() {
        double[] result = new double[6];
        result[constraintRegistry.getIndex(fixedXSupport)] = 2.0;
        result[constraintRegistry.getIndex(fixedX)] = -2.0;
        result[constraintRegistry.getIndex(fixedYSupport)] = -2.0;
        result[constraintRegistry.getIndex(fixedY)] = 2.0;
        result[constraintRegistry.getIndex(fixedZZSupport)] = -13.0;
        result[constraintRegistry.getIndex(fixedZZ)] = 13.0;
        return result;
    }

    private void thenResultHasCorrectReactionsForCantilever() {
        Reaction expectedFixedSupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(POS_A, new Vector(2, -2, 0)),
                new Moment(new Vector(0, 0, -13)));
        Reaction expectedFixedFromBeamReaction = new Reaction(beam.getId(), new ConcentratedForce(POS_A, new Vector(-2, 2, 0)),
                new Moment(new Vector(0, 0, 13)));
        Reaction expectedBeamFromFixedReaction = new Reaction(fixedJoint.getId(), new ConcentratedForce(POS_A, new Vector(2, -2, 0)),
                new Moment(new Vector(0, 0, -13)));
        Map<String, Collection<Reaction>> reactions = result.getReactions();

        assertThat(reactions.keySet(), containsInAnyOrder(fixedJoint.getId(), beam.getId()));
        assertThat(reactions.get(fixedJoint.getId()), containsInAnyOrder(expectedFixedSupportReaction, expectedFixedFromBeamReaction));
        assertThat(reactions.get(beam.getId()), containsInAnyOrder(expectedBeamFromFixedReaction));
    }

    private void whenCalledWithSolutionForSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        Analysis analysis = createAnalysisForDeterminateProblem();
        Problem problem = createSimplySupportedBeamWithJointsInLocalReferenceFrameProblem();
        double[] solution = createSolutionForSimplySupportedBeamWithJointsInLocalReferenceFrame();
        result = builder.build(solution, analysis, problem, constraintRegistry);
    }

    private Problem createSimplySupportedBeamWithJointsInLocalReferenceFrameProblem() {
        Structure structure = createSimplySupportedBeamWithJointsInLocalReferenceFrame();
        Map<Element, Collection<Effect>> load = createLoadForSimplySupportedBeam();
        return new Problem(structure, load, PRECISION);
    }

    private Structure createSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        ReferenceFrame rollerJointReferenceFrame = new ReferenceFrame(new org.gabs.statics.solver.data.Vector(SQRT2DIV2, SQRT2DIV2, 0),
                new org.gabs.statics.solver.data.Vector(-SQRT2DIV2, SQRT2DIV2, 0),
                new org.gabs.statics.solver.data.Vector(0, 0, 1));
        ReferenceFrame pinnedJointReferenceFrame = new ReferenceFrame(new org.gabs.statics.solver.data.Vector(SQRT2DIV2, -SQRT2DIV2, 0),
                new org.gabs.statics.solver.data.Vector(SQRT2DIV2, SQRT2DIV2, 0),
                new org.gabs.statics.solver.data.Vector(0, 0, 1));

        rollerJoint = new Element("roller joint", true);
        pinnedJoint = new Element("pinned joint", true);
        beam = new Element("beam", false);
        rollerXSupport = new Constraint("roller X support", Dof.X, rollerJointReferenceFrame, INTERNAL_POS_A);
        rollerX = new Constraint("roller X", Dof.X, rollerJointReferenceFrame, INTERNAL_POS_A);
        pinnedXSupport = new Constraint("pinned X support", Dof.X, pinnedJointReferenceFrame, INTERNAL_POS_B);
        pinnedYSupport = new Constraint("pinned Y support", Dof.Y, pinnedJointReferenceFrame, INTERNAL_POS_B);
        pinnedX = new Constraint("pinned X", Dof.X, pinnedJointReferenceFrame, INTERNAL_POS_B);
        pinnedY = new Constraint("pinned Y", Dof.Y, pinnedJointReferenceFrame, INTERNAL_POS_B);

        initializeConstraintRegistryForSimplySupportedBeam();

        Structure result = new Structure();
        result.addExternalConstraints(rollerJoint, Collections.singleton(rollerXSupport));
        result.addInternalConstraints(rollerJoint, beam, Collections.singleton(rollerX));
        result.addExternalConstraints(pinnedJoint, Arrays.asList(pinnedXSupport, pinnedYSupport));
        result.addInternalConstraints(pinnedJoint, beam, Arrays.asList(pinnedX, pinnedY));
        result.addInternalConstraints(beam, rollerJoint, Collections.singleton(rollerX));
        result.addInternalConstraints(beam, pinnedJoint, Arrays.asList(pinnedX, pinnedY));
        return result;
    }

    private double[] createSolutionForSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        double[] result = new double[6];
        result[constraintRegistry.getIndex(rollerXSupport)] = -3.0 / SQRT2;
        result[constraintRegistry.getIndex(rollerX)] = 3.0 / SQRT2;
        result[constraintRegistry.getIndex(pinnedXSupport)] = 0;
        result[constraintRegistry.getIndex(pinnedX)] = 0;
        result[constraintRegistry.getIndex(pinnedYSupport)] = 7.0 / SQRT2;
        result[constraintRegistry.getIndex(pinnedY)] = -7.0 / SQRT2;
        return result;
    }

    private void thenResultHasCorrectReactionsForSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        Reaction expectedRollerSupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(POS_A, new Vector(-1.5, -1.5, 0)), null);
        Reaction expectedRollerFromBeamReaction = new Reaction(beam.getId(), new ConcentratedForce(POS_A, new Vector(1.5, 1.5, 0)), null);
        Reaction expectedPinnedSupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(POS_B, new Vector(3.5, 3.5, 0)), null);
        Reaction expectedPinnedFromBeamReaction = new Reaction(beam.getId(), new ConcentratedForce(POS_B, new Vector(-3.5, -3.5, 0)), null);
        Reaction expectedBeamFromRollerReaction = new Reaction(rollerJoint.getId(), new ConcentratedForce(POS_A, new Vector(-1.5, -1.5, 0)), null);
        Reaction expectedBeamFromPinnedReaction = new Reaction(pinnedJoint.getId(), new ConcentratedForce(POS_B, new Vector(3.5, 3.5, 0)), null);

        Map<String, Collection<Reaction>> reactions = result.getReactions();
        assertThat(reactions.keySet(), containsInAnyOrder(rollerJoint.getId(), pinnedJoint.getId(), beam.getId()));
        assertThat(reactions.get(rollerJoint.getId()), containsInAnyOrder(expectedRollerSupportReaction, expectedRollerFromBeamReaction));
        assertThat(reactions.get(pinnedJoint.getId()), containsInAnyOrder(expectedPinnedSupportReaction, expectedPinnedFromBeamReaction));
        assertThat(reactions.get(beam.getId()), containsInAnyOrder(expectedBeamFromRollerReaction, expectedBeamFromPinnedReaction));
    }

    private void whenCalledWithError() {
        result = builder.build(ERROR);
    }

    private void thenResultHasNullReactions() {
        assertThat(result.getReactions(), nullValue());
    }

    private void thenResultHasNullStructuralDeterminacy() {
        assertThat(result.getStructureDeterminacy(), nullValue());
    }

    private void thenResultHasNullProblemDeterminacy() {
        assertThat(result.getProblemDeterminacy(), nullValue());
    }

    private void thenResultHasCorrectError() {
        assertThat(result.getError(), equalTo(ERROR));
    }

}
