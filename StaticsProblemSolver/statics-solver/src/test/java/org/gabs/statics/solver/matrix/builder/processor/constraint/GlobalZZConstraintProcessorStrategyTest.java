package org.gabs.statics.solver.matrix.builder.processor.constraint;

import org.gabs.statics.solver.data.Vector;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;
import org.junit.Before;
import org.junit.Test;

import java.util.EnumMap;
import java.util.Map;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class GlobalZZConstraintProcessorStrategyTest {

    private static final int INDEX = 1;
    private static final Vector POSITION = new Vector(5, 10, 0);

    private GlobalZZConstraintProcessorStrategy strategy;
    private Map<Dof, double[]> coefficients;

    @Before
    public void setup() {
        coefficients = createBlankCoefficientsMap();
    }

    @Test
    public void testProcessWithJointConstrainedOnZZShouldPutOneAsCoefficientAtIndex() {
        givenGlobalZZConstraintProcessorStrategy();
        whenCalledWithJointConstrainedOnZZ();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeOneAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithBeamConstrainedOnZZShouldPutMinusOneAsCoefficientAtIndex() {
        givenGlobalZZConstraintProcessorStrategy();
        whenCalledWithBeamConstrainedOnZZ();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeMinusOneAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithJointConstrainedOnXShouldPutZeroAsCoefficientAtIndex() {
        givenGlobalZZConstraintProcessorStrategy();
        whenCalledWithJointConstrainedOnX();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeZeroAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithBeamConstrainedOnXShouldPutZeroAsCoefficientAtIndex() {
        givenGlobalZZConstraintProcessorStrategy();
        whenCalledWithBeamConstrainedOnX();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeDistanceYAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithJointConstrainedOnYShouldPutZeroAsCoefficientAtIndex() {
        givenGlobalZZConstraintProcessorStrategy();
        whenCalledWithJointConstrainedOnY();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeZeroAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithBeamConstrainedOnYShouldPutXDistanceAsCoefficientAtIndex() {
        givenGlobalZZConstraintProcessorStrategy();
        whenCalledWithBeamConstrainedOnY();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeMinusDistanceXAtIndexInZZCoefficients();
    }

    private Map<Dof, double[]> createBlankCoefficientsMap() {
        Map<Dof, double[]> result = new EnumMap<>(Dof.class);
        result.put(Dof.X, new double[]{0, 0, 0});
        result.put(Dof.Y, new double[]{0, 0, 0});
        result.put(Dof.ZZ, new double[]{0, 0, 0});
        return result;
    }

    private void givenGlobalZZConstraintProcessorStrategy() {
        strategy = new GlobalZZConstraintProcessorStrategy();
    }

    private void whenCalledWithJointConstrainedOnZZ() {
        strategy.process(createConstraint(Dof.ZZ), INDEX, true, coefficients);
    }

    private Constraint createConstraint(Dof constrainedDof) {
        return new Constraint(null, constrainedDof, null, POSITION);
    }

    private void thenCoefficientShouldBeZeroAtIndexInXCoefficients() {
        assertThat(coefficients.get(Dof.X)[INDEX], equalTo(0d));
    }

    private void thenCoefficientShouldBeZeroAtIndexInYCoefficients() {
        assertThat(coefficients.get(Dof.Y)[INDEX], equalTo(0d));
    }

    private void thenCoefficientShouldBeOneAtIndexInZZCoefficients() {
        assertThat(coefficients.get(Dof.ZZ)[INDEX], equalTo(1d));
    }

    private void whenCalledWithBeamConstrainedOnZZ() {
        strategy.process(createConstraint(Dof.ZZ), INDEX, false, coefficients);
    }

    private void thenCoefficientShouldBeMinusOneAtIndexInZZCoefficients() {
        assertThat(coefficients.get(Dof.ZZ)[INDEX], equalTo(-1d));
    }

    private void thenCoefficientShouldBeZeroAtIndexInZZCoefficients() {
        assertThat(coefficients.get(Dof.ZZ)[INDEX], equalTo(0d));
    }

    private void whenCalledWithJointConstrainedOnX() {
        strategy.process(createConstraint(Dof.X), INDEX, true, coefficients);
    }

    private void whenCalledWithBeamConstrainedOnX() {
        strategy.process(createConstraint(Dof.X), INDEX, false, coefficients);
    }

    private void thenCoefficientShouldBeDistanceYAtIndexInZZCoefficients() {
        assertThat(coefficients.get(Dof.ZZ)[INDEX], equalTo(POSITION.getY()));
    }

    private void whenCalledWithJointConstrainedOnY() {
        strategy.process(createConstraint(Dof.Y), INDEX, true, coefficients);
    }

    private void whenCalledWithBeamConstrainedOnY() {
        strategy.process(createConstraint(Dof.Y), INDEX, false, coefficients);
    }

    private void thenCoefficientShouldBeMinusDistanceXAtIndexInZZCoefficients() {
        assertThat(coefficients.get(Dof.ZZ)[INDEX], equalTo(-POSITION.getX()));
    }

}
