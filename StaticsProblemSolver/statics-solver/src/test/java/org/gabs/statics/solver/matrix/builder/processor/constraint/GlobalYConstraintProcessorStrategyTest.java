package org.gabs.statics.solver.matrix.builder.processor.constraint;

import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;
import org.junit.Before;
import org.junit.Test;

import java.util.EnumMap;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class GlobalYConstraintProcessorStrategyTest {

    private static final int INDEX = 1;

    private GlobalYConstraintProcessorStrategy strategy;
    private Map<Dof, double[]> coefficients;

    @Before
    public void setup() {
        coefficients = createBlankCoefficientsMap();
    }

    @Test
    public void testProcessWithJointConstrainedOnYShouldPutOneAsCoefficientAtIndex() {
        givenGlobalYConstraintProcessorStrategy();
        whenCalledWithJointConstrainedOnY();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeOneAtIndexInYCoefficients();
        thenCoefficientShouldBeZeroAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithBeamConstrainedOnYShouldPutMinusOneAsCoefficientAtIndex() {
        givenGlobalYConstraintProcessorStrategy();
        whenCalledWithBeamConstrainedOnY();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeMinusOneAtIndexInYCoefficients();
        thenCoefficientShouldBeZeroAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithJointConstrainedOnXShouldNotPutCoefficientAtIndex() {
        givenGlobalYConstraintProcessorStrategy();
        whenCalledWithJointConstrainedOnX();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeZeroAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithJointConstrainedOnZZShouldNotPutCoefficientAtIndex() {
        givenGlobalYConstraintProcessorStrategy();
        whenCalledWithJointConstrainedOnZZ();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeZeroAtIndexInZZCoefficients();
    }

    private Map<Dof, double[]> createBlankCoefficientsMap() {
        Map<Dof, double[]> result = new EnumMap<>(Dof.class);
        result.put(Dof.X, new double[]{0, 0, 0});
        result.put(Dof.Y, new double[]{0, 0, 0});
        result.put(Dof.ZZ, new double[]{0, 0, 0});
        return result;
    }

    private void givenGlobalYConstraintProcessorStrategy() {
        strategy = new GlobalYConstraintProcessorStrategy();
    }

    private void whenCalledWithJointConstrainedOnY() {
        strategy.process(createConstraint(Dof.Y), INDEX, true, coefficients);
    }

    private void thenCoefficientShouldBeZeroAtIndexInXCoefficients() {
        assertThat(coefficients.get(Dof.X)[INDEX], equalTo(0d));
    }

    private void thenCoefficientShouldBeOneAtIndexInYCoefficients() {
        assertThat(coefficients.get(Dof.Y)[INDEX], equalTo(1d));
    }

    private void thenCoefficientShouldBeZeroAtIndexInZZCoefficients() {
        assertThat(coefficients.get(Dof.ZZ)[INDEX], equalTo(0d));
    }

    private void whenCalledWithBeamConstrainedOnY() {
        strategy.process(createConstraint(Dof.Y), INDEX, false, coefficients);
    }

    private void thenCoefficientShouldBeMinusOneAtIndexInYCoefficients() {
        assertThat(coefficients.get(Dof.Y)[INDEX], equalTo(-1d));
    }

    private void whenCalledWithJointConstrainedOnX() {
        strategy.process(createConstraint(Dof.X), INDEX, true, coefficients);
    }

    private Constraint createConstraint(Dof constrainedDof) {
        return new Constraint(null, constrainedDof, null, null);
    }

    private void thenCoefficientShouldBeZeroAtIndexInYCoefficients() {
        assertThat(coefficients.get(Dof.Y)[INDEX], equalTo(0d));
    }

    private void whenCalledWithJointConstrainedOnZZ() {
        strategy.process(createConstraint(Dof.ZZ), INDEX, true, coefficients);
    }

}
