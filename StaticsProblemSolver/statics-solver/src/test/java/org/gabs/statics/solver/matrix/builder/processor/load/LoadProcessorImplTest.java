package org.gabs.statics.solver.matrix.builder.processor.load;

import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.Effect;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Map;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LoadProcessorImplTest {

    private LoadProcessorImpl processor;
    @Mock private Effect effect;
    @Mock private LoadProcessorStrategy loadProcessorStrategy1;
    @Mock private LoadProcessorStrategy loadProcessorStrategy2;
    @Mock private Map<Dof, double[]> coefficients;

    @Test
    public void testProcessShouldCallProcessOnStrategies() {
        givenLoadProcessorImpl();
        whenProcessCalled();
        thenProcessorShouldCallProcessOnStrategies();
    }

    private void givenLoadProcessorImpl() {
        processor = new LoadProcessorImpl();
        processor.setLoadProcessorStrategies(Arrays.asList(loadProcessorStrategy1, loadProcessorStrategy2));
    }

    private void whenProcessCalled() {
        processor.process(effect, coefficients, true);
    }

    private void thenProcessorShouldCallProcessOnStrategies() {
        verify(loadProcessorStrategy1).process(effect, coefficients, true);
        verify(loadProcessorStrategy2).process(effect, coefficients, true);
    }

}
