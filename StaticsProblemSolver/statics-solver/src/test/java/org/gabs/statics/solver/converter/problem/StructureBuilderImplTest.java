package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.problem.Beam;
import org.gabs.statics.model.problem.Constraints;
import org.gabs.statics.model.problem.Joint;
import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Pair;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Element;
import org.gabs.statics.solver.model.Structure;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StructureBuilderImplTest {

    @InjectMocks private StructureBuilderImpl builder;
    @Mock private Converter<Pair, Collection<Constraint>> constraintsConverter;
    @Mock private Joint joint1;
    @Mock private Joint joint2;
    @Mock private Beam beam1;
    @Mock private Beam beam2;
    @Mock private Element convertedJoint1;
    @Mock private Element convertedJoint2;
    @Mock private Element convertedBeam1;
    @Mock private Element convertedBeam2;
    @Mock private Constraints joint1Support;
    @Mock private Constraints joint2Support;
    @Mock private Collection<Constraint> convertedConstraintsForJoint1;
    @Mock private Collection<Constraint> convertedConstraintsForJoint2;
    @Mock private Collection<Constraint> convertedConstraintsForJoint1Beam1;
    @Mock private Collection<Constraint> convertedConstraintsForJoint2Beam1;
    @Mock private Collection<Constraint> convertedConstraintsForJoint1Beam2;
    private Structure result;

    @Before
    public void setup() {
        when(joint1.getId()).thenReturn("joint 1");
        when(joint2.getId()).thenReturn("joint 2");
        when(beam1.getId()).thenReturn("beam 1");
        when(beam2.getId()).thenReturn("beam 2");
    }

    @Test
    public void testBuildWithSimplySupportedBeamResultShouldHaveConstraintsAtJoints() {
        whenCalledWithSimplySupportedBeam();
        thenResultShouldHaveConstraintsAtJoints();
    }

    @Test
    public void testBuildWithSimplySupportedBeamResultShouldHaveConstraintsAtConnectedElements() {
        whenCalledWithSimplySupportedBeam();
        thenResultShouldHaveConstraintsAtConnectedElements();
    }

    @Test
    public void testBuildWithTwoBeamsConnectedToJointResultShouldNotHaveConstraintsAtJoints() {
        whenCalledWithTwoBeamsConnectedToJoint();
        thenResultShouldNotHaveConstraintsAtJoints();
    }

    @Test
    public void testBuildWithTwoBeamsConnectedToJointShouldHaveConstraintsAtConnectedElements() {
        whenCalledWithTwoBeamsConnectedToJoint();
        thenResultShouldHaveConstraintsAtBothConnectedElements();
    }

    private void whenCalledWithSimplySupportedBeam() {
        Problem problem = createSimplySupportedBeam();
        Map<org.gabs.statics.model.problem.Element, Element> elementMapping = createElementMapping();
        when(joint1.getSupport()).thenReturn(joint1Support);
        when(joint2.getSupport()).thenReturn(joint2Support);
        when(constraintsConverter.convert(new Pair<Joint, Beam>(joint1, null))).thenReturn(convertedConstraintsForJoint1);
        when(constraintsConverter.convert(new Pair<Joint, Beam>(joint2, null))).thenReturn(convertedConstraintsForJoint2);
        when(constraintsConverter.convert(new Pair<>(joint1, beam1))).thenReturn(convertedConstraintsForJoint1Beam1);
        when(constraintsConverter.convert(new Pair<>(joint2, beam1))).thenReturn(convertedConstraintsForJoint2Beam1);
        result = builder.build(problem, elementMapping);
    }

    private Problem createSimplySupportedBeam() {
        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(joint1, beam1);
        builder.addBeam(joint2, beam1);
        return builder.build();
    }

    private Map<org.gabs.statics.model.problem.Element, Element> createElementMapping() {
        Map<org.gabs.statics.model.problem.Element, Element> result = new HashMap<>(3, 1);
        result.put(joint1, convertedJoint1);
        result.put(joint2, convertedJoint2);
        result.put(beam1, convertedBeam1);
        result.put(beam2, convertedBeam2);
        return result;
    }

    private void thenResultShouldHaveConstraintsAtJoints() {
        assertThat(result.getConstraintsBetween(convertedJoint1, Element.GROUND), equalTo(convertedConstraintsForJoint1));
        assertThat(result.getConstraintsBetween(convertedJoint2, Element.GROUND), equalTo(convertedConstraintsForJoint2));
    }

    private void thenResultShouldHaveConstraintsAtConnectedElements() {
        assertThat(result.getConstraintsBetween(convertedJoint1, convertedBeam1), equalTo(convertedConstraintsForJoint1Beam1));
        assertThat(result.getConstraintsBetween(convertedBeam1, convertedJoint1), equalTo(convertedConstraintsForJoint1Beam1));
        assertThat(result.getConstraintsBetween(convertedJoint2, convertedBeam1), equalTo(convertedConstraintsForJoint2Beam1));
        assertThat(result.getConstraintsBetween(convertedBeam1, convertedJoint2), equalTo(convertedConstraintsForJoint2Beam1));
    }

    private void whenCalledWithTwoBeamsConnectedToJoint() {
        Problem problem = createJointWithTwoBeamsConnected();
        Map<org.gabs.statics.model.problem.Element, Element> elementMapping = createElementMapping();
        when(joint1.getSupport()).thenReturn(null);
        when(constraintsConverter.convert(new Pair<Joint, Beam>(joint1, null))).thenReturn(convertedConstraintsForJoint1);
        when(constraintsConverter.convert(new Pair<Joint, Beam>(joint2, null))).thenReturn(convertedConstraintsForJoint2);
        when(constraintsConverter.convert(new Pair<>(joint1, beam1))).thenReturn(convertedConstraintsForJoint1Beam1);
        when(constraintsConverter.convert(new Pair<>(joint1, beam2))).thenReturn(convertedConstraintsForJoint1Beam2);
        result = builder.build(problem, elementMapping);
    }

    private Problem createJointWithTwoBeamsConnected() {
        return new Problem.Builder().addBeams(joint1, beam1, beam2).build();
    }

    private void thenResultShouldNotHaveConstraintsAtJoints() {
        assertThat(result.getConstraintsBetween(convertedJoint1, convertedJoint1), nullValue());
    }

    private void thenResultShouldHaveConstraintsAtBothConnectedElements() {
        assertThat(result.getConstraintsBetween(convertedJoint1, convertedBeam1), equalTo(convertedConstraintsForJoint1Beam1));
        assertThat(result.getConstraintsBetween(convertedBeam1, convertedJoint1), equalTo(convertedConstraintsForJoint1Beam1));
        assertThat(result.getConstraintsBetween(convertedJoint1, convertedBeam2), equalTo(convertedConstraintsForJoint1Beam2));
        assertThat(result.getConstraintsBetween(convertedBeam2, convertedJoint1), equalTo(convertedConstraintsForJoint1Beam2));
    }

}
