package org.gabs.statics.solver.resultbuilder;

import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.model.result.Ground;
import org.gabs.statics.model.result.Reaction;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Pair;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.Problem;
import org.gabs.statics.solver.model.Structure;
import org.gabs.statics.solver.registry.ConstraintRegistry;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ReactionMappingBuilderImplTest {

    private static final double VALUE_1 = 1.0;
    private static final double VALUE_2 = 2.0;
    private static final double VALUE_3 = 3.0;
    private static final String ELEMENT_1_ID = "element 1";
    private static final String ELEMENT_2_ID = "element 2";

    @InjectMocks private ReactionMappingBuilderImpl builder;
    @Mock private Converter<Pair<Constraint, Double>, ConcentratedForce> concentratedForceConverter;
    @Mock private Converter<Pair<Constraint, Double>, Moment> concentratedMomentConverter;
    @Mock private ConcentratedForceMergingStrategy concentratedForceMergingStrategy;
    @Mock private Constraint constraint1;
    @Mock private Constraint constraint2;
    @Mock private Constraint constraint3;
    @Mock private org.gabs.statics.solver.model.Element element1;
    @Mock private org.gabs.statics.solver.model.Element element2;
    @Mock private ConcentratedForce force1;
    @Mock private ConcentratedForce force2;
    @Mock private ConcentratedForce force3;
    @Mock private ConcentratedForce mergedForce;
    @Mock private Moment moment1;
    @Mock private Moment moment2;
    @Mock private Problem problem;
    @Mock private ConstraintRegistry constraintRegistry;
    @Captor private ArgumentCaptor<Pair<Constraint, Double>> pairCaptor;
    private Map<String, Collection<Reaction>> result;

    @Before
    public void setup() {
        when(element1.getId()).thenReturn(ELEMENT_1_ID);
        when(element2.getId()).thenReturn(ELEMENT_2_ID);
    }

    @Test
    public void testBuildWithLinearConstraintsAndJointsShouldResultCorrectMappingsWithForces() {
        whenCalledWithLinearConstraintsAndJoints();
        thenResultShouldHaveCorrectMappingsWithForces();
    }

    @Test
    public void testBuildWithAngularConstraintsAndJointsShouldResultCorrectMappingsWithForces() {
        whenCalledWithAngularConstraintsAndJoints();
        thenResultShouldHaveCorrectMappingsWithMoments();
    }

    @Test
    public void testBuildWithLinearConstraintsAndBeamsShouldResultCorrectMappingsWithNegativeForces() {
        whenCalledWithLinearConstraintsAndBeams();
        thenForceConverterIsCalledWithNegativeForces();
    }

    private void whenCalledWithLinearConstraintsAndJoints() {
        createLinearConstraints();
        initializeConstraintRegistry();
        double[] solution = new double[]{VALUE_1, VALUE_2, VALUE_3};
        when(problem.getStructure()).thenReturn(createStructure());
        when(element1.isJoint()).thenReturn(true);
        when(element2.isJoint()).thenReturn(true);
        when(concentratedForceConverter.convert(new Pair<>(constraint1, VALUE_1))).thenReturn(force1);
        when(concentratedForceConverter.convert(new Pair<>(constraint2, VALUE_2))).thenReturn(force2);
        when(concentratedForceConverter.convert(new Pair<>(constraint3, VALUE_3))).thenReturn(force3);
        when(concentratedForceMergingStrategy.merge(Arrays.asList(force2, force3))).thenReturn(mergedForce);
        when(concentratedForceMergingStrategy.merge(Arrays.asList(force1))).thenReturn(force1);
        result = builder.build(solution, problem, constraintRegistry);
    }

    private void createLinearConstraints() {
        when(constraint1.getConstrainedDisplacement()).thenReturn(Dof.X);
        when(constraint2.getConstrainedDisplacement()).thenReturn(Dof.X);
        when(constraint3.getConstrainedDisplacement()).thenReturn(Dof.Y);
    }

    private void initializeConstraintRegistry() {
        when(constraintRegistry.getIndex(constraint1)).thenReturn(0);
        when(constraintRegistry.getIndex(constraint2)).thenReturn(1);
        when(constraintRegistry.getIndex(constraint3)).thenReturn(2);
    }

    private Structure createStructure() {
        Structure result = new Structure();
        result.addExternalConstraints(element1, Collections.singleton(constraint1));
        result.addInternalConstraints(element1, element2, Arrays.asList(constraint2, constraint3));
        result.addInternalConstraints(element2, element1, Arrays.asList(constraint2, constraint3));
        return result;
    }

    private void thenResultShouldHaveCorrectMappingsWithForces() {
        Reaction expectedElement1Support = new Reaction(Ground.getId(), force1, null);
        Reaction expectedElement12Reaction = new Reaction(ELEMENT_2_ID, mergedForce, null);
        Reaction expectedElement21Reaction = new Reaction(ELEMENT_1_ID, mergedForce, null);
        assertThat(result.keySet(), containsInAnyOrder(ELEMENT_1_ID, ELEMENT_2_ID));
        assertThat(result.get(ELEMENT_1_ID), containsInAnyOrder(expectedElement1Support, expectedElement12Reaction));
        assertThat(result.get(ELEMENT_2_ID), contains(expectedElement21Reaction));
    }

    private void whenCalledWithAngularConstraintsAndJoints() {
        createAngularConstraints();
        initializeConstraintRegistry();
        double[] solution = new double[]{VALUE_1, VALUE_2};
        when(problem.getStructure()).thenReturn(createStructureWithAngularConstraints());
        when(element1.isJoint()).thenReturn(true);
        when(element2.isJoint()).thenReturn(true);
        when(concentratedMomentConverter.convert(new Pair<>(constraint1, VALUE_1))).thenReturn(moment1);
        when(concentratedMomentConverter.convert(new Pair<>(constraint2, VALUE_2))).thenReturn(moment2);
        result = builder.build(solution, problem, constraintRegistry);
    }

    private Structure createStructureWithAngularConstraints() {
        Structure result = new Structure();
        result.addExternalConstraints(element1, Collections.singleton(constraint1));
        result.addInternalConstraints(element1, element2, Arrays.asList(constraint2));
        result.addInternalConstraints(element2, element1, Arrays.asList(constraint2));
        return result;
    }

    private void createAngularConstraints() {
        when(constraint1.getConstrainedDisplacement()).thenReturn(Dof.ZZ);
        when(constraint2.getConstrainedDisplacement()).thenReturn(Dof.ZZ);
    }

    private void thenResultShouldHaveCorrectMappingsWithMoments() {
        Reaction expectedElement1Support = new Reaction(Ground.getId(), null, moment1);
        Reaction expectedElement12Reaction = new Reaction(ELEMENT_2_ID, null, moment2);
        Reaction expectedElement21Reaction = new Reaction(ELEMENT_1_ID, null, moment2);
        assertThat(result.keySet(), containsInAnyOrder(ELEMENT_1_ID, ELEMENT_2_ID));
        assertThat(result.get(ELEMENT_1_ID), containsInAnyOrder(expectedElement1Support, expectedElement12Reaction));
        assertThat(result.get(ELEMENT_2_ID), contains(expectedElement21Reaction));
    }

    private void whenCalledWithLinearConstraintsAndBeams() {
        createLinearConstraints();
        initializeConstraintRegistry();
        double[] solution = new double[]{VALUE_1, VALUE_2, VALUE_3};
        when(problem.getStructure()).thenReturn(createStructure());
        when(element1.isJoint()).thenReturn(false);
        when(element2.isJoint()).thenReturn(false);
        result = builder.build(solution, problem, constraintRegistry);
    }

    @SuppressWarnings("unchecked")
    private void thenForceConverterIsCalledWithNegativeForces() {
        verify(concentratedForceConverter, times(5)).convert(pairCaptor.capture());
        assertThat(pairCaptor.getAllValues(), containsInAnyOrder(new Pair<>(constraint1, -VALUE_1), new Pair<>(constraint2, -VALUE_2),
                new Pair<>(constraint2, -VALUE_2), new Pair<>(constraint3, -VALUE_3), new Pair<>(constraint3, -VALUE_3)));
    }

}
