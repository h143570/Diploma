package org.gabs.statics.solver.matrix.builder.processor.load;

import org.gabs.statics.solver.data.Vector;
import org.gabs.statics.solver.model.ConcentratedForce;
import org.gabs.statics.solver.model.ConcentratedMoment;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.Effect;
import org.junit.Before;
import org.junit.Test;

import java.util.EnumMap;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class ConcentratedForceProcessorStrategyTest {

    private static final double INITIAL_CONSTANT = 1;
    private static final Vector POINT_OF_APPLICATION = new Vector(5.0, 10.0, 0);
    private static final Vector FORCE_VECTOR = new Vector(1, 1, 0);
    private static final Effect CONCENTRATED_FORCE = new ConcentratedForce(POINT_OF_APPLICATION, FORCE_VECTOR);

    private ConcentratedForceProcessorStrategy strategy;
    private Map<Dof, double[]> coefficients;

    @Before
    public void setup() {
        coefficients = createCoefficientsMap();
    }

    @Test
    public void testProcessWithConcentratedForceShouldAddNegatedXProjectionToLastIndexOfXCoefficients() {
        givenConcentratedForceProcessorStrategy();
        whenCalledWithConcentratedForceOnBeam();
        thenNegatedXProjectionShouldBeAddedToLastIndexOfXCoefficients();
    }

    @Test
    public void testProcessWithConcentratedForceShouldAddNegatedYProjectionToLastIndexOfYCoefficients() {
        givenConcentratedForceProcessorStrategy();
        whenCalledWithConcentratedForceOnBeam();
        thenNegatedYProjectionShouldBeAddedToLastIndexOfYCoefficients();
    }

    @Test
    public void testProcessWithConcentratedForceShouldAddNegatedMomentToLastIndexOfZZCoefficients() {
        givenConcentratedForceProcessorStrategy();
        whenCalledWithConcentratedForceOnBeam();
        thenNegatedMomentShouldBeAddedToLastIndexOfZZCoefficients();
    }

    @Test
    public void testProcessWithConcentratedForceOnJointShouldNotAddMomentToZZCoefficients() {
        givenConcentratedForceProcessorStrategy();
        whenCalledWithConcentratedForceOnJoint();
        thenZZCoefficientsShouldHaveInitialConstantsAtLastIndex();
    }

    @Test
    public void testProcessWithConcentratedMomentOnBeamShouldNotChangeCoefficients() {
        givenConcentratedForceProcessorStrategy();
        whenCalledWithConcentratedMomentOnBeam();
        thenCoefficientsShouldHaveInitialConstantsAtLastIndex();
    }

    @Test
    public void testProcessWithConcentratedMomentOnJointShouldNotChangeCoefficients() {
        givenConcentratedForceProcessorStrategy();
        whenCalledWithConcentratedMomentOnJoint();
        thenZZCoefficientsShouldHaveInitialConstantsAtLastIndex();
    }

    private Map<Dof, double[]> createCoefficientsMap() {
        Map<Dof, double[]> result = new EnumMap<>(Dof.class);
        result.put(Dof.X, new double[]{0, 0, INITIAL_CONSTANT});
        result.put(Dof.Y, new double[]{0, 0, INITIAL_CONSTANT});
        result.put(Dof.ZZ, new double[]{0, 0, INITIAL_CONSTANT});
        return result;
    }

    private void givenConcentratedForceProcessorStrategy() {
        strategy = new ConcentratedForceProcessorStrategy();
    }

    private void whenCalledWithConcentratedForceOnBeam() {
        strategy.process(CONCENTRATED_FORCE, coefficients, false);
    }

    private void thenNegatedXProjectionShouldBeAddedToLastIndexOfXCoefficients() {
        assertThat(coefficients.get(Dof.X)[2], equalTo(INITIAL_CONSTANT - FORCE_VECTOR.getX()));
    }

    private void thenNegatedYProjectionShouldBeAddedToLastIndexOfYCoefficients() {
        assertThat(coefficients.get(Dof.Y)[2], equalTo(INITIAL_CONSTANT - FORCE_VECTOR.getY()));
    }

    private void thenNegatedMomentShouldBeAddedToLastIndexOfZZCoefficients() {
        double moment = POINT_OF_APPLICATION.crossProduct(FORCE_VECTOR).getZ();
        assertThat(coefficients.get(Dof.ZZ)[2], equalTo(INITIAL_CONSTANT - moment));
    }

    private void whenCalledWithConcentratedForceOnJoint() {
        strategy.process(CONCENTRATED_FORCE, coefficients, true);
    }

    private void thenZZCoefficientsShouldHaveInitialConstantsAtLastIndex() {
        assertThat(coefficients.get(Dof.ZZ)[2], equalTo(INITIAL_CONSTANT));
    }

    private void whenCalledWithConcentratedMomentOnBeam() {
        strategy.process(new ConcentratedMoment(new Vector(0, 0, 1)), coefficients, false);
    }

    private void thenCoefficientsShouldHaveInitialConstantsAtLastIndex() {
        assertThat(coefficients.get(Dof.X)[2], equalTo(INITIAL_CONSTANT));
        assertThat(coefficients.get(Dof.Y)[2], equalTo(INITIAL_CONSTANT));
        assertThat(coefficients.get(Dof.ZZ)[2], equalTo(INITIAL_CONSTANT));
    }

    private void whenCalledWithConcentratedMomentOnJoint() {
        strategy.process(new ConcentratedMoment(new Vector(0, 0, 1)), coefficients, true);
    }

}
