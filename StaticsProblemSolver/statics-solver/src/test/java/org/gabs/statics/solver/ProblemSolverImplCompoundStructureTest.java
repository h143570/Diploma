package org.gabs.statics.solver;

import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.problem.*;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.model.result.Ground;
import org.gabs.statics.model.result.Reaction;
import org.gabs.statics.model.result.Result;
import org.gabs.statics.model.result.StaticalDeterminacy;
import org.junit.Test;

import java.util.Collection;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class ProblemSolverImplCompoundStructureTest {

    private ProblemSolver solver;
    private Joint rollerJoint1;
    private Joint rollerJoint2;
    private Joint rollerJoint3;
    private Joint rollerJoint4;
    private Joint pinnedJoint;
    private Joint hingeJoint1;
    private Joint hingeJoint2;
    private Joint fixedHinge;
    private Joint fixedJoint;
    private Beam beam1;
    private Beam beam2;
    private Beam beam3;
    private Result result;

    @Test
    public void testSolveGerbersBeam() {
        givenProblemSolver();
        whenCalledWithGerbersBeam();
        thenResultHasCorrectReactionsForGerbersBeam();
        thenResultHasDeterminateStructuralDeterminacy();
        thenResultHasDeterminateProblemDeterminacy();
        thenResultHasNullError();
    }

    @Test
    public void testSolveSolvableOverdeterminateCompoundStructure() {
        givenProblemSolver();
        whenCalledWithSolvableOverdeterminedCompoundStructure();
        thenResultHasCorrectReactionsForSolvableOverdeterminateCompoundStructure();
        thenResultHasOverdeterminateStructuralDeterminacy();
        thenResultHasDeterminateProblemDeterminacy();
        thenResultHasNullError();
    }

    @Test
    public void testSolveUnsolvableOverdeterminateCompoundStructure() {
        givenProblemSolver();
        whenCalledWithUnsolvableOverdeterminateCompoundStructure();
        thenResultHasNullReactions();
        thenResultHasOverdeterminateStructuralDeterminacy();
        thenResultHasOverdeterminateProblemDeterminacy();
        thenResultHasNullError();
    }

    @Test
    public void testSolveIndeterminateCompoundStructure() {
        givenProblemSolver();
        whenCalledWithIndeterminateCompoundStructure();
        thenResultHasNullReactions();
        thenResultHasIndeterminateStructuralDeterminacy();
        thenResultHasIndeterminateProblemDeterminacy();
        thenResultHasNullError();
    }

    @Test
    public void testSolveCompoundStructureWithFixedHinge() {
        givenProblemSolver();
        whenCalledWithCompoundStructureWithFixedHinge();
        thenResultHasCorrectReactionsForCompoundStructureWithFixedHinge();
        thenResultHasDeterminateStructuralDeterminacy();
        thenResultHasDeterminateProblemDeterminacy();
        thenResultHasNullError();
    }

    private void givenProblemSolver() {
        solver = new ProblemSolverFactoryImpl().create();
    }

    private void whenCalledWithGerbersBeam() {
        Problem problem = createGerbersBeam();
        result = solver.solve(problem);
    }

    private Problem createGerbersBeam() {
        ConcentratedForce concentratedForce1 = new ConcentratedForce(new Vector(0, 5, 0), new Vector(-3, 0, 0));
        ConcentratedForce concentratedForce2 = new ConcentratedForce(new Vector(0, 6, 0), new Vector(-2, -2, 0));
        Moment moment = new Moment(new Vector(0, 0, 5));
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints pinnedSupport = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints rollerSupport = new Constraints.Builder().constrainTx().build();
        pinnedJoint = new Joint.Builder("pinned joint", new Vector(0, 0, 0), hinge).withSupport(pinnedSupport).build();
        rollerJoint1 = new Joint.Builder("roller joint 1", new Vector(0, 3, 0), hinge).withSupport(rollerSupport).build();
        rollerJoint2 = new Joint.Builder("roller joint 2", new Vector(0, 7, 0), hinge).withSupport(rollerSupport).build();
        rollerJoint3 = new Joint.Builder("roller joint 3", new Vector(0, 10, 0), hinge).withSupport(rollerSupport).build();
        hingeJoint1 = new Joint.Builder("hinge joint 1", new Vector(0, 4, 0), hinge).build();
        hingeJoint2 = new Joint.Builder("hinge joint 2", new Vector(0, 6, 0), hinge).addConcentratedForce(concentratedForce2).build();
        beam1 = new Beam.Builder("beam 1").build();
        beam2 = new Beam.Builder("beam 2").addConcentratedForce(concentratedForce1).build();
        beam3 = new Beam.Builder("beam 3").addMoment(moment).build();

        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(pinnedJoint, beam1);
        builder.addBeam(rollerJoint1, beam1);
        builder.addBeams(hingeJoint1, beam1, beam2);
        builder.addBeams(hingeJoint2, beam2, beam3);
        builder.addBeam(rollerJoint2, beam3);
        builder.addBeam(rollerJoint3, beam3);
        return builder.build();
    }

    private void thenResultHasCorrectReactionsForGerbersBeam() {
        Reaction expectedPinnedSupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(-0.5, 2, 0)),
                null);
        Reaction expectedPinnedFromBeam1Reaction = new Reaction(beam1.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(0.5, -2, 0)),
                null);
        Reaction expectedRoller1SupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(2, 0, 0)), null);
        Reaction expectedRoller1FromBeam1Reaction = new Reaction(beam1.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(-2, 0, 0)),
                null);
        Reaction expectedHinge1FromBeam1Reaction = new Reaction(beam1.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(1.5, 2, 0)),
                null);
        Reaction expectedHinge1FromBeam2Reaction = new Reaction(beam2.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(-1.5, -2, 0)),
                null);
        Reaction expectedHinge2FromBeam2Reaction = new Reaction(beam2.getId(), new ConcentratedForce(new Vector(0, 6, 0), new Vector(-1.5, 2, 0)),
                null);
        Reaction expectedHinge2FromBeam3Reaction = new Reaction(beam3.getId(), new ConcentratedForce(new Vector(0, 6, 0), new Vector(3.5, 0, 0)),
                null);
        Reaction expectedRoller2SupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 7, 0), new Vector(3, 0, 0)), null);
        Reaction expectedRoller2FromBeam3Reaction = new Reaction(beam3.getId(), new ConcentratedForce(new Vector(0, 7, 0), new Vector(-3.0, 0, 0)),
                null);
        Reaction expectedRoller3SupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 10, 0), new Vector(0.5, 0, 0)),
                null);
        Reaction expectedRoller3FromBeam3Reaction = new Reaction(beam3.getId(), new ConcentratedForce(new Vector(0, 10, 0), new Vector(-0.5, 0,
                0)), null);
        Reaction expectedBeam1FromPinnedReaction = new Reaction(pinnedJoint.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(-0.5, 2,
                0)), null);
        Reaction expectedBeam1FromRoller1Reaction = new Reaction(rollerJoint1.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(2, 0,
                0)), null);
        Reaction expectedBeam1FromHinge1Reaction = new Reaction(hingeJoint1.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(-1.5,
                -2, 0)), null);
        Reaction expectedBeam2FromHinge1Reaction = new Reaction(hingeJoint1.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(1.5, 2,
                0)), null);
        Reaction expectedBeam2FromHinge2Reaction = new Reaction(hingeJoint2.getId(), new ConcentratedForce(new Vector(0, 6, 0), new Vector(1.5, -2,
                0)), null);
        Reaction expectedBeam3FromHinge2Reaction = new Reaction(hingeJoint2.getId(), new ConcentratedForce(new Vector(0, 6, 0), new Vector(-3.5, 0,
                0)), null);
        Reaction expectedBeam3FromRoller2Reaction = new Reaction(rollerJoint2.getId(), new ConcentratedForce(new Vector(0, 7, 0), new Vector(3.0,
                0, 0)), null);
        Reaction expectedBeam3FromRoller3Reaction = new Reaction(rollerJoint3.getId(), new ConcentratedForce(new Vector(0, 10, 0), new Vector(0.5,
                0, 0)), null);

        Map<String, Collection<Reaction>> reactions = result.getReactions();
        assertThat(reactions.keySet(), containsInAnyOrder(pinnedJoint.getId(), rollerJoint1.getId(), rollerJoint2.getId(), rollerJoint3.getId(),
                hingeJoint1.getId(), hingeJoint2.getId(), beam1.getId(), beam2.getId(), beam3.getId()));
        assertThat(reactions.get(pinnedJoint.getId()), containsInAnyOrder(expectedPinnedSupportReaction, expectedPinnedFromBeam1Reaction));
        assertThat(reactions.get(rollerJoint1.getId()), containsInAnyOrder(expectedRoller1SupportReaction, expectedRoller1FromBeam1Reaction));
        assertThat(reactions.get(hingeJoint1.getId()), containsInAnyOrder(expectedHinge1FromBeam1Reaction, expectedHinge1FromBeam2Reaction));
        assertThat(reactions.get(hingeJoint2.getId()), containsInAnyOrder(expectedHinge2FromBeam2Reaction, expectedHinge2FromBeam3Reaction));
        assertThat(reactions.get(rollerJoint2.getId()), containsInAnyOrder(expectedRoller2SupportReaction, expectedRoller2FromBeam3Reaction));
        assertThat(reactions.get(rollerJoint3.getId()), containsInAnyOrder(expectedRoller3SupportReaction, expectedRoller3FromBeam3Reaction));
        assertThat(reactions.get(beam1.getId()), containsInAnyOrder(expectedBeam1FromPinnedReaction, expectedBeam1FromRoller1Reaction,
                expectedBeam1FromHinge1Reaction));
        assertThat(reactions.get(beam2.getId()), containsInAnyOrder(expectedBeam2FromHinge1Reaction, expectedBeam2FromHinge2Reaction));
        assertThat(reactions.get(beam3.getId()), containsInAnyOrder(expectedBeam3FromHinge2Reaction, expectedBeam3FromRoller2Reaction,
                expectedBeam3FromRoller3Reaction));
    }

    private void thenResultHasDeterminateStructuralDeterminacy() {
        assertThat(result.getStructureDeterminacy(), equalTo(StaticalDeterminacy.DETERMINATE));
    }

    private void thenResultHasDeterminateProblemDeterminacy() {
        assertThat(result.getProblemDeterminacy(), equalTo(StaticalDeterminacy.DETERMINATE));
    }

    private void thenResultHasNullError() {
        assertThat(result.getError(), nullValue());
    }

    private void whenCalledWithSolvableOverdeterminedCompoundStructure() {
        Problem problem = createSolvableOverdeterminateCompoundStructure();
        result = solver.solve(problem);
    }

    private Problem createSolvableOverdeterminateCompoundStructure() {
        ConcentratedForce concentratedForce1 = new ConcentratedForce(new Vector(0, 5, 0), new Vector(-3, 0, 0));
        ConcentratedForce concentratedForce2 = new ConcentratedForce(new Vector(0, 6, 0), new Vector(-2, 0, 0));
        Moment moment = new Moment(new Vector(0, 0, 5));
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints rollerSupport = new Constraints.Builder().constrainTx().build();
        rollerJoint1 = new Joint.Builder("roller joint 1", new Vector(0, 0, 0), hinge).withSupport(rollerSupport).build();
        rollerJoint2 = new Joint.Builder("roller joint 2", new Vector(0, 3, 0), hinge).withSupport(rollerSupport).build();
        rollerJoint3 = new Joint.Builder("roller joint 3", new Vector(0, 7, 0), hinge).withSupport(rollerSupport).build();
        rollerJoint4 = new Joint.Builder("roller joint 4", new Vector(0, 10, 0), hinge).withSupport(rollerSupport).build();
        hingeJoint1 = new Joint.Builder("hinge joint 1", new Vector(0, 4, 0), new Constraints.Builder().constrainTx().constrainTy().build()).build();
        hingeJoint2 = new Joint.Builder("hinge joint 2", new Vector(0, 6, 0), new Constraints.Builder().constrainTx().constrainTy().build())
                .addConcentratedForce(concentratedForce2).build();
        beam1 = new Beam.Builder("beam 1").build();
        beam2 = new Beam.Builder("beam 2").addConcentratedForce(concentratedForce1).build();
        beam3 = new Beam.Builder("beam 3").addMoment(moment).build();

        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(rollerJoint1, beam1);
        builder.addBeam(rollerJoint2, beam1);
        builder.addBeams(hingeJoint1, beam1, beam2);
        builder.addBeams(hingeJoint2, beam2, beam3);
        builder.addBeam(rollerJoint3, beam3);
        builder.addBeam(rollerJoint4, beam3);
        return builder.build();
    }

    private void thenResultHasCorrectReactionsForSolvableOverdeterminateCompoundStructure() {
        Reaction expectedRoller1SupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(-0.5, 0, 0)),
                null);
        Reaction expectedRoller1FromBeam1Reaction = new Reaction(beam1.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(0.5, 0, 0)),
                null);
        Reaction expectedRoller2SupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(2, 0, 0)), null);
        Reaction expectedRoller2FromBeam1Reaction = new Reaction(beam1.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(-2, 0, 0)),
                null);
        Reaction expectedHinge1FromBeam1Reaction = new Reaction(beam1.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(1.5, 0, 0)),
                null);
        Reaction expectedHinge1FromBeam2Reaction = new Reaction(beam2.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(-1.5, 0, 0)),
                null);
        Reaction expectedHinge2FromBeam2Reaction = new Reaction(beam2.getId(), new ConcentratedForce(new Vector(0, 6, 0), new Vector(-1.5, 0, 0)),
                null);
        Reaction expectedHinge2FromBeam3Reaction = new Reaction(beam3.getId(), new ConcentratedForce(new Vector(0, 6, 0), new Vector(3.5, 0, 0)),
                null);
        Reaction expectedRoller3SupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 7, 0), new Vector(3, 0, 0)), null);
        Reaction expectedRoller3FromBeam3Reaction = new Reaction(beam3.getId(), new ConcentratedForce(new Vector(0, 7, 0), new Vector(-3.0, 0, 0)),
                null);
        Reaction expectedRoller4SupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 10, 0), new Vector(0.5, 0, 0)),
                null);
        Reaction expectedRoller4FromBeam3Reaction = new Reaction(beam3.getId(), new ConcentratedForce(new Vector(0, 10, 0), new Vector(-0.5, 0,
                0)), null);
        Reaction expectedBeam1FromRoller1Reaction = new Reaction(rollerJoint1.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(-0.5,
                0, 0)), null);
        Reaction expectedBeam1FromRoller2Reaction = new Reaction(rollerJoint2.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(2, 0,
                0)), null);
        Reaction expectedBeam1FromHinge1Reaction = new Reaction(hingeJoint1.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(-1.5, 0,
                0)), null);
        Reaction expectedBeam2FromHinge1Reaction = new Reaction(hingeJoint1.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(1.5, 0,
                0)), null);
        Reaction expectedBeam2FromHinge2Reaction = new Reaction(hingeJoint2.getId(), new ConcentratedForce(new Vector(0, 6, 0), new Vector(1.5, 0,
                0)), null);
        Reaction expectedBeam3FromHinge2Reaction = new Reaction(hingeJoint2.getId(), new ConcentratedForce(new Vector(0, 6, 0), new Vector(-3.5, 0,
                0)), null);
        Reaction expectedBeam3FromRoller3Reaction = new Reaction(rollerJoint3.getId(), new ConcentratedForce(new Vector(0, 7, 0), new Vector(3.0,
                0, 0)), null);
        Reaction expectedBeam3FromRoller4Reaction = new Reaction(rollerJoint4.getId(), new ConcentratedForce(new Vector(0, 10, 0), new Vector(0.5,
                0, 0)), null);

        Map<String, Collection<Reaction>> reactions = result.getReactions();
        assertThat(reactions.keySet(), containsInAnyOrder(rollerJoint1.getId(), rollerJoint2.getId(), rollerJoint3.getId(), rollerJoint4.getId(),
                hingeJoint1.getId(), hingeJoint2.getId(), beam1.getId(), beam2.getId(), beam3.getId()));
        assertThat(reactions.get(rollerJoint1.getId()), containsInAnyOrder(expectedRoller1SupportReaction, expectedRoller1FromBeam1Reaction));
        assertThat(reactions.get(rollerJoint2.getId()), containsInAnyOrder(expectedRoller2SupportReaction, expectedRoller2FromBeam1Reaction));
        assertThat(reactions.get(hingeJoint1.getId()), containsInAnyOrder(expectedHinge1FromBeam1Reaction, expectedHinge1FromBeam2Reaction));
        assertThat(reactions.get(hingeJoint2.getId()), containsInAnyOrder(expectedHinge2FromBeam2Reaction, expectedHinge2FromBeam3Reaction));
        assertThat(reactions.get(rollerJoint3.getId()), containsInAnyOrder(expectedRoller3SupportReaction, expectedRoller3FromBeam3Reaction));
        assertThat(reactions.get(rollerJoint4.getId()), containsInAnyOrder(expectedRoller4SupportReaction, expectedRoller4FromBeam3Reaction));
        assertThat(reactions.get(beam1.getId()), containsInAnyOrder(expectedBeam1FromRoller1Reaction, expectedBeam1FromRoller2Reaction,
                expectedBeam1FromHinge1Reaction));
        assertThat(reactions.get(beam2.getId()), containsInAnyOrder(expectedBeam2FromHinge1Reaction, expectedBeam2FromHinge2Reaction));
        assertThat(reactions.get(beam3.getId()), containsInAnyOrder(expectedBeam3FromHinge2Reaction, expectedBeam3FromRoller3Reaction,
                expectedBeam3FromRoller4Reaction));
    }

    private void thenResultHasOverdeterminateStructuralDeterminacy() {
        assertThat(result.getStructureDeterminacy(), equalTo(StaticalDeterminacy.OVERDETERMINATE));
    }

    private void whenCalledWithUnsolvableOverdeterminateCompoundStructure() {
        Problem problem = createUnsolvableOverdeterminateCompoundStructure();
        result = solver.solve(problem);
    }

    private Problem createUnsolvableOverdeterminateCompoundStructure() {
        ConcentratedForce concentratedForce1 = new ConcentratedForce(new Vector(0, 5, 0), new Vector(-3, 0, 0));
        ConcentratedForce concentratedForce2 = new ConcentratedForce(new Vector(0, 6, 0), new Vector(-2, -2, 0));
        Moment moment = new Moment(new Vector(0, 0, 5));
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints rollerSupport = new Constraints.Builder().constrainTx().build();
        rollerJoint1 = new Joint.Builder("roller joint 1", new Vector(0, 0, 0), hinge).withSupport(rollerSupport).build();
        rollerJoint2 = new Joint.Builder("roller joint 2", new Vector(0, 3, 0), hinge).withSupport(rollerSupport).build();
        rollerJoint3 = new Joint.Builder("roller joint 3", new Vector(0, 7, 0), hinge).withSupport(rollerSupport).build();
        rollerJoint4 = new Joint.Builder("roller joint 4", new Vector(0, 10, 0), hinge).withSupport(rollerSupport).build();
        hingeJoint1 = new Joint.Builder("hinge joint 1", new Vector(0, 4, 0), new Constraints.Builder().constrainTx().constrainTy().build()).build();
        hingeJoint2 = new Joint.Builder("hinge joint 2", new Vector(0, 6, 0), new Constraints.Builder().constrainTx().constrainTy().build())
                .addConcentratedForce(concentratedForce2).build();
        beam1 = new Beam.Builder("beam 1").build();
        beam2 = new Beam.Builder("beam 2").addConcentratedForce(concentratedForce1).build();
        beam3 = new Beam.Builder("beam 3").addMoment(moment).build();

        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(rollerJoint1, beam1);
        builder.addBeam(rollerJoint2, beam1);
        builder.addBeams(hingeJoint1, beam1, beam2);
        builder.addBeams(hingeJoint2, beam2, beam3);
        builder.addBeam(rollerJoint3, beam3);
        builder.addBeam(rollerJoint4, beam3);
        return builder.build();
    }

    private void thenResultHasNullReactions() {
        assertThat(result.getReactions(), nullValue());
    }

    private void thenResultHasOverdeterminateProblemDeterminacy() {
        assertThat(result.getProblemDeterminacy(), equalTo(StaticalDeterminacy.OVERDETERMINATE));
    }

    private void whenCalledWithIndeterminateCompoundStructure() {
        Problem problem = createIndeterminateCompoundStructure();
        result = solver.solve(problem);
    }

    private Problem createIndeterminateCompoundStructure() {
        ConcentratedForce concentratedForce1 = new ConcentratedForce(new Vector(0, 5, 0), new Vector(-3, 0, 0));
        ConcentratedForce concentratedForce2 = new ConcentratedForce(new Vector(0, 6, 0), new Vector(-2, -2, 0));
        Moment moment = new Moment(new Vector(0, 0, 5));
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints fixed = new Constraints.Builder().constrainTx().constrainTy().constrainRz().build();
        Constraints rollerSupport = new Constraints.Builder().constrainTx().build();
        fixedJoint = new Joint.Builder("fixed joint", new Vector(0, 0, 0), fixed).withSupport(fixed).build();
        rollerJoint1 = new Joint.Builder("roller joint 1", new Vector(0, 3, 0), hinge).withSupport(rollerSupport).build();
        rollerJoint2 = new Joint.Builder("roller joint 2", new Vector(0, 7, 0), hinge).withSupport(rollerSupport).build();
        rollerJoint3 = new Joint.Builder("roller joint 3", new Vector(0, 10, 0), hinge).withSupport(rollerSupport).build();
        hingeJoint1 = new Joint.Builder("hinge joint 1", new Vector(0, 4, 0), hinge).build();
        hingeJoint2 = new Joint.Builder("hinge joint 2", new Vector(0, 6, 0), hinge).addConcentratedForce(concentratedForce2).build();
        beam1 = new Beam.Builder("beam 1").build();
        beam2 = new Beam.Builder("beam 2").addConcentratedForce(concentratedForce1).build();
        beam3 = new Beam.Builder("beam 3").addMoment(moment).build();

        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(fixedJoint, beam1);
        builder.addBeam(rollerJoint1, beam1);
        builder.addBeams(hingeJoint1, beam1, beam2);
        builder.addBeams(hingeJoint2, beam2, beam3);
        builder.addBeam(rollerJoint2, beam3);
        builder.addBeam(rollerJoint3, beam3);
        return builder.build();
    }

    private void thenResultHasIndeterminateStructuralDeterminacy() {
        assertThat(result.getStructureDeterminacy(), equalTo(StaticalDeterminacy.INDETERMINATE));
    }

    private void thenResultHasIndeterminateProblemDeterminacy() {
        assertThat(result.getProblemDeterminacy(), equalTo(StaticalDeterminacy.INDETERMINATE));
    }

    private void whenCalledWithCompoundStructureWithFixedHinge() {
        Problem problem = createCompoundStructureWithFixedHinge();
        result = solver.solve(problem);
    }

    private Problem createCompoundStructureWithFixedHinge() {
        ConcentratedForce concentratedForce1 = new ConcentratedForce(new Vector(0, 5, 0), new Vector(-3, 0, 0));
        ConcentratedForce concentratedForce2 = new ConcentratedForce(new Vector(0, 6, 0), new Vector(-2, -2, 0));
        Moment moment = new Moment(new Vector(0, 0, 5));
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints fixed = new Constraints.Builder().constrainTx().constrainTy().constrainRz().build();
        Constraints pinnedSupport = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints rollerSupport = new Constraints.Builder().constrainTx().build();
        pinnedJoint = new Joint.Builder("pinned joint", new Vector(0, 0, 0), hinge).withSupport(pinnedSupport).build();
        rollerJoint1 = new Joint.Builder("roller joint 1", new Vector(0, 3, 0), hinge).withSupport(rollerSupport).build();
        rollerJoint2 = new Joint.Builder("roller joint 2", new Vector(0, 7, 0), hinge).withSupport(rollerSupport).build();
        fixedHinge = new Joint.Builder("fixed hinge", new Vector(0, 4, 0), fixed).build();
        hingeJoint1 = new Joint.Builder("hinge joint 1", new Vector(0, 6, 0), hinge).addConcentratedForce(concentratedForce2).build();
        beam1 = new Beam.Builder("beam 1").build();
        beam2 = new Beam.Builder("beam 2").addConcentratedForce(concentratedForce1).build();
        beam3 = new Beam.Builder("beam 3").addMoment(moment).build();

        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(pinnedJoint, beam1);
        builder.addBeam(rollerJoint1, beam1);
        builder.addBeams(fixedHinge, beam1, beam2);
        builder.addBeams(hingeJoint1, beam2, beam3);
        builder.addBeam(rollerJoint2, beam3);
        return builder.build();
    }

    private void thenResultHasCorrectReactionsForCompoundStructureWithFixedHinge() {
        Reaction expectedPinnedSupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(1, 2, 0)), null);
        Reaction expectedPinnedFromBeam1Reaction = new Reaction(beam1.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(-1, -2, 0)),
                null);
        Reaction expectedRoller1SupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(-1, 0, 0)),
                null);
        Reaction expectedRoller1FromBeam1Reaction = new Reaction(beam1.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(1, 0, 0)),
                null);
        Reaction expectedFixedHingeFromBeam1Reaction = new Reaction(beam1.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(0, 2, 0)),
                new Moment(new Vector(0, 0, 3)));
        Reaction expectedFixedHingeFromBeam2Reaction = new Reaction(beam2.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(0, -2, 0)),
                new Moment(new Vector(0, 0, -3)));
        Reaction expectedHingeFromBeam2Reaction = new Reaction(beam2.getId(), new ConcentratedForce(new Vector(0, 6, 0), new Vector(-3, 2, 0)), null);
        Reaction expectedHingeFromBeam3Reaction = new Reaction(beam3.getId(), new ConcentratedForce(new Vector(0, 6, 0), new Vector(5, 0, 0)), null);
        Reaction expectedRoller2SupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 7, 0), new Vector(5, 0, 0)), null);
        Reaction expectedRoller2FromBeam3Reaction = new Reaction(beam3.getId(), new ConcentratedForce(new Vector(0, 7, 0), new Vector(-5, 0, 0)),
                null);
        Reaction expectedBeam1FromPinnedReaction = new Reaction(pinnedJoint.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(1, 2,
                0)), null);
        Reaction expectedBeam1FromRoller1Reaction = new Reaction(rollerJoint1.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(-1, 0,
                0)), null
        );
        Reaction expectedBeam1FromFixedHingeReaction = new Reaction(fixedHinge.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(0,
                -2, 0)), new Moment(new Vector(0, 0, -3))
        );
        Reaction expectedBeam2FromFixedHingeReaction = new Reaction(fixedHinge.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(0, 2,
                0)), new Moment(new Vector(0, 0, 3))
        );
        Reaction expectedBeam2FromHingeReaction = new Reaction(hingeJoint1.getId(), new ConcentratedForce(new Vector(0, 6, 0), new Vector(3, -2,
                0)), null);
        Reaction expectedBeam3FromHingeReaction = new Reaction(hingeJoint1.getId(), new ConcentratedForce(new Vector(0, 6, 0), new Vector(-5, 0,
                0)), null);
        Reaction expectedBeam3FromRoller2Reaction = new Reaction(rollerJoint2.getId(), new ConcentratedForce(new Vector(0, 7, 0), new Vector(5, 0,
                0)), null);

        Map<String, Collection<Reaction>> reactions = result.getReactions();
        assertThat(reactions.keySet(), containsInAnyOrder(pinnedJoint.getId(), rollerJoint1.getId(), rollerJoint2.getId(), fixedHinge.getId(),
                hingeJoint1.getId(), beam1.getId(), beam2.getId(), beam3.getId()));
        assertThat(reactions.get(pinnedJoint.getId()), containsInAnyOrder(expectedPinnedSupportReaction, expectedPinnedFromBeam1Reaction));
        assertThat(reactions.get(rollerJoint1.getId()), containsInAnyOrder(expectedRoller1SupportReaction, expectedRoller1FromBeam1Reaction));
        assertThat(reactions.get(fixedHinge.getId()), containsInAnyOrder(expectedFixedHingeFromBeam1Reaction, expectedFixedHingeFromBeam2Reaction));
        assertThat(reactions.get(hingeJoint1.getId()), containsInAnyOrder(expectedHingeFromBeam2Reaction, expectedHingeFromBeam3Reaction));
        assertThat(reactions.get(rollerJoint2.getId()), containsInAnyOrder(expectedRoller2SupportReaction, expectedRoller2FromBeam3Reaction));
        assertThat(reactions.get(beam1.getId()), containsInAnyOrder(expectedBeam1FromPinnedReaction, expectedBeam1FromRoller1Reaction,
                expectedBeam1FromFixedHingeReaction));
        assertThat(reactions.get(beam2.getId()), containsInAnyOrder(expectedBeam2FromFixedHingeReaction, expectedBeam2FromHingeReaction));
        assertThat(reactions.get(beam3.getId()), containsInAnyOrder(expectedBeam3FromHingeReaction, expectedBeam3FromRoller2Reaction));
    }

}
