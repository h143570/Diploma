package org.gabs.statics.solver.resultbuilder.postprocessor;

import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.model.result.Reaction;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;

public class FilteringPostProcessorTest {

    private static final int PRECISION = 14;
    private static final String ELEMENT_1 = "element 1";
    private static final ConcentratedForce ZERO_FORCE = new ConcentratedForce(new Vector(0, 0, 0), new Vector(0, 0, 0));
    private static final Moment ZERO_MOMENT = new Moment(new Vector(0, 0, 0));
    private static final String ELEMENT_2 = "element 2";
    private static final ConcentratedForce FORCE = new ConcentratedForce(new Vector(0, 0, 0), new Vector(1, 0, 0));
    private static final Moment MOMENT = new Moment(new Vector(0, 0, 1));
    private FilteringPostProcessor postProcessor;
    private Map<String, Collection<Reaction>> reactions;

    @Test
    public void testPostProcessWithNullForceAndNullMomentShouldRemoveReaction() {
        givenFilteringPostProcessor();
        whenCalledWithNullForceAndNullMoment();
        thenZeroAndNullReactionsShouldBeRemoved();
    }

    @Test
    public void testPostProcessWithNullForceAndZeroMomentShouldRemoveReaction() {
        givenFilteringPostProcessor();
        whenCalledWithNullForceAndZeroMoment();
        thenZeroAndNullReactionsShouldBeRemoved();
    }

    @Test
    public void testPostProcessWithZeroForceAndNullMomentShouldRemoveReaction() {
        givenFilteringPostProcessor();
        whenCalledWithZeroForceAndNullMoment();
        thenZeroAndNullReactionsShouldBeRemoved();
    }

    @Test
    public void testPostProcessWithZeroForceAndZeroMomentShouldRemoveReaction() {
        givenFilteringPostProcessor();
        whenCalledWithZeroForceZeroNullMoment();
        thenZeroAndNullReactionsShouldBeRemoved();
    }

    @Test
    public void testPostProcessWithoutNullVectorsShouldNotRemoveReactions() {
        givenFilteringPostProcessor();
        whenCalledWithoutNullVectors();
        thenShouldNotRemoveReactions();
    }

    private void givenFilteringPostProcessor() {
        postProcessor = new FilteringPostProcessor();
    }

    private void whenCalledWithNullForceAndNullMoment() {
        reactions = new HashMap<>(2, 1);
        reactions.put(ELEMENT_1, new ArrayList<>(Arrays.asList(new Reaction(ELEMENT_1, FORCE, MOMENT), new Reaction(ELEMENT_2, null, null))));
        reactions.put(ELEMENT_2, new ArrayList<>(Arrays.asList(new Reaction(ELEMENT_2, FORCE, MOMENT), new Reaction(ELEMENT_1, null, null))));
        postProcessor.postProcess(reactions, PRECISION);
    }

    private void thenZeroAndNullReactionsShouldBeRemoved() {
        Reaction expectedReaction1 = new Reaction(ELEMENT_1, FORCE, MOMENT);
        Reaction expectedReaction2 = new Reaction(ELEMENT_2, FORCE, MOMENT);
        assertThat(reactions.keySet(), containsInAnyOrder(ELEMENT_1, ELEMENT_2));
        assertThat(reactions.get(ELEMENT_1), contains(expectedReaction1));
        assertThat(reactions.get(ELEMENT_2), contains(expectedReaction2));
    }

    private void whenCalledWithNullForceAndZeroMoment() {
        reactions = new HashMap<>(2, 1);
        reactions.put(ELEMENT_1, new ArrayList<>(Arrays.asList(new Reaction(ELEMENT_1, FORCE, MOMENT), new Reaction(ELEMENT_2, null, ZERO_MOMENT))));
        reactions.put(ELEMENT_2, new ArrayList<>(Arrays.asList(new Reaction(ELEMENT_2, FORCE, MOMENT), new Reaction(ELEMENT_1, null, ZERO_MOMENT))));
        postProcessor.postProcess(reactions, PRECISION);
    }

    private void whenCalledWithZeroForceAndNullMoment() {
        reactions = new HashMap<>(2, 1);
        reactions.put(ELEMENT_1, new ArrayList<>(Arrays.asList(new Reaction(ELEMENT_1, FORCE, MOMENT), new Reaction(ELEMENT_2, ZERO_FORCE, null))));
        reactions.put(ELEMENT_2, new ArrayList<>(Arrays.asList(new Reaction(ELEMENT_2, FORCE, MOMENT), new Reaction(ELEMENT_1, ZERO_FORCE, null))));
        postProcessor.postProcess(reactions, PRECISION);
    }

    private void whenCalledWithZeroForceZeroNullMoment() {
        reactions = new HashMap<>(2, 1);
        reactions.put(ELEMENT_1, new ArrayList<>(Arrays.asList(new Reaction(ELEMENT_1, FORCE, MOMENT), new Reaction(ELEMENT_2, ZERO_FORCE,
                ZERO_MOMENT))));
        reactions.put(ELEMENT_2, new ArrayList<>(Arrays.asList(new Reaction(ELEMENT_2, FORCE, MOMENT), new Reaction(ELEMENT_1, ZERO_FORCE,
                ZERO_MOMENT))));
        postProcessor.postProcess(reactions, PRECISION);
    }

    private void whenCalledWithoutNullVectors() {
        reactions = new HashMap<>(2, 1);
        reactions.put(ELEMENT_1, Arrays.asList(new Reaction(ELEMENT_1, null, MOMENT), new Reaction(ELEMENT_2, FORCE, null)));
        reactions.put(ELEMENT_2, Arrays.asList(new Reaction(ELEMENT_1, FORCE, ZERO_MOMENT), new Reaction(ELEMENT_2, ZERO_FORCE, MOMENT)));
        postProcessor.postProcess(reactions, PRECISION);
    }

    private void thenShouldNotRemoveReactions() {
        Reaction expectedReaction11 = new Reaction(ELEMENT_1, null, MOMENT);
        Reaction expectedReaction12 = new Reaction(ELEMENT_2, FORCE, null);
        Reaction expectedReaction21 = new Reaction(ELEMENT_1, FORCE, ZERO_MOMENT);
        Reaction expectedReaction22 = new Reaction(ELEMENT_2, ZERO_FORCE, MOMENT);
        assertThat(reactions.keySet(), containsInAnyOrder(ELEMENT_1, ELEMENT_2));
        assertThat(reactions.get(ELEMENT_1), containsInAnyOrder(expectedReaction11, expectedReaction12));
        assertThat(reactions.get(ELEMENT_2), containsInAnyOrder(expectedReaction21, expectedReaction22));
    }

}
