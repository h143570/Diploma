package org.gabs.statics.solver.validator;

import org.gabs.statics.model.problem.Beam;
import org.gabs.statics.model.problem.Joint;
import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.solver.data.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StructuralContinuityValidatorTest {

    private static final String ERROR = "Structure must be continuous.";

    @InjectMocks private StructuralContinuityValidator validator;
    @Mock private Joint joint1;
    @Mock private Joint joint2;
    @Mock private Joint joint3;
    @Mock private Joint joint4;
    @Mock private Beam beam1;
    @Mock private Beam beam2;
    @Mock private Beam beam3;
    private Error result;

    @Before
    public void setup() {
        when(joint1.getId()).thenReturn("joint 1");
        when(joint2.getId()).thenReturn("joint 2");
        when(joint3.getId()).thenReturn("joint 3");
        when(joint4.getId()).thenReturn("joint 4");
        when(beam1.getId()).thenReturn("beam 1");
        when(beam2.getId()).thenReturn("beam 2");
        when(beam2.getId()).thenReturn("beam 3");
    }

    @Test
    public void testValidateEmptyProblemShouldPass() {
        whenCalledValidateWithEmptyProblem();
        thenErrorIsNull();
    }

    @Test
    public void testValidateContinuousStructureShouldPass() {
        whenCalledValidateWithContinuousStructure();
        thenErrorIsNull();
    }

    @Test
    public void testValidateNotContinuousStructureShouldNotPass() {
        whenCalledValidateWithNotContinuousStructure();
        thenResultShouldContainError();
    }

    private void whenCalledValidateWithEmptyProblem() {
        Problem problem = new Problem.Builder().build();
        result = validator.validate(problem);
    }

    private void thenErrorIsNull() {
        assertThat(result, nullValue());
    }

    private void whenCalledValidateWithContinuousStructure() {
        Problem problem = createContinuousStructure();
        result = validator.validate(problem);
    }

    private Problem createContinuousStructure() {
        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(joint1, beam1);
        builder.addBeams(joint2, beam1, beam2);
        builder.addBeams(joint3, beam2, beam3);
        builder.addBeam(joint4, beam3);
        return builder.build();
    }

    private void whenCalledValidateWithNotContinuousStructure() {
        Problem problem = createNotContinuousStructure();
        result = validator.validate(problem);
    }

    private Problem createNotContinuousStructure() {
        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(joint1, beam1);
        builder.addBeam(joint2, beam1);
        builder.addBeam(joint3, beam2);
        builder.addBeam(joint4, beam2);
        return builder.build();
    }

    private void thenResultShouldContainError() {
        assertThat(result.getDescription(), equalTo(ERROR));
    }

}
