package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.problem.Element;
import org.gabs.statics.model.problem.Beam;
import org.gabs.statics.model.problem.Joint;
import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.solver.converter.Converter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ElementMappingBuilderImplTest {

    @InjectMocks private ElementMappingBuilderImpl builder;
    @Mock private Converter<Element, org.gabs.statics.solver.model.Element> elementConverter;
    @Mock private Joint joint;
    @Mock private Beam beam;
    @Mock private org.gabs.statics.solver.model.Element convertedJoint;
    @Mock private org.gabs.statics.solver.model.Element convertedBeam;
    private Map<Element, org.gabs.statics.solver.model.Element> result;

    @Test
    public void testBuildWithEmptyProblemShouldReturnEmptyMap() {
        whenBuildCalledWithEmptyProblem();
        thenResultShouldBeEmpty();
    }

    @Test
    public void testBuildWithProblemShouldReturnElements() {
        whenBuildCalledWithProblem();
        thenResultShouldHaveCorrectElements();
    }

    private void whenBuildCalledWithEmptyProblem() {
        Problem problem = new Problem.Builder().build();
        result = builder.build(problem);
    }

    private void thenResultShouldBeEmpty() {
        assertThat(result.isEmpty(), equalTo(true));
    }

    private void whenBuildCalledWithProblem() {
        builder.setElementConverter(elementConverter);
        when(elementConverter.convert(joint)).thenReturn(convertedJoint);
        when(elementConverter.convert(beam)).thenReturn(convertedBeam);
        when(joint.getId()).thenReturn("joint");
        when(beam.getId()).thenReturn("beam");
        Problem problem = new Problem.Builder().addBeam(joint, beam).build();
        result = builder.build(problem);
    }

    private void thenResultShouldHaveCorrectElements() {
        assertThat(result.keySet(), hasSize(2));
        assertThat(result.get(joint), equalTo(convertedJoint));
        assertThat(result.get(beam), equalTo(convertedBeam));
    }

}
