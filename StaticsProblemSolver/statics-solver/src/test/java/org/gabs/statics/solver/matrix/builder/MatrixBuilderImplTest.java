package org.gabs.statics.solver.matrix.builder;

import org.gabs.statics.solver.matrix.builder.processor.ElementProcessor;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Effect;
import org.gabs.statics.solver.model.Element;
import org.gabs.statics.solver.model.Problem;
import org.gabs.statics.solver.model.Structure;
import org.gabs.statics.solver.registry.ConstraintRegistry;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MatrixBuilderImplTest {

    @InjectMocks private MatrixBuilderImpl builder;
    @Mock private ElementProcessor elementProcessor;
    @Mock private ConstraintRegistry constraintRegistry;
    @Mock private Problem problem;
    @Mock private Element element1;
    @Mock private Element element2;
    @Mock private Collection<Constraint> constraints11;
    @Mock private Collection<Constraint> constraints12;
    @Mock private Collection<Constraint> constraints21;
    @Mock private Collection<Effect> load1;
    @Mock private Collection<Effect> load2;
    private Structure structure;
    private double[] coefficients1X;
    private double[] coefficients1Y;
    private double[] coefficients1ZZ;
    private double[] coefficients2X;
    private double[] coefficients2Y;
    private double[] coefficients2ZZ;
    private double[][] result;

    @Before
    public void setup() {
        createCoefficients();
    }

    @Test
    public void testBuildShouldCallElementProcessorWithEveryElementOfStructureAndEveryLoad() {
        whenElementProcessorReturnsCoefficients();
        whenCalledWithProblem();
        thenBuilderShouldCallElementProcessorWithEveryElementOfStructureTable();
    }

    @Test
    public void testBuildShouldReturnProcessedCoefficientArrays() {
        whenElementProcessorReturnsCoefficients();
        whenCalledWithProblem();
        thenBuilderShouldReturnProcessedCoefficientArrays();
    }

    private Structure createStructure() {
        Structure result = new Structure();
        result.addExternalConstraints(element1, constraints11);
        result.addInternalConstraints(element1, element2, constraints12);
        result.addInternalConstraints(element2, element1, constraints21);
        return result;
    }

    private Map<Element, Collection<Effect>> createLoad() {
        Map<Element, Collection<Effect>> result = new HashMap<>(2, 1);
        result.put(element1, load1);
        result.put(element2, load2);
        return result;
    }

    private void createCoefficients() {
        coefficients1X = new double[]{1, 1, 0, 0};
        coefficients1Y = new double[]{1, 0, 1, 0};
        coefficients1ZZ = new double[]{1, 1, 0, 0};
        coefficients2X = new double[]{2, 1, 0, 0};
        coefficients2Y = new double[]{2, 0, 1, 0};
        coefficients2ZZ = new double[]{2, 0, 0, 1};
    }

    private void whenCalledWithProblem() {
        when(problem.getStructure()).thenReturn(structure);
        when(problem.getLoad()).thenReturn(createLoad());
        result = builder.build(problem, constraintRegistry);
    }

    private void thenBuilderShouldCallElementProcessorWithEveryElementOfStructureTable() {
        verify(elementProcessor).process(element1, structure, load1, constraintRegistry);
        verify(elementProcessor).process(element2, structure, load2, constraintRegistry);
        verifyNoMoreInteractions(elementProcessor);
    }

    private void whenElementProcessorReturnsCoefficients() {
        Collection<double[]> coefficients1 = new ArrayList<>(3);
        coefficients1.add(coefficients1X);
        coefficients1.add(coefficients1Y);
        coefficients1.add(coefficients1ZZ);

        Collection<double[]> coefficients2 = new ArrayList<>(3);
        coefficients2.add(coefficients2X);
        coefficients2.add(coefficients2Y);
        coefficients2.add(coefficients2ZZ);

        structure = createStructure();
        when(elementProcessor.process(element1, structure, load1, constraintRegistry)).thenReturn(coefficients1);
        when(elementProcessor.process(element2, structure, load2, constraintRegistry)).thenReturn(coefficients2);
    }

    private void thenBuilderShouldReturnProcessedCoefficientArrays() {
        assertThat(result.length, equalTo(6));
        assertThat(Arrays.asList(result), containsInAnyOrder(coefficients1X, coefficients1Y, coefficients1ZZ, coefficients2X, coefficients2Y,
                coefficients2ZZ));
    }

}
