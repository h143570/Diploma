package org.gabs.statics.solver.matrix.builder.processor;

import org.gabs.statics.solver.matrix.builder.processor.ElementProcessorImpl;
import org.gabs.statics.solver.matrix.builder.processor.constraint.ConstraintProcessor;
import org.gabs.statics.solver.matrix.builder.processor.load.LoadProcessor;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.Effect;
import org.gabs.statics.solver.model.Element;
import org.gabs.statics.solver.model.Structure;
import org.gabs.statics.solver.registry.ConstraintRegistry;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasKey;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ElementProcessorImplTest {

    private static final int CONSTRAINT_1_INDEX = 0;
    private static final int CONSTRAINT_2_INDEX = 2;
    private static final int CONSTRAINT_3_INDEX = 3;
    private static final int CONSTRAINT_4_INDEX = 4;

    @InjectMocks private ElementProcessorImpl processor;
    @Mock private ConstraintProcessor constraintProcessor;
    @Mock private LoadProcessor loadProcessor;
    @Mock private ConstraintRegistry constraintRegistry;
    @Mock private Element element;
    @Mock private Element connectedElement;
    @Mock private Constraint constraint1;
    @Mock private Constraint constraint2;
    @Mock private Constraint constraint3;
    @Mock private Constraint constraint4;
    @Mock private Effect effect1;
    @Mock private Effect effect2;
    @Captor private ArgumentCaptor<Map<Dof, double[]>> coefficients;
    private Structure structure;
    private double[] coefficientsX;
    private double[] coefficientsY;
    private double[] coefficientsZZ;
    private double[] coefficientsAllZeroes;
    private Collection<double[]> result;


    @Before
    public void setup() {
        createCoefficients();
    }

    @Test
    public void testProcessWithJointShouldCallConstraintProcessorWithJointFlagTrue() {
        givenElementProcessorImpl();
        whenCalledWithJoint();
        thenProcessorShouldCallConstraintProcessorWithJointFlagTrue();
    }

    @Test
    public void testProcessWithBeamShouldCallConstraintProcessorWithJointFlagFalse() {
        givenElementProcessorImpl();
        whenCalledWithBeam();
        thenProcessorShouldCallConstraintProcessorWithJointFlagFalse();
    }

    @Test
    public void testProcessWithNullLoadShouldNotCallLoadProcessor() {
        givenElementProcessorImpl();
        whenCalledWithNullLoad();
        thenProcessorShouldNotCallLoadProcessor();
    }

    @Test
    public void testProcessWithFourConstraintsShouldCallConstraintProcessorWithCoefficientArraysLengthOfFive() {
        givenElementProcessorImpl();
        whenCalledWithFourConstraints();
        thenProcessorShouldCallConstraintProcessorWithCoefficientArraysLengthOfFive();
    }

    @Test
    public void testProcessWithFourConstraintsShouldReturnCoefficientsForEveryDof() {
        givenElementProcessorImpl();
        whenCalledWithFourConstraints();
        thenProcessorShouldCallConstraintProcessorWithCoefficientsForEveryDof();
    }

    @Test
    public void testProcessWithLoadShouldCallLoadProcessorWithEachLoad() {
        givenElementProcessorImpl();
        whenCalledWithLoadOnJoint();
        thenProcessorShouldCallLoadProcessorWithEachLoad();
    }

    @Test
    public void testProcessShouldReturnProcessedArrays() {
        givenElementProcessorImpl();
        whenLoadProcessorUpdatesCoefficients();
        whenCalledWithLoadOnJoint();
        thenProcessorShouldReturnProcessedArrays();
    }

    @Test
    public void testProcessShouldReturnNonZeroProcessedArrays() {
        givenElementProcessorImpl();
        whenLoadProcessorUpdatesCoefficientsWithZeros();
        whenCalledWithLoadOnJoint();
        thenProcessorShouldReturnNonZeroProcessedArrays();
    }

    private void mockConstraintRegistry() {
        when(constraintRegistry.getIndex(constraint1)).thenReturn(CONSTRAINT_1_INDEX);
        when(constraintRegistry.getIndex(constraint2)).thenReturn(CONSTRAINT_2_INDEX);
        when(constraintRegistry.getIndex(constraint3)).thenReturn(CONSTRAINT_3_INDEX);
        when(constraintRegistry.getIndex(constraint4)).thenReturn(CONSTRAINT_4_INDEX);
        when(constraintRegistry.size()).thenReturn(4);
    }

    private void createCoefficients() {
        coefficientsX = new double[]{1, 1, 0, 0};
        coefficientsY = new double[]{1, 0, 1, 0};
        coefficientsZZ = new double[]{1, 1, 0, 0};
        coefficientsAllZeroes = new double[]{0, 0, 0, 0};
    }

    private void givenElementProcessorImpl() {
        structure = createStructure();
        mockConstraintRegistry();
    }

    private void whenCalledWithJoint() {
        when(element.isJoint()).thenReturn(true);
        processor.process(element, structure, null, constraintRegistry);
    }

    private Structure createStructure() {
        Structure structure = new Structure();
        structure.addExternalConstraints(element, Arrays.asList(constraint1, constraint2));
        structure.addInternalConstraints(element, connectedElement, Arrays.asList(constraint3, constraint4));
        return structure;
    }

    @SuppressWarnings("unchecked")
    private void thenProcessorShouldCallConstraintProcessorWithJointFlagTrue() {
        verify(constraintProcessor).process(eq(constraint1), eq(CONSTRAINT_1_INDEX), eq(true), any(Map.class));
        verify(constraintProcessor).process(eq(constraint2), eq(CONSTRAINT_2_INDEX), eq(true), any(Map.class));
        verify(constraintProcessor).process(eq(constraint3), eq(CONSTRAINT_3_INDEX), eq(true), any(Map.class));
        verify(constraintProcessor).process(eq(constraint4), eq(CONSTRAINT_4_INDEX), eq(true), any(Map.class));
    }

    private void whenCalledWithBeam() {
        when(element.isJoint()).thenReturn(false);
        processor.process(element, structure, null, constraintRegistry);
    }

    @SuppressWarnings("unchecked")
    private void thenProcessorShouldCallConstraintProcessorWithJointFlagFalse() {
        verify(constraintProcessor).process(eq(constraint1), eq(CONSTRAINT_1_INDEX), eq(false), any(Map.class));
        verify(constraintProcessor).process(eq(constraint2), eq(CONSTRAINT_2_INDEX), eq(false), any(Map.class));
        verify(constraintProcessor).process(eq(constraint3), eq(CONSTRAINT_3_INDEX), eq(false), any(Map.class));
        verify(constraintProcessor).process(eq(constraint4), eq(CONSTRAINT_4_INDEX), eq(false), any(Map.class));
    }

    private void whenCalledWithNullLoad() {
        processor.process(element, structure, null, constraintRegistry);
    }

    private void thenProcessorShouldNotCallLoadProcessor() {
        verifyZeroInteractions(loadProcessor);
    }

    private void whenCalledWithFourConstraints() {
        result = processor.process(element, structure, null, constraintRegistry);
    }

    private void thenProcessorShouldCallConstraintProcessorWithCoefficientArraysLengthOfFive() {
        verify(constraintProcessor).process(eq(constraint1), eq(CONSTRAINT_1_INDEX), any(Boolean.class), coefficients.capture());
        assertThat(coefficients.getValue().get(Dof.X).length, equalTo(5));
        assertThat(coefficients.getValue().get(Dof.Y).length, equalTo(5));
        assertThat(coefficients.getValue().get(Dof.ZZ).length, equalTo(5));
    }

    private void thenProcessorShouldCallConstraintProcessorWithCoefficientsForEveryDof() {
        verify(constraintProcessor).process(eq(constraint1), eq(CONSTRAINT_1_INDEX), any(Boolean.class), coefficients.capture());
        assertThat(coefficients.getValue().size(), Matchers.equalTo(3));
        assertThat(coefficients.getValue(), hasKey(Dof.X));
        assertThat(coefficients.getValue(), hasKey(Dof.Y));
        assertThat(coefficients.getValue(), hasKey(Dof.ZZ));
    }

    private void whenCalledWithLoadOnJoint() {
        when(element.isJoint()).thenReturn(true);
        result = processor.process(element, structure, Arrays.asList(effect1, effect2), constraintRegistry);
    }

    @SuppressWarnings("unchecked")
    private void thenProcessorShouldCallLoadProcessorWithEachLoad() {
        verify(loadProcessor).process(eq(effect1), any(Map.class), eq(true));
        verify(loadProcessor).process(eq(effect2), any(Map.class), eq(true));
        verifyNoMoreInteractions(loadProcessor);
    }

    @SuppressWarnings("unchecked")
    private void whenLoadProcessorUpdatesCoefficients() {
        doAnswer(coefficients(coefficientsX, coefficientsY, coefficientsZZ)).when(loadProcessor).process(eq(effect1), any(Map.class), eq(true));
    }

    @SuppressWarnings("unchecked")
    private Answer<Map<Dof, double[]>> coefficients(final double[] coefficientsX, final double[] coefficientsY, final double[] coefficientsZZ) {
        return new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Map<Dof, double[]> coefficientArg = (Map<Dof, double[]>) invocation.getArguments()[1];
                coefficientArg.put(Dof.X, coefficientsX);
                coefficientArg.put(Dof.Y, coefficientsY);
                coefficientArg.put(Dof.ZZ, coefficientsZZ);
                return null;
            }
        };
    }

    private void thenProcessorShouldReturnProcessedArrays() {
        assertThat(result, containsInAnyOrder(coefficientsX, coefficientsY, coefficientsZZ));
    }

    @SuppressWarnings("unchecked")
    private void whenLoadProcessorUpdatesCoefficientsWithZeros() {
        doAnswer(coefficients(coefficientsX, coefficientsAllZeroes, coefficientsZZ)).when(loadProcessor).process(eq(effect1), any(Map.class),
                eq(true));
    }

    private void thenProcessorShouldReturnNonZeroProcessedArrays() {
        assertThat(result, containsInAnyOrder(coefficientsX, coefficientsZZ));
    }

}
