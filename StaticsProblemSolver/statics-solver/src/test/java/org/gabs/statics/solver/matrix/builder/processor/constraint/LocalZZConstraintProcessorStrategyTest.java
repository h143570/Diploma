package org.gabs.statics.solver.matrix.builder.processor.constraint;

import org.gabs.statics.solver.data.Vector;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.ReferenceFrame;
import org.junit.Before;
import org.junit.Test;

import java.util.EnumMap;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class LocalZZConstraintProcessorStrategyTest {

    private static final int INDEX = 1;
    private static final Vector POSITION = new Vector(5, 10, 0);
    private static final double LOCAL_REFERENCE_FRAME_ROTATION = Math.PI / 6;
    private static final ReferenceFrame REFERENCE_FRAME = createLocalReferenceFrame();
    private static final double LOCAL_X_DISTANCE = POSITION.crossProduct(REFERENCE_FRAME.getJ()).length();
    private static final double LOCAL_Y_DISTANCE = POSITION.crossProduct(REFERENCE_FRAME.getI()).length();

    private LocalZZConstraintProcessorStrategy strategy;
    private Map<Dof, double[]> coefficients;

    private static ReferenceFrame createLocalReferenceFrame() {
        double sin = Math.sin(LOCAL_REFERENCE_FRAME_ROTATION);
        double cos = Math.cos(LOCAL_REFERENCE_FRAME_ROTATION);
        Vector x = new Vector(cos, -sin, 0);
        Vector y = new Vector(sin, cos, 0);
        Vector z = new Vector(0, 0, 1);
        return new ReferenceFrame(x, y, z);
    }

    @Before
    public void setup() {
        coefficients = createBlankCoefficientsMap();
    }

    @Test
    public void testProcessWithJointConstrainedOnZZShouldPutOneAsCoefficientAtIndex() {
        givenLocalZZConstraintProcessorStrategy();
        whenCalledWithJointConstrainedOnZZ();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeOneAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithBeamConstrainedOnZZShouldPutMinusOneAsCoefficientAtIndex() {
        givenLocalZZConstraintProcessorStrategy();
        whenCalledWithBeamConstrainedOnZZ();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeMinusOneAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithJointConstrainedOnXShouldPutZeroAsCoefficientAtIndex() {
        givenLocalZZConstraintProcessorStrategy();
        whenCalledWithJointConstrainedOnX();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeZeroAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithBeamConstrainedOnXShouldPutLocalYDistanceAsCoefficientAtIndex() {
        givenLocalZZConstraintProcessorStrategy();
        whenCalledWithBeamConstrainedOnX();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeLocalYDistanceAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithJointConstrainedOnYShouldPutZeroAsCoefficientAtIndex() {
        givenLocalZZConstraintProcessorStrategy();
        whenCalledWithJointConstrainedOnY();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeZeroAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithBeamConstrainedOnYShouldPutLocalXDistanceAsCoefficientAtIndex() {
        givenLocalZZConstraintProcessorStrategy();
        whenCalledWithBeamConstrainedOnY();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeLocalXDistanceAtIndexInZZCoefficients();
    }

    private Map<Dof, double[]> createBlankCoefficientsMap() {
        Map<Dof, double[]> result = new EnumMap<>(Dof.class);
        result.put(Dof.X, new double[]{0, 0, 0});
        result.put(Dof.Y, new double[]{0, 0, 0});
        result.put(Dof.ZZ, new double[]{0, 0, 0});
        return result;
    }

    private void givenLocalZZConstraintProcessorStrategy() {
        strategy = new LocalZZConstraintProcessorStrategy();
    }

    private void whenCalledWithJointConstrainedOnZZ() {
        strategy.process(createConstraint(Dof.ZZ), INDEX, true, coefficients);
    }

    private Constraint createConstraint(Dof constrainedDof) {
        return new Constraint(null, constrainedDof, REFERENCE_FRAME, POSITION);
    }

    private void thenCoefficientShouldBeZeroAtIndexInXCoefficients() {
        assertThat(coefficients.get(Dof.X)[INDEX], equalTo(0d));
    }

    private void thenCoefficientShouldBeZeroAtIndexInYCoefficients() {
        assertThat(coefficients.get(Dof.Y)[INDEX], equalTo(0d));
    }

    private void thenCoefficientShouldBeOneAtIndexInZZCoefficients() {
        assertThat(coefficients.get(Dof.ZZ)[INDEX], equalTo(1d));
    }

    private void whenCalledWithBeamConstrainedOnZZ() {
        strategy.process(createConstraint(Dof.ZZ), INDEX, false, coefficients);
    }

    private void thenCoefficientShouldBeMinusOneAtIndexInZZCoefficients() {
        assertThat(coefficients.get(Dof.ZZ)[INDEX], equalTo(-1d));
    }

    private void thenCoefficientShouldBeZeroAtIndexInZZCoefficients() {
        assertThat(coefficients.get(Dof.ZZ)[INDEX], equalTo(0d));
    }

    private void whenCalledWithJointConstrainedOnX() {
        strategy.process(createConstraint(Dof.X), INDEX, true, coefficients);
    }

    private void thenCoefficientShouldBeLocalYDistanceAtIndexInZZCoefficients() {
        assertThat(coefficients.get(Dof.ZZ)[INDEX], equalTo(LOCAL_Y_DISTANCE));
    }

    private void whenCalledWithBeamConstrainedOnX() {
        strategy.process(createConstraint(Dof.X), INDEX, false, coefficients);
    }

    private void whenCalledWithJointConstrainedOnY() {
        strategy.process(createConstraint(Dof.Y), INDEX, true, coefficients);
    }

    private void whenCalledWithBeamConstrainedOnY() {
        strategy.process(createConstraint(Dof.Y), INDEX, false, coefficients);
    }

    private void thenCoefficientShouldBeLocalXDistanceAtIndexInZZCoefficients() {
        assertThat(coefficients.get(Dof.ZZ)[INDEX], equalTo(LOCAL_X_DISTANCE));
    }

}
