package org.gabs.statics.solver.registry;

import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Element;
import org.gabs.statics.solver.model.Structure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ConstraintRegistryFactoryImplTest {

    private ConstraintRegistryFactoryImpl factory;
    @Mock private Element element1;
    @Mock private Element element2;
    @Mock private Element element3;
    @Mock private Constraint constraint1;
    @Mock private Constraint constraint2;
    @Mock private Constraint constraint3;
    @Mock private Constraint constraint4;
    @Mock private Constraint constraint5;
    @Mock private Constraint constraint6;
    private ConstraintRegistry result;

    @Test
    public void testCreateWithStructureShouldHaveAllConstraintsRegistered() {
        givenConstraintRegistryFactoryImpl();
        whenCalledWithStructure();
        thenResultHasAllConstraintsRegistered();
    }

    @Test
    public void testCreateWithStructureShouldHaveCorrectIndices() {
        givenConstraintRegistryFactoryImpl();
        whenCalledWithStructure();
        thenResultHasIndicesFromZeroToFive();
    }

    private void givenConstraintRegistryFactoryImpl() {
        factory = new ConstraintRegistryFactoryImpl();
    }

    private void whenCalledWithStructure() {
        Structure structure = createStructure();
        result = factory.createFrom(structure);
    }

    private Structure createStructure() {
        Structure result = new Structure();
        result.addExternalConstraints(element1, Collections.singletonList(constraint1));
        result.addInternalConstraints(element1, element2, Collections.singletonList(constraint2));
        result.addExternalConstraints(element2, Arrays.asList(constraint3, constraint4));
        result.addInternalConstraints(element2, element3, Arrays.asList(constraint5, constraint6));
        result.addInternalConstraints(element3, element1, Collections.singletonList(constraint2));
        result.addInternalConstraints(element3, element2, Arrays.asList(constraint5, constraint6));
        return result;
    }

    private void thenResultHasAllConstraintsRegistered() {
        assertThat(result.size(), equalTo(6));
    }

    private void thenResultHasIndicesFromZeroToFive() {
        List<Integer> indices = getIndices();
        assertThat(indices, containsInAnyOrder(0, 1, 2, 3, 4, 5));
    }

    private List<Integer> getIndices() {
        List<Integer> indices = new ArrayList<>(6);
        indices.add(result.getIndex(constraint1));
        indices.add(result.getIndex(constraint2));
        indices.add(result.getIndex(constraint3));
        indices.add(result.getIndex(constraint4));
        indices.add(result.getIndex(constraint5));
        indices.add(result.getIndex(constraint6));
        return indices;
    }

}
