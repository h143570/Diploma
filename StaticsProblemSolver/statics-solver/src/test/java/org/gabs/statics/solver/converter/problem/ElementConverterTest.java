package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.problem.Beam;
import org.gabs.statics.model.problem.Joint;
import org.gabs.statics.solver.model.Element;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ElementConverterTest {

    private static final String ID = "id";

    @InjectMocks private ElementConverter converter;
    @Mock private Joint joint;
    @Mock private Beam beam;
    private Element result;

    @Test
    public void testConvertBeamShouldHaveCorrectId() {
        whenConvertingBeam();
        resultHasCorrectId();
    }

    @Test
    public void testConvertBeamShouldHaveFalseJointFlag() {
        whenConvertingBeam();
        resultHasFalseJointFlag();
    }

    @Test
    public void testConvertJointShouldHaveCorrectId() {
        whenConvertingJoint();
        resultHasCorrectId();
    }

    @Test
    public void testConvertJointShouldHaveTrueJointFlag() {
        whenConvertingJoint();
        resultHasTrueJointFlag();
    }

    private void whenConvertingBeam() {
        when(beam.getId()).thenReturn(ID);
        result = converter.convert(beam);
    }

    private void resultHasCorrectId() {
        assertThat(result.getId(), equalTo(ID));
    }

    private void whenConvertingJoint() {
        when(joint.getId()).thenReturn(ID);
        result = converter.convert(joint);
    }

    private void resultHasFalseJointFlag() {
        assertThat(result.isJoint(), equalTo(false));
    }

    private void resultHasTrueJointFlag() {
        assertThat(result.isJoint(), equalTo(true));
    }

}
