package org.gabs.statics.solver.converter.result;

import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Pair;
import org.gabs.statics.solver.data.Vector;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.ReferenceFrame;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConcentratedForceConverterTest {

    private static final double SQRT2DIV2 = Math.sqrt(2) / 2.0d;
    private static final ReferenceFrame LOCAL_REFERENCE_FRAME = new ReferenceFrame(new Vector(SQRT2DIV2, SQRT2DIV2, 0), new Vector(-SQRT2DIV2,
            SQRT2DIV2, 0), new Vector(0, 0, 1));
    private static final double VALUE = 10.0;
    private static final Dof CONSTRAINED_DISPLACEMENT = Dof.X;
    private static final Vector POSITION = new Vector(1, 0, 0);

    @InjectMocks private ConcentratedForceConverter converter;
    @Mock private Converter<Vector, org.gabs.statics.model.common.Vector> vectorConverter;
    @Mock private org.gabs.statics.model.common.Vector convertedPosition;
    @Mock private org.gabs.statics.model.common.Vector convertedForceVector;
    private ConcentratedForce result;

    @Test
    public void testConvertWithConstraintInGlobalShouldReturnCorrectForce() {
        whenCalledWithConstraintInGlobalReferenceFrame();
        thenResultShouldHaveCorrectDirection();
        thenResultShouldHaveCorrectPointOfApplication();
    }

    @Test
    public void testConvertWithConstraintInLocalShouldReturnCorrectForce() {
        whenCalledWithConstraintInLocalReferenceFrame();
        thenResultShouldHaveCorrectDirection();
        thenResultShouldHaveCorrectPointOfApplication();
    }

    private void whenCalledWithConstraintInGlobalReferenceFrame() {
        Constraint constraint = new Constraint(null, CONSTRAINED_DISPLACEMENT, POSITION);
        when(vectorConverter.convert(POSITION)).thenReturn(convertedPosition);
        when(vectorConverter.convert(new Vector(10, 0, 0))).thenReturn(convertedForceVector);
        result = converter.convert(new Pair<>(constraint, VALUE));
    }

    private void thenResultShouldHaveCorrectDirection() {
        assertThat(result.getVector(), equalTo(convertedForceVector));
    }

    private void thenResultShouldHaveCorrectPointOfApplication() {
        assertThat(((ConcentratedForce) result).getPointOfApplication(), equalTo(convertedPosition));
    }

    private void whenCalledWithConstraintInLocalReferenceFrame() {
        Constraint constraint = new Constraint(null, CONSTRAINED_DISPLACEMENT, LOCAL_REFERENCE_FRAME, POSITION);
        when(vectorConverter.convert(POSITION)).thenReturn(convertedPosition);
        when(vectorConverter.convert(new Vector(10 * SQRT2DIV2, 10 * SQRT2DIV2, 0))).thenReturn(convertedForceVector);
        result = converter.convert(new Pair<>(constraint, VALUE));
    }

}
