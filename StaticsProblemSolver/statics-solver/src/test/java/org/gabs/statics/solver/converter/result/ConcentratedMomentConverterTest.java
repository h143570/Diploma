package org.gabs.statics.solver.converter.result;

import org.gabs.statics.model.common.Moment;
import org.gabs.statics.solver.data.Pair;
import org.gabs.statics.solver.data.Vector;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.ReferenceFrame;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class ConcentratedMomentConverterTest {

    private static final double SQRT2DIV2 = Math.sqrt(2) / 2.0d;
    private static final ReferenceFrame LOCAL_REFERENCE_FRAME = new ReferenceFrame(new Vector(SQRT2DIV2, SQRT2DIV2, 0), new Vector(-SQRT2DIV2,
            SQRT2DIV2, 0), new Vector(0, 0, 1));
    private static final double VALUE = 10.0;

    private ConcentratedMomentConverter converter;
    private Moment result;

    @Test
    public void testConvertWithConstraintInGlobalShouldReturnCorrectMoment() {
        givenConcentratedMomentConverter();
        whenCalledWithConstraintInGlobalReferenceFrame();
        thenResultShouldHaveCorrectDirection();
        thenResultShouldHaveCorrectLength();
    }

    @Test
    public void testConvertWithConstraintInLocalShouldReturnCorrectMoment() {
        givenConcentratedMomentConverter();
        whenCalledWithConstraintInLocalReferenceFrame();
        thenResultShouldHaveCorrectDirection();
        thenResultShouldHaveCorrectLength();
    }

    private void givenConcentratedMomentConverter() {
        converter = new ConcentratedMomentConverter();
    }

    private void whenCalledWithConstraintInGlobalReferenceFrame() {
        Constraint constraint = new Constraint(null, null, null);
        result = converter.convert(new Pair<>(constraint, VALUE));
    }

    private void thenResultShouldHaveCorrectDirection() {
        Vector direction = new Vector(result.getVector().getX(), result.getVector().getY(), result.getVector().getZ()).normalize();
        assertThat(direction, equalTo(ReferenceFrame.GLOBAL.getK()));
    }

    private void thenResultShouldHaveCorrectLength() {
        double length = new Vector(result.getVector().getX(), result.getVector().getY(), result.getVector().getZ()).length();
        assertThat(length, equalTo(VALUE));
    }

    private void whenCalledWithConstraintInLocalReferenceFrame() {
        Constraint constraint = new Constraint(null, null, LOCAL_REFERENCE_FRAME, null);
        result = converter.convert(new Pair<>(constraint, VALUE));
    }

}
