package org.gabs.statics.solver.converter.result;

import org.gabs.statics.model.common.Vector;
import org.junit.Test;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class VectorConverterTest {

    private static final double X = 1;
    private static final double Y = 2;
    private static final double Z = 3;

    private VectorConverter converter;
    private Vector result;

    @Test
    public void testConvertShouldReturnCorrectCoordinates() {
        givenVectorConverter();
        whenCalledWithVector();
        thenResultShouldHaveCorrectCoordinates();
    }

    private void givenVectorConverter() {
        converter = new VectorConverter();
    }

    private void whenCalledWithVector() {
        result = converter.convert(new org.gabs.statics.solver.data.Vector(X, Y, Z));
    }

    private void thenResultShouldHaveCorrectCoordinates() {
        assertThat(result.getX(), equalTo(X));
        assertThat(result.getY(), equalTo(Y));
        assertThat(result.getZ(), equalTo(Z));
    }

}
