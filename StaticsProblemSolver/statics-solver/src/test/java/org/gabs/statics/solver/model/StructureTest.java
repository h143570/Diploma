package org.gabs.statics.solver.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class StructureTest {

    @Mock private Element element1;
    @Mock private Element element2;
    @Mock private Constraint constraint1;
    @Mock private Constraint constraint2;
    private Collection<Constraint> constraints;
    private Collection<Constraint> constraintsResult;
    private Collection<Collection<Constraint>> constraintsGroupResult;
    private Collection<Element> connectedElements;
    private Structure structure;

    @Before
    public void setup() {
        constraints = Collections.singleton(constraint1);
    }

    @Test
    public void testGetConstraintsBetweenWithCorrectElementsShouldReturnValue() {
        givenStructureWithConstraintsBetween(element1, element1);
        whenGetConstraintsBetweenCalledWith(element1, element1);
        thenConstraintsReturned();
    }

    @Test
    public void testGetConstraintsBetweenWithIncorrectElement1ShouldReturnNull() {
        givenStructureWithConstraintsBetween(element1, element1);
        whenGetConstraintsBetweenCalledWith(element2, element1);
        thenNullReturned();
    }

    @Test
    public void testGetConstraintsBetweenWithIncorrectElement2ShouldReturnNull() {
        givenStructureWithConstraintsBetween(element1, element1);
        whenGetConstraintsBetweenCalledWith(element1, element2);
        thenNullReturned();
    }

    @Test
    public void testGetConstraintsOfWithCorrectElementShouldReturnNull() {
        givenStructureWithConstraintsBetween(element1, element2);
        whenGetConstraintsOfCalledWith(element1);
        thenConstraintsGroupsReturned();
    }

    @Test
    public void testGetConnectedElementsWithElementShouldReturnRow() {
        givenStructureWithConstraintsBetween(element1, element2);
        whenConnectedElementsOfCalledWith(element1);
        thenConnectedElementsHas(element2);
    }

    @Test
    public void testAddConstraintsToEmptyStructureShouldAddConstraints() {
        givenEmptyStructure();
        whenAddConstraintsCalledWith(element1, element1);
        thenConstraintsAt(element1, element1);
    }


    private void givenEmptyStructure() {
        structure = new Structure();
    }

    private void thenNullReturned() {
        assertThat(constraintsResult, nullValue());
    }

    private void givenStructureWithConstraintsBetween(Element element1, Element element2) {
        structure = new Structure();
        setConstraintsForTest(element1, element2, constraints);
    }

    private void setConstraintsForTest(Element element1, Element element2, Collection<Constraint> constraints) {
        Map<Element, Map<Element, Collection<Constraint>>> testBackingMap = new HashMap<>(1, 1);
        Map<Element, Collection<Constraint>> row = new HashMap<>(1, 1);
        testBackingMap.put(element1, row);
        row.put(element2, constraints);

        try {
            Field field = structure.getClass().getDeclaredField("backingMap");
            field.setAccessible(true);
            field.set(structure, testBackingMap);
        }
        catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException("Failed get set test structure.");
        }
    }

    private void whenGetConstraintsBetweenCalledWith(Element element1, Element element2) {
        constraintsResult = structure.getConstraintsBetween(element1, element2);
    }

    private void thenConstraintsReturned() {
        assertThat(constraintsResult, equalTo(constraints));
    }

    private void whenGetConstraintsOfCalledWith(Element element1) {
        constraintsGroupResult = structure.getConstraintsOf(element1);
    }

    @SuppressWarnings("unchecked")
    private void thenConstraintsGroupsReturned() {
        assertThat(constraintsGroupResult, contains(constraints));
    }

    private void whenConnectedElementsOfCalledWith(Element rowKey) {
        connectedElements = structure.getConnectedElementsOf(rowKey);
    }

    private void thenConnectedElementsHas(Element element) {
        assertThat(connectedElements, contains(element));
    }

    private void whenAddConstraintsCalledWith(Element element1, Element element2) {
        structure.addInternalConstraints(element1, element2, constraints);
    }

    private void thenConstraintsAt(Element element1, Element element2) {
        assertThat(structure.getConstraintsBetween(element1, element2), equalTo(constraints));
    }

}
