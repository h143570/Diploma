package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.problem.Beam;
import org.gabs.statics.model.problem.Constraints;
import org.gabs.statics.model.problem.Joint;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Pair;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.ReferenceFrame;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConstraintsConverterTest {

    private static final String JOINT_ID = "joint ID";
    private static final String BEAM_ID = "beam ID";
    private static final String ID_SEPARATOR = ":";
    private static final String GROUND = "ground";
    private static final Vector POSITION = mock(Vector.class);
    private static final org.gabs.statics.solver.data.Vector CONVERTED_POSITION = mock(org.gabs.statics.solver.data.Vector.class);
    private static final Vector LOCAL_REFERENCE_FRAME_I = mock(Vector.class);
    private static final ReferenceFrame REFERENCE_FRAME = mock(ReferenceFrame.class);

    @InjectMocks private ConstraintsConverter converter;
    @Mock private Converter<Vector, org.gabs.statics.solver.data.Vector> vectorConverter;
    @Mock private Converter<Vector, ReferenceFrame> referenceFrameConverter;
    @Mock private Joint joint;
    @Mock private Beam beam;
    @Mock private Constraints constraints;
    private Collection<Constraint> result;

    @Test
    public void testConvertWithJointBeamPairResultShouldHaveCorrectId() {
        whenCalledWithJointBeamPair();
        thenResultItemsShouldHaveIdWithJointAndBeamIds();
    }

    @Test
    public void testConvertWithOnlyJointResultShouldHaveCorrectId() {
        whenCalledWithOnlyJoint();
        thenResultItemsShouldHaveIdWithJoint();
    }

    @Test
    public void testConvertResultShouldHaveCorrectReferenceFrame() {
        whenCalledWithJointWithConstraintInLocalReferenceFrame();
        thenResultItemsShouldHaveCorrectReferenceFrame();
    }

    @Test
    public void testConvertResultShouldHaveCorrectPosition() {
        whenCalledWithJointHavingPosition();
        thenResultItemsShouldHaveCorrectPosition();
    }

    @Test
    public void testConvertWithJointBeamPairShouldHaveCorrectConstraints() {
        whenCalledWithJointAndBeamConstrainedOnXY();
        thenResultItemsShouldHaveXYConstraints();
    }

    @Test
    public void testConvertWithOnlyJointShouldHaveCorrectConstraints() {
        whenCalledWithJointConstrainedOnXY();
        thenResultItemsShouldHaveXYConstraints();
    }

    private void whenCalledWithJointBeamPair() {
        when(joint.getConstraints()).thenReturn(constraints);
        when(joint.getId()).thenReturn(JOINT_ID);
        when(beam.getId()).thenReturn(BEAM_ID);
        when(constraints.getLocalReferenceFrameI()).thenReturn(LOCAL_REFERENCE_FRAME_I);
        when(constraints.isConstrainedRz()).thenReturn(true);
        when(constraints.isConstrainedTx()).thenReturn(true);
        when(constraints.isConstrainedTy()).thenReturn(true);
        when(referenceFrameConverter.convert(LOCAL_REFERENCE_FRAME_I)).thenReturn(REFERENCE_FRAME);
        result = converter.convert(new Pair<>(joint, beam));
    }

    private void thenResultItemsShouldHaveIdWithJointAndBeamIds() {
        String expectedId = JOINT_ID + ID_SEPARATOR + BEAM_ID;
        for (Constraint constraint : result) {
            assertThat(constraint.getId(), equalTo(expectedId));
        }
    }

    private void whenCalledWithOnlyJoint() {
        when(joint.getConstraints()).thenReturn(constraints);
        when(joint.getId()).thenReturn(JOINT_ID);
        when(constraints.getLocalReferenceFrameI()).thenReturn(LOCAL_REFERENCE_FRAME_I);
        when(constraints.isConstrainedRz()).thenReturn(true);
        when(constraints.isConstrainedTx()).thenReturn(true);
        when(constraints.isConstrainedTy()).thenReturn(true);
        when(referenceFrameConverter.convert(LOCAL_REFERENCE_FRAME_I)).thenReturn(REFERENCE_FRAME);
        result = converter.convert(new Pair<Joint, Beam>(joint, null));
    }

    private void thenResultItemsShouldHaveIdWithJoint() {
        String expectedId = JOINT_ID + ID_SEPARATOR + GROUND;
        for (Constraint constraint : result) {
            assertThat(constraint.getId(), equalTo(expectedId));
        }
    }

    private void whenCalledWithJointWithConstraintInLocalReferenceFrame() {
        when(joint.getConstraints()).thenReturn(constraints);
        when(joint.getId()).thenReturn(JOINT_ID);
        when(beam.getId()).thenReturn(BEAM_ID);
        when(constraints.getLocalReferenceFrameI()).thenReturn(LOCAL_REFERENCE_FRAME_I);
        when(constraints.isConstrainedRz()).thenReturn(true);
        when(constraints.isConstrainedTx()).thenReturn(true);
        when(constraints.isConstrainedTy()).thenReturn(true);
        when(referenceFrameConverter.convert(LOCAL_REFERENCE_FRAME_I)).thenReturn(REFERENCE_FRAME);
        result = converter.convert(new Pair<>(joint, beam));
    }

    private void thenResultItemsShouldHaveCorrectReferenceFrame() {
        for (Constraint constraint : result) {
            assertThat(constraint.getLocalReferenceFrame(), equalTo(REFERENCE_FRAME));
        }
    }

    private void whenCalledWithJointHavingPosition() {
        when(joint.getConstraints()).thenReturn(constraints);
        when(constraints.isConstrainedRz()).thenReturn(true);
        when(constraints.isConstrainedTx()).thenReturn(true);
        when(constraints.isConstrainedTy()).thenReturn(true);
        when(joint.getPosition()).thenReturn(POSITION);
        when(vectorConverter.convert(POSITION)).thenReturn(CONVERTED_POSITION);
        result = converter.convert(new Pair<Joint, Beam>(joint, null));
    }

    private void thenResultItemsShouldHaveCorrectPosition() {
        for (Constraint constraint : result) {
            assertThat(constraint.getPosition(), equalTo(CONVERTED_POSITION));
        }
    }

    private void whenCalledWithJointAndBeamConstrainedOnXY() {
        when(joint.getConstraints()).thenReturn(constraints);
        when(constraints.isConstrainedRz()).thenReturn(false);
        when(constraints.isConstrainedTx()).thenReturn(true);
        when(constraints.isConstrainedTy()).thenReturn(true);
        result = converter.convert(new Pair<>(joint, beam));
    }

    @SuppressWarnings("unchecked")
    private void thenResultItemsShouldHaveXYConstraints() {
        assertThat(result, containsInAnyOrder(hasProperty("constrainedDisplacement", equalTo(Dof.X)), hasProperty("constrainedDisplacement",
                equalTo(Dof.Y))));
    }

    private void whenCalledWithJointConstrainedOnXY() {
        when(joint.getSupport()).thenReturn(constraints);
        when(constraints.isConstrainedRz()).thenReturn(false);
        when(constraints.isConstrainedTx()).thenReturn(true);
        when(constraints.isConstrainedTy()).thenReturn(true);
        result = converter.convert(new Pair<Joint, Beam>(joint, null));
    }

}
