package org.gabs.statics.solver.analyzer;

import org.gabs.statics.solver.data.Analysis;
import org.gabs.statics.solver.model.Determinacy;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class ProblemAnalyzerImplTest {

    private ProblemAnalyzerImpl analyzer;
    private Analysis result;

    @Before
    public void setup() {
        analyzer = null;
        result = null;
    }

    @Test
    public void testAnalyzeWithDeterminateProblemShouldReturnDeterminateStructureAndProblem() {
        givenProblemAnalyzerImpl();
        whenCalledWithDeterminateProblem();
        thenResultShouldBeDeterminateStructure();
        thenResultShouldBeDeterminateProblem();
    }

    @Test
    public void testAnalyzeWithIndeterminateProblemShouldReturnIndeterminateStructureAndProblem() {
        givenProblemAnalyzerImpl();
        whenCalledWithIndeterminateProblem();
        thenResultShouldBeIndeterminateStructure();
        thenResultShouldBeIndeterminateProblem();
    }

    @Test
    public void testAnalyzeWithSolvableProblemWithOverdeterminateStructureShouldReturnDeterminateProblem() {
        givenProblemAnalyzerImpl();
        whenCalledWithSolvableProblemWithOverdeterminateStructure();
        thenResultShouldBeOverdeterminateStructure();
        thenResultShouldBeDeterminateProblem();
    }

    @Test
    public void testAnalyzeWithUnsolvableProblemWithOverdeterminateStructureShouldReturnOverdeterminateProblem() {
        givenProblemAnalyzerImpl();
        whenCalledWithUnsolvableProblemWithOverdeterminateStructure();
        thenResultShouldBeOverdeterminateStructure();
        thenResultShouldBeOverdeterminateProblem();
    }

    private void givenProblemAnalyzerImpl() {
        analyzer = new ProblemAnalyzerImpl();
    }

    private void whenCalledWithDeterminateProblem() {
        double[][] matrix = new double[][]{
                {3, 12, 9, 6},
                {0, 2, -2, 2},
                {0, 0, 2, -2}
        };
        result = analyzer.analyze(matrix);
    }

    private void thenResultShouldBeDeterminateStructure() {
        assertThat(result.getStructureDeterminacy(), equalTo(Determinacy.DETERMINATE));
    }

    private void thenResultShouldBeDeterminateProblem() {
        assertThat(result.getProblemDeterminacy(), equalTo(Determinacy.DETERMINATE));
    }

    private void whenCalledWithIndeterminateProblem() {
        double[][] matrix = new double[][]{
                {3, 12, 9, 6},
                {0, 2, -2, 2}
        };
        result = analyzer.analyze(matrix);
    }

    private void thenResultShouldBeIndeterminateStructure() {
        assertThat(result.getStructureDeterminacy(), equalTo(Determinacy.INDETERMINATE));
    }

    private void thenResultShouldBeIndeterminateProblem() {
        assertThat(result.getProblemDeterminacy(), equalTo(Determinacy.INDETERMINATE));
    }

    private void whenCalledWithSolvableProblemWithOverdeterminateStructure() {
        double[][] matrix = new double[][]{
                {3, 12, 9, 6},
                {0, 2, -2, 2},
                {0, 0, 2, -2},
                {0, 0, 0, 0}
        };
        result = analyzer.analyze(matrix);
    }

    private void whenCalledWithUnsolvableProblemWithOverdeterminateStructure() {
        double[][] matrix = new double[][]{
                {3, 12, 9, 6},
                {0, 2, -2, 2},
                {0, 0, 2, -2},
                {0, 0, 0, 1}
        };
        result = analyzer.analyze(matrix);
    }

    private void thenResultShouldBeOverdeterminateStructure() {
        assertThat(result.getStructureDeterminacy(), equalTo(Determinacy.OVERDETERMINATE));
    }

    private void thenResultShouldBeOverdeterminateProblem() {
        assertThat(result.getProblemDeterminacy(), equalTo(Determinacy.OVERDETERMINATE));
    }

}
