package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.common.Vector;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.model.ReferenceFrame;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ReferenceFrameConverterTest {

    private static final double ERROR = 0.000000000000001;
    private static final Vector NORMALIZED_GLOBAL_I = new Vector(1, 0, 0);
    private static final Vector NON_GLOBAL_I = new Vector(sin(PI / 3) * 5, cos(PI / 3) * 5, 0);
    private static final Vector NORMALIZED_NON_GLOBAL_I = new Vector(sin(PI / 3), cos(PI / 3), 0);
    private static final org.gabs.statics.solver.data.Vector CONVERTED_NORMALIZED_GLOBAL_I = new org.gabs.statics.solver.data.Vector(1, 0, 0);
    private static final org.gabs.statics.solver.data.Vector CONVERTED_NON_GLOBAL_I = new org.gabs.statics.solver.data.Vector(cos(PI / 3) * 5,
            sin(PI / 3) * 5, 0);
    private static final org.gabs.statics.solver.data.Vector CONVERTED_NORMALIZED_NON_GLOBAL_I = new org.gabs.statics.solver.data.Vector(cos(PI / 3),
            sin(PI / 3), 0);
    private static final org.gabs.statics.solver.data.Vector NORMALIZED_NON_GLOBAL_J = new org.gabs.statics.solver.data.Vector(cos(PI * 5 / 6),
            sin(PI * 5 / 6), 0);
    private static final org.gabs.statics.solver.data.Vector NORMALIZED_NON_GLOBAL_K = new org.gabs.statics.solver.data.Vector(0, 0, 1);

    @InjectMocks private ReferenceFrameConverter converter;
    @Mock private Converter<Vector, org.gabs.statics.solver.data.Vector> vectorConverter;
    private ReferenceFrame result;

    @Test
    public void testConvertWithGlobalRefFrameShouldReturnGlobal() {
        whenCalledWithGlobalReferenceFrame();
        thenResultShouldBeGlobalReferenceFrame();
    }

    @Test
    public void testConvertWithNonGlobalNormalizedIShouldReturnPerpendicularAxes() {
        whenCalledWithNonGlobalI();
        thenIEqualsLocalI();
        thenJIsPerpendicularToLocalI();
        thenKIsPerpendicularToIAndJ();
    }

    @Test
    public void testConvertWithNonGlobalNonNormalizedIShouldReturnPerpendicularAxes() {
        whenCalledWithNonGlobalNonNormalizedI();
        thenIEqualsLocalI();
        thenJIsPerpendicularToLocalI();
        thenKIsPerpendicularToIAndJ();
    }

    private void whenCalledWithGlobalReferenceFrame() {
        when(vectorConverter.convert(NORMALIZED_GLOBAL_I)).thenReturn(CONVERTED_NORMALIZED_GLOBAL_I);
        result = converter.convert(NORMALIZED_GLOBAL_I);
    }

    private void thenResultShouldBeGlobalReferenceFrame() {
        assertThat(result, equalTo(ReferenceFrame.GLOBAL));
    }

    private void whenCalledWithNonGlobalI() {
        when(vectorConverter.convert(NORMALIZED_NON_GLOBAL_I)).thenReturn(CONVERTED_NORMALIZED_NON_GLOBAL_I);
        result = converter.convert(NORMALIZED_NON_GLOBAL_I);
    }

    private void thenIEqualsLocalI() {
        assertVectorsAreClose(result.getI(), CONVERTED_NORMALIZED_NON_GLOBAL_I);
    }

    private void assertVectorsAreClose(org.gabs.statics.solver.data.Vector vector1, org.gabs.statics.solver.data.Vector vector2) {
        assertThat(vector1.getX(), closeTo(vector2.getX(), ERROR));
        assertThat(vector1.getY(), closeTo(vector2.getY(), ERROR));
        assertThat(vector1.getZ(), closeTo(vector2.getZ(), ERROR));
    }

    private void thenJIsPerpendicularToLocalI() {
        assertVectorsAreClose(result.getJ(), NORMALIZED_NON_GLOBAL_J);
    }

    private void thenKIsPerpendicularToIAndJ() {
        assertVectorsAreClose(result.getK(), NORMALIZED_NON_GLOBAL_K);
    }

    private void whenCalledWithNonGlobalNonNormalizedI() {
        when(vectorConverter.convert(NON_GLOBAL_I)).thenReturn(CONVERTED_NON_GLOBAL_I);
        result = converter.convert(NON_GLOBAL_I);
    }

}
