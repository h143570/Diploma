package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.problem.*;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.model.Effect;
import org.gabs.statics.solver.model.Element;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoadMappingBuilderImplTest {

    @InjectMocks private LoadMappingBuilderImpl builder;
    @Mock private Joint joint;
    @Mock private Beam beam;
    @Mock private Element element1;
    @Mock private Element element2;
    @Mock private ConcentratedForce force;
    @Mock private Moment moment;
    @Mock private Effect effect1;
    @Mock private Effect effect2;
    @Mock private Converter<ConcentratedForce, Effect> forceConverter;
    @Mock private Converter<Moment, Effect> momentConverter;
    private Map<Element, Collection<Effect>> result;

    @Test
    public void testBuildWhenCalledWithEmptyProblemShouldReturnEmptyMap() {
        whenBuildCalledWithEmptyProblem();
        thenResultShouldBeEmpty();
    }

    @Test
    public void testBuildWhenCalledWithProblemShouldReturnLoadMapping() {
        whenBuildCalledWithProblem();
        thenResultShouldHaveCorrectMapping();
    }

    private void whenBuildCalledWithEmptyProblem() {
        Problem problem = new Problem.Builder().build();
        result = builder.build(problem, null);
    }

    private void thenResultShouldBeEmpty() {
        assertThat(result, equalTo(Collections.<Element, Collection<Effect>>emptyMap()));
    }

    private void whenBuildCalledWithProblem() {
        setConverters();
        addLoadToElements();
        result = builder.build(createProblem(), createElementMapping());
    }

    private Problem createProblem() {
        when(joint.getId()).thenReturn("joint");
        when(beam.getId()).thenReturn("beam");
        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(joint, beam);
        return builder.build();
    }

    private void setConverters() {
        builder.setMomentConverter(momentConverter);
        builder.setForceConverter(forceConverter);
        when(momentConverter.convert(moment)).thenReturn(effect1);
        when(forceConverter.convert(force)).thenReturn(effect2);
    }

    private void addLoadToElements() {
        when(beam.getMoments()).thenReturn(Collections.singleton(moment));
        when(joint.getConcentratedForces()).thenReturn(Collections.singleton(force));
    }

    private Map<org.gabs.statics.model.problem.Element, Element> createElementMapping() {
        Map<org.gabs.statics.model.problem.Element, Element> result = new HashMap<>(2, 1);
        result.put(beam, element1);
        result.put(joint, element2);
        return result;
    }

    private void thenResultShouldHaveCorrectMapping() {
        assertThat(result.keySet(), hasSize(2));
        assertThat(result.get(element1), hasItems(effect1));
        assertThat(result.get(element2), hasItems(effect2));
    }

}
