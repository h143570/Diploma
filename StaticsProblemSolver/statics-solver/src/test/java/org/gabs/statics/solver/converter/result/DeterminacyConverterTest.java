package org.gabs.statics.solver.converter.result;

import org.gabs.statics.model.result.StaticalDeterminacy;
import org.gabs.statics.solver.model.Determinacy;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class DeterminacyConverterTest {

    private DeterminacyConverter converter;
    private StaticalDeterminacy result;

    @Test
    public void testConvertWithIndeterminateShouldResultIndeterminate() {
        givenDeterminacyConverter();
        whenCalledWithIndeterminateDeterminacy();
        thenResultShouldBeIndeterminate();
    }

    @Test
    public void testConvertWithDeterminateShouldResultDeterminate() {
        givenDeterminacyConverter();
        whenCalledWithDeterminateDeterminacy();
        thenResultShouldBeDeterminate();
    }

    @Test
    public void testConvertWithOverdeterminateShouldResultOverdeterminate() {
        givenDeterminacyConverter();
        whenCalledWithOverdeterminateDeterminacy();
        thenResultShouldBeOverdeterminate();
    }

    private void givenDeterminacyConverter() {
        converter = new DeterminacyConverter();
    }

    private void whenCalledWithIndeterminateDeterminacy() {
        result = converter.convert(Determinacy.INDETERMINATE);
    }

    private void thenResultShouldBeIndeterminate() {
        assertThat(result, equalTo(StaticalDeterminacy.INDETERMINATE));
    }

    private void whenCalledWithDeterminateDeterminacy() {
        result = converter.convert(Determinacy.DETERMINATE);
    }

    private void thenResultShouldBeDeterminate() {
        assertThat(result, equalTo(StaticalDeterminacy.DETERMINATE));
    }

    private void whenCalledWithOverdeterminateDeterminacy() {
        result = converter.convert(Determinacy.OVERDETERMINATE);
    }

    private void thenResultShouldBeOverdeterminate() {
        assertThat(result, equalTo(StaticalDeterminacy.OVERDETERMINATE));
    }

}
