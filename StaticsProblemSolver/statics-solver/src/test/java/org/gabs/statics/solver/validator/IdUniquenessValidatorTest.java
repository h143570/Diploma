package org.gabs.statics.solver.validator;

import org.gabs.statics.model.problem.Beam;
import org.gabs.statics.model.problem.Joint;
import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.solver.data.Error;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IdUniquenessValidatorTest {

    private static final String ERROR = "Element IDs must be unique.";

    @InjectMocks private IdUniquenessValidator validator;
    @Mock private Joint joint1;
    @Mock private Joint joint2;
    @Mock private Beam beam1;
    @Mock private Beam beam2;
    private Error result;

    @Test
    public void testValidateEmptyProblemShouldPass() {
        whenCalledValidateWithEmptyProblem();
        thenErrorIsNull();
    }

    @Test
    public void testValidateProblemWithUniqueIdsShouldPass() {
        whenCalledValidateWithProblemWithUniqueIds();
        thenErrorIsNull();
    }

    @Test
    public void testValidateProblemWithNonUniqueJointIdsShouldNotPass() {
        whenCalledValidateWithProblemWithNonUniqueJointIds();
        thenResultShouldContainError();
    }

    @Test
    public void testValidateProblemWithNonUniqueBeamIdsShouldNotPass() {
        whenCalledValidateWithProblemWithNonUniqueBeamIds();
        thenResultShouldContainError();
    }

    @Test
    public void testValidateProblemWithNonUniqueJointAndBodyIdsShouldNotPass() {
        whenCalledValidateWithProblemWithNonUniqueJointAndBodyIds();
        thenResultShouldContainError();
    }

    private void whenCalledValidateWithEmptyProblem() {
        Problem problem = new Problem.Builder().build();
        result = validator.validate(problem);
    }

    private void thenErrorIsNull() {
        assertThat(result, nullValue());
    }

    private void whenCalledValidateWithProblemWithUniqueIds() {
        Problem problem = createProblemWithUniqueIds();
        result = validator.validate(problem);
    }

    private Problem createProblemWithUniqueIds() {
        useUniqueIds();
        Problem problem = createProblem();
        return problem;
    }

    private Problem createProblem() {
        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(joint1, beam1);
        builder.addBeam(joint2, beam1);
        builder.addBeam(joint2, beam2);
        return builder.build();
    }

    private void useUniqueIds() {
        when(joint1.getId()).thenReturn("joint1");
        when(joint2.getId()).thenReturn("joint2");
        when(beam1.getId()).thenReturn("beam1");
        when(beam2.getId()).thenReturn("beam2");
    }

    private void whenCalledValidateWithProblemWithNonUniqueJointIds() {
        Problem problem = createProblemWithNonUniqueJointIds();
        result = validator.validate(problem);
    }

    private Problem createProblemWithNonUniqueJointIds() {
        useNonUniqueJointIds();
        Problem problem = createProblem();
        return problem;
    }

    private void useNonUniqueJointIds() {
        when(joint1.getId()).thenReturn("joint");
        when(joint2.getId()).thenReturn("joint");
        when(beam1.getId()).thenReturn("beam1");
        when(beam2.getId()).thenReturn("beam2");
    }

    private void thenResultShouldContainError() {
        assertThat(result.getDescription(), equalTo(ERROR));
    }

    private void whenCalledValidateWithProblemWithNonUniqueBeamIds() {
        Problem problem = createProblemWithNonUniqueBeamIds();
        result = validator.validate(problem);
    }

    private Problem createProblemWithNonUniqueBeamIds() {
        useNonUniqueBeamIds();
        Problem problem = createProblem();
        return problem;
    }

    private void useNonUniqueBeamIds() {
        when(joint1.getId()).thenReturn("joint1");
        when(joint2.getId()).thenReturn("joint2");
        when(beam1.getId()).thenReturn("beam");
        when(beam2.getId()).thenReturn("beam");
    }

    private void whenCalledValidateWithProblemWithNonUniqueJointAndBodyIds() {
        Problem problem = createProblemWithNonUniqueJointAndBeamIds();
        result = validator.validate(problem);
    }

    private Problem createProblemWithNonUniqueJointAndBeamIds() {
        useNonUniqueJointAndBeamIds();
        Problem problem = createProblem();
        return problem;
    }

    private void useNonUniqueJointAndBeamIds() {
        when(joint1.getId()).thenReturn("joint1");
        when(joint2.getId()).thenReturn("joint2");
        when(beam1.getId()).thenReturn("joint1");
        when(beam2.getId()).thenReturn("beam");
    }

}
