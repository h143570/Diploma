package org.gabs.statics.solver.matrix.extractor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class SolutionExtractorImplTest {

    private SolutionExtractorImpl extractor;
    private double[] result;

    @Test
    public void testExtractSolutionWithDeterminateProblemShouldReturnCorrectResult() {
        givenSolutionExtractorImpl();
        whenCalledWithDeterminateProblem();
        thenResultShouldBeCorrectForDeterminateProblem();
    }

    @Test
    public void testExtractSolutionWithOverdeterminateProblemWithDeterminateStructureShouldReturnCorrectResult() {
        givenSolutionExtractorImpl();
        whenCalledWithOverdeterminateProblemWithDetermindateStructure();
        thenResultShouldBeCorrectForDeterminateProblem();
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testExtractSolutionWithIndeterminateProblemShouldThrowException() {
        givenSolutionExtractorImpl();
        whenCalledWithIndeterminateProblem();
    }

    private void givenSolutionExtractorImpl() {
        extractor = new SolutionExtractorImpl();
    }

    private void whenCalledWithDeterminateProblem() {
        double[][] matrix = new double[][]{
                {3, 12, 9, 6},
                {0, 2, -2, 2},
                {0, 0, 2, -2}
        };
        result = extractor.extractSolution(matrix);
    }

    private void thenResultShouldBeCorrectForDeterminateProblem() {
        assertThat(result[0], equalTo(5.0));
        assertThat(result[1], equalTo(0.0));
        assertThat(result[2], equalTo(-1.0));
    }

    private void whenCalledWithOverdeterminateProblemWithDetermindateStructure() {
        double[][] matrix = new double[][]{
                {3, 12, 9, 6},
                {0, 2, -2, 2},
                {0, 0, 2, -2},
                {0, 0, 0, 0}
        };
        result = extractor.extractSolution(matrix);
    }

    private void whenCalledWithIndeterminateProblem() {
        double[][] matrix = new double[][]{
                {3, 12, 9, 6},
                {0, 2, -2, 2}
        };
        result = extractor.extractSolution(matrix);
    }

}
