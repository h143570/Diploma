package org.gabs.statics.solver.resultbuilder;

import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.common.ConcentratedForce;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ConcentratedForceMergingStrategyImplTest {

    private static final ConcentratedForce FORCE_1 = new ConcentratedForce(new Vector(0, 1, 2), new Vector(2, 4, 0));
    private static final ConcentratedForce FORCE_2 = new ConcentratedForce(new Vector(0, 1, 2), new Vector(1, 3, 2));

    private ConcentratedForceMergingStrategyImpl strategy;
    private ConcentratedForce result;

    @Test
    public void testMergeShouldMergeForces() {
        givenConcentratedForceMergingStrategyImpl();
        whenCalledWithForces();
        thenForcesShouldBeMerged();
    }

    @Test
    public void testMergeShouldReturnNullWhenForceListIsEmpty() {
        givenConcentratedForceMergingStrategyImpl();
        whenCalledWithEmptyForceList();
        thenResultShouldBeNull();
    }

    private void givenConcentratedForceMergingStrategyImpl() {
        strategy = new ConcentratedForceMergingStrategyImpl();
    }

    private void whenCalledWithForces() {
        result = strategy.merge(Arrays.asList(FORCE_1, FORCE_2));
    }

    private void thenForcesShouldBeMerged() {
        ConcentratedForce expectedResult = new ConcentratedForce(new Vector(0, 1, 2), new Vector(3, 7, 2));
        assertThat(result, equalTo(expectedResult));
    }

    private void whenCalledWithEmptyForceList() {
        result = strategy.merge(Collections.<ConcentratedForce>emptyList());
    }

    private void thenResultShouldBeNull() {
        assertThat(result, nullValue(ConcentratedForce.class));
    }

}
