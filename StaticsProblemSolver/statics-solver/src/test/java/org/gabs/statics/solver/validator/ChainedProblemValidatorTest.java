package org.gabs.statics.solver.validator;

import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.solver.data.Error;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChainedProblemValidatorTest {

    private static final Error ERROR_1 = new Error("error 1");
    private static final Error ERROR_2 = new Error("error 2");

    @InjectMocks private ChainedProblemValidator chainedValidator;
    @Mock private Problem problem;
    @Mock private ProblemValidator validator1;
    @Mock private ProblemValidator validator2;
    private Error result;

    @Test
    public void testValidateValidProblemShouldReturnNullError() {
        givenChainedValidator();
        whenCalledValidateWithValidProblem();
        thenEveryValidatorIsCalled();
        thenResultIsNull();
    }

    @Test
    public void testValidateProblemFailsOnFirstValidatorShouldReturnCorrectError() {
        givenChainedValidator();
        whenProblemFailsOnFirstValidator();
        whenCalledValidate();
        thenSecondValidatorShouldNotBeCalled();
        thenResultShouldBeFirstError();
    }

    @Test
    public void testValidateProblemFailsOnSecondValidatorShouldReturnCorrectError() {
        givenChainedValidator();
        whenProblemFailsOnSecondValidator();
        whenCalledValidate();
        thenEveryValidatorIsCalled();
        thenResultShouldBeSecondError();
    }

    private void givenChainedValidator() {
        chainedValidator.setValidators(Arrays.asList(validator1, validator2));
    }

    private void whenCalledValidateWithValidProblem() {
        when(validator1.validate(problem)).thenReturn(null);
        when(validator2.validate(problem)).thenReturn(null);
        result = chainedValidator.validate(problem);
    }

    private void thenEveryValidatorIsCalled() {
        verify(validator1).validate(problem);
        verify(validator2).validate(problem);
    }

    private void thenResultIsNull() {
        assertThat(result, nullValue());
    }

    private void whenProblemFailsOnFirstValidator() {
        when(validator1.validate(problem)).thenReturn(ERROR_1);
    }

    private void whenCalledValidate() {
        result = chainedValidator.validate(problem);
    }

    private void thenSecondValidatorShouldNotBeCalled() {
        verifyZeroInteractions(validator2);
    }

    private void thenResultShouldBeFirstError() {
        assertThat(result, equalTo(ERROR_1));
    }

    private void whenProblemFailsOnSecondValidator() {
        when(validator2.validate(problem)).thenReturn(ERROR_2);
    }

    private void thenResultShouldBeSecondError() {
        assertThat(result, equalTo(ERROR_2));
    }

}
