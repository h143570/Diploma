package org.gabs.statics.solver;

import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.problem.Beam;
import org.gabs.statics.model.problem.Constraints;
import org.gabs.statics.model.problem.Joint;
import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.model.result.Result;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class ProblemSolverImplErrorTest {

    private static final String STRUCTURAL_CONTINUITY_ERROR = "Structure must be continuous.";

    private ProblemSolver solver;
    private Joint rollerJoint1;
    private Joint rollerJoint2;
    private Joint pinnedJoint1;
    private Joint pinnedJoint2;
    private Beam beam1;
    private Beam beam2;
    private Result result;

    @Test
    public void testTwoSimplySupportedBeams() {
        givenProblemSolver();
        whenCalledWithTwoSimplySupportedBeams();
        thenResultHasNullReactions();
        thenResultHasNullStructuralDeterminacy();
        thenResultHasNullProblemDeterminacy();
        thenResultHasStructuralContinuityError();
    }

    private void givenProblemSolver() {
        solver = new ProblemSolverFactoryImpl().create();
    }

    private void whenCalledWithTwoSimplySupportedBeams() {
        Problem problem = createTwoSimplySupportedBeams();
        result = solver.solve(problem);
    }

    private Problem createTwoSimplySupportedBeams() {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints rollerSupport = new Constraints.Builder().constrainTx().build();
        Constraints pinnedSupport = new Constraints.Builder().constrainTx().constrainTx().build();
        rollerJoint1 = new Joint.Builder("roller joint 1", new Vector(0, 0, 0), hinge).withSupport(rollerSupport).build();
        rollerJoint2 = new Joint.Builder("roller joint 2", new Vector(0, 2, 0), hinge).withSupport(rollerSupport).build();
        pinnedJoint1 = new Joint.Builder("pinned joint 1", new Vector(0, 1, 0), hinge).withSupport(pinnedSupport).build();
        pinnedJoint2 = new Joint.Builder("pinned joint 2", new Vector(0, 3, 0), hinge).withSupport(pinnedSupport).build();
        beam1 = new Beam.Builder("beam 1").build();
        beam2 = new Beam.Builder("beam 2").build();

        Problem.Builder builder = new Problem.Builder();
        builder.addBeams(rollerJoint1, beam1);
        builder.addBeams(pinnedJoint1, beam1);
        builder.addBeams(rollerJoint2, beam2);
        builder.addBeams(pinnedJoint2, beam2);
        return builder.build();
    }

    private void thenResultHasNullReactions() {
        assertThat(result.getReactions(), nullValue());
    }

    private void thenResultHasNullStructuralDeterminacy() {
        assertThat(result.getStructureDeterminacy(), nullValue());
    }

    private void thenResultHasNullProblemDeterminacy() {
        assertThat(result.getProblemDeterminacy(), nullValue());
    }

    private void thenResultHasStructuralContinuityError() {
        assertThat(result.getError(), equalTo(STRUCTURAL_CONTINUITY_ERROR));
    }

}
