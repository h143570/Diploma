package org.gabs.statics.solver.validator;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.problem.Beam;
import org.gabs.statics.model.problem.Constraints;
import org.gabs.statics.model.problem.Joint;
import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.solver.data.Error;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class ProblemValidatorComponentTest {

    private static final String ID_UNIQUENESS_ERROR = "Element IDs must be unique.";
    private static final String STRUCTURAL_CONTINUITY_ERROR = "Structure must be continuous.";

    private ProblemValidator validator;
    private Error result;

    @Test
    public void testValidateSimplySupportedBeam() {
        givenProblemValidator();
        whenCalledWithValidSimplySupportedBeam();
        thenErrorIsNull();
    }

    @Test
    public void testValidateSimplySupportedBeamWithDuplicateIds() {
        givenProblemValidator();
        whenCalledWithSimplySupportedBeamWithDuplicateIds();
        thenErrorShouldContainIdUniquenessError();
    }

    @Test
    public void testValidateTwoSeparateSimplySupportedBeams() {
        givenProblemValidator();
        whenCalledWithTwoSeparateSimplySupportedBeams();
        thenErrorShouldContainStructuralContinuityError();
    }

    private void givenProblemValidator() {
        Injector injector = Guice.createInjector(new ProblemValidatorModule());
        validator = injector.getInstance(ProblemValidator.class);
    }

    private void whenCalledWithValidSimplySupportedBeam() {
        Problem problem = createValidSimpleSupportedBeam();
        result = validator.validate(problem);
    }

    private Problem createValidSimpleSupportedBeam() {
        Joint rollerJoint = createRollerJoint("roller joint");
        Joint pinnedJoint = createPinnedJoint("pinned joint");
        Beam beam = createBeam("beam");
        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(rollerJoint, beam);
        builder.addBeam(pinnedJoint, beam);
        return builder.build();
    }

    private Joint createRollerJoint(String id) {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints rollerSupport = new Constraints.Builder().constrainTx().build();
        Joint.Builder builder = new Joint.Builder(id, new Vector(0, 0, 0), hinge);
        builder.withSupport(rollerSupport);
        return builder.build();
    }

    private Joint createPinnedJoint(String id) {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints pinnedSupport = new Constraints.Builder().constrainTx().constrainTy().build();
        Joint.Builder builder = new Joint.Builder(id, new Vector(0, 2, 0), hinge);
        builder.withSupport(pinnedSupport);
        return builder.build();
    }

    private Beam createBeam(String id) {
        return new Beam.Builder(id).build();
    }

    private void thenErrorIsNull() {
        assertThat(result, nullValue());
    }

    private void whenCalledWithSimplySupportedBeamWithDuplicateIds() {
        Problem problem = createSimpleSupportedBeamWithDuplicateJointIds();
        result = validator.validate(problem);
    }

    private Problem createSimpleSupportedBeamWithDuplicateJointIds() {
        Joint rollerJoint = createRollerJoint("joint");
        Joint pinnedJoint = createPinnedJoint("joint");
        Beam beam = createBeam("beam");
        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(rollerJoint, beam);
        builder.addBeam(pinnedJoint, beam);
        return builder.build();
    }

    private void thenErrorShouldContainIdUniquenessError() {
        assertThat(result.getDescription(), equalTo(ID_UNIQUENESS_ERROR));
    }

    private void whenCalledWithTwoSeparateSimplySupportedBeams() {
        Problem problem = createTwoSeparateSimpleSupportedBeams();
        result = validator.validate(problem);
    }

    private Problem createTwoSeparateSimpleSupportedBeams() {
        Joint rollerJoint1 = createRollerJoint("roller joint 1");
        Joint rollerJoint2 = createRollerJoint("roller joint 2");
        Joint pinnedJoint1 = createPinnedJoint("pinned joint 1");
        Joint pinnedJoint2 = createPinnedJoint("pinned joint 2");
        Beam beam1 = createBeam("beam 1");
        Beam beam2 = createBeam("beam 2");
        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(rollerJoint1, beam1);
        builder.addBeam(pinnedJoint1, beam1);
        builder.addBeam(rollerJoint2, beam2);
        builder.addBeam(pinnedJoint2, beam2);
        return builder.build();
    }

    private void thenErrorShouldContainStructuralContinuityError() {
        assertThat(result.getDescription(), equalTo(STRUCTURAL_CONTINUITY_ERROR));
    }

}
