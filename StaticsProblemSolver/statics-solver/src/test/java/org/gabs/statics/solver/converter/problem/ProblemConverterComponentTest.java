package org.gabs.statics.solver.converter.problem;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.problem.*;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.Effect;
import org.gabs.statics.solver.model.Element;
import org.gabs.statics.solver.model.Problem;
import org.gabs.statics.solver.model.ReferenceFrame;
import org.junit.Test;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class ProblemConverterComponentTest {

    private static final int DEFAULT_PRECISION = 12;
    private static final double SQRT2DIV2 = Math.sqrt(2) / 2.0d;

    private Converter<org.gabs.statics.model.problem.Problem, Problem> converter;
    private Joint rollerJoint1;
    private Joint rollerJoint2;
    private Joint pinnedJoint;
    private Joint hingeJoint;
    private Beam beam1;
    private Beam beam2;
    private ConcentratedForce concentratedForce;
    private Moment moment;
    private Problem result;

    @Test
    public void testConvertSimplySupportedBeam() {
        givenProblemConverter();
        whenCalledWithSimplySupportedBeam();
        thenResultHasCorrectLoadMappingForSimplySupportedBeam();
        thenResultHasCorrectStructureSimplySupportedBeam();
        thenResultHasDefaultPrecision();
    }

    @Test
    public void testConvertGerbersBeam() {
        givenProblemConverter();
        whenCalledWithGerbersBeam();
        thenResultHasCorrectLoadMappingForGerbersBeam();
        thenResultHasCorrectStructureForGerbersBeam();
        thenResultHasDefaultPrecision();
    }

    @Test
    public void testConvertSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        givenProblemConverter();
        whenCalledWithSimplySupportedBeamWithJointsInLocalReferenceFrame();
        thenResultHasCorrectLoadMappingForSimplySupportedBeam();
        thenResultHasCorrectStructureForSimplySupportedBeamWithJointsInLocalReferenceFrame();
        thenResultHasDefaultPrecision();
    }

    @Test
    public void testConvertSimplySupportedBeamWithCustomPrecision() {
        givenProblemConverter();
        whenCalledWithSimplySupportedBeamWithCustomPrecision();
        thenResultHasCorrectLoadMappingForSimplySupportedBeam();
        thenResultHasCorrectStructureSimplySupportedBeam();
        thenResultHasCustomPrecision();
    }

    private void givenProblemConverter() {
        Injector injector = Guice.createInjector(new ProblemConverterModule());
        converter = injector.getInstance(Key.get(new TypeLiteral<Converter<org.gabs.statics.model.problem.Problem, Problem>>() {}));
    }

    private void whenCalledWithSimplySupportedBeam() {
        result = converter.convert(createSimplySupportedBeam());
    }

    private org.gabs.statics.model.problem.Problem createSimplySupportedBeam() {
        concentratedForce = new ConcentratedForce(new Vector(0, 1, 0), new Vector(-2, -2, 0));
        moment = new Moment(new Vector(0, 0, 5));
        rollerJoint1 = createRollerJointForSimplySupportedBeam();
        pinnedJoint = createPinnedJointForSimplySupportedBeam();
        beam1 = createBeamForSimplySupportedBeam();

        org.gabs.statics.model.problem.Problem.Builder builder = new org.gabs.statics.model.problem.Problem.Builder();
        builder.addBeam(rollerJoint1, beam1);
        builder.addBeam(pinnedJoint, beam1);
        return builder.build();
    }

    private Joint createRollerJointForSimplySupportedBeam() {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints rollerSupport = new Constraints.Builder().constrainTx().build();
        Joint.Builder builder = new Joint.Builder("roller joint", new Vector(0, 0, 0), hinge);
        builder.withSupport(rollerSupport);
        return builder.build();
    }

    private Joint createPinnedJointForSimplySupportedBeam() {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints pinnedSupport = new Constraints.Builder().constrainTx().constrainTy().build();
        Joint.Builder builder = new Joint.Builder("pinned joint", new Vector(0, 2, 0), hinge);
        builder.withSupport(pinnedSupport);
        return builder.build();
    }

    private Beam createBeamForSimplySupportedBeam() {
        Beam.Builder builder = new Beam.Builder("beam");
        builder.addConcentratedForce(concentratedForce);
        builder.addMoment(moment);
        return builder.build();
    }

    private void thenResultHasCorrectLoadMappingForSimplySupportedBeam() {
        Element convertedRollerJoint = new Element("roller joint", true);
        Element convertedPinnedJoint = new Element("pinned joint", true);
        Element convertedBeam = new Element("beam", false);
        Effect expectedConcentratedForce = new org.gabs.statics.solver.model.ConcentratedForce(new org.gabs.statics.solver.data.Vector(0, 1, 0),
                new org.gabs.statics.solver.data.Vector(-2, -2, 0));
        Effect expectedConcentratedMoment = new org.gabs.statics.solver.model.ConcentratedMoment(new org.gabs.statics.solver.data.Vector(0, 0, 5));
        assertThat(result.getLoad().keySet(), containsInAnyOrder(convertedRollerJoint, convertedPinnedJoint, convertedBeam));
        assertThat(result.getLoad().get(convertedRollerJoint), empty());
        assertThat(result.getLoad().get(convertedPinnedJoint), empty());
        assertThat(result.getLoad().get(convertedBeam), containsInAnyOrder(expectedConcentratedForce, expectedConcentratedMoment));
    }

    private void thenResultHasCorrectStructureSimplySupportedBeam() {
        Element convertedRollerJoint = new Element("roller joint", true);
        Element convertedPinnedJoint = new Element("pinned joint", true);
        Element convertedBeam = new Element("beam", false);
        Constraint expectedRollerXSupport = new Constraint("roller joint:ground", Dof.X, new org.gabs.statics.solver.data.Vector(0, 0, 0));
        Constraint expectedRollerX = new Constraint("roller joint:beam", Dof.X, new org.gabs.statics.solver.data.Vector(0, 0, 0));
        Constraint expectedRollerY = new Constraint("roller joint:beam", Dof.Y, new org.gabs.statics.solver.data.Vector(0, 0, 0));
        Constraint expectedPinnedXSupport = new Constraint("pinned joint:ground", Dof.X, new org.gabs.statics.solver.data.Vector(0, 2, 0));
        Constraint expectedPinnedYSupport = new Constraint("pinned joint:ground", Dof.Y, new org.gabs.statics.solver.data.Vector(0, 2, 0));
        Constraint expectedPinnedX = new Constraint("pinned joint:beam", Dof.X, new org.gabs.statics.solver.data.Vector(0, 2, 0));
        Constraint expectedPinnedY = new Constraint("pinned joint:beam", Dof.Y, new org.gabs.statics.solver.data.Vector(0, 2, 0));

        assertThat(result.getStructure().getConnectedElementsOf(convertedRollerJoint).size(), equalTo(2));
        assertThat(result.getStructure().getConstraintsBetween(convertedRollerJoint, Element.GROUND), contains(expectedRollerXSupport));
        assertThat(result.getStructure().getConstraintsBetween(convertedRollerJoint, convertedBeam), containsInAnyOrder(expectedRollerX,
                expectedRollerY));
        assertThat(result.getStructure().getConnectedElementsOf(convertedPinnedJoint).size(), equalTo(2));
        assertThat(result.getStructure().getConstraintsBetween(convertedPinnedJoint, Element.GROUND), containsInAnyOrder(expectedPinnedXSupport,
                expectedPinnedYSupport));
        assertThat(result.getStructure().getConstraintsBetween(convertedPinnedJoint, convertedBeam), containsInAnyOrder(expectedPinnedX,
                expectedPinnedY));
        assertThat(result.getStructure().getConnectedElementsOf(convertedBeam).size(), equalTo(2));
        assertThat(result.getStructure().getConstraintsBetween(convertedBeam, convertedRollerJoint), containsInAnyOrder(expectedRollerX,
                expectedRollerY));
        assertThat(result.getStructure().getConstraintsBetween(convertedBeam, convertedPinnedJoint), containsInAnyOrder(expectedPinnedX,
                expectedPinnedY));
    }

    private void thenResultHasDefaultPrecision() {
        assertThat(result.getPrecision(), equalTo(DEFAULT_PRECISION));
    }

    private void whenCalledWithGerbersBeam() {
        result = converter.convert(createGerbersBeam());
    }

    private org.gabs.statics.model.problem.Problem createGerbersBeam() {
        concentratedForce = new ConcentratedForce(new Vector(0, 1, 0), new Vector(-2, -2, 0));
        moment = new Moment(new Vector(0, 0, 5));
        rollerJoint1 = createRollerJoint1ForGerbersBeam();
        rollerJoint2 = createRollerJoint2ForGerbersBeam();
        pinnedJoint = createPinnedJointForGerbersBeam();
        hingeJoint = createHingeJointForGerbersBeam();
        beam1 = createBeam1ForGerbersBeam();
        beam2 = createBeam2ForGerbersBeam();

        org.gabs.statics.model.problem.Problem.Builder builder = new org.gabs.statics.model.problem.Problem.Builder();
        builder.addBeam(pinnedJoint, beam1);
        builder.addBeams(hingeJoint, beam1, beam2);
        builder.addBeam(rollerJoint1, beam2);
        builder.addBeam(rollerJoint2, beam2);
        return builder.build();
    }

    private Joint createRollerJoint1ForGerbersBeam() {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints rollerSupport = new Constraints.Builder().constrainTx().build();
        Joint.Builder builder = new Joint.Builder("roller joint 1", new Vector(0, 2, 0), hinge);
        builder.withSupport(rollerSupport);
        return builder.build();
    }

    private Joint createRollerJoint2ForGerbersBeam() {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints rollerSupport = new Constraints.Builder().constrainTx().build();
        Joint.Builder builder = new Joint.Builder("roller joint 2", new Vector(0, 3, 0), hinge);
        builder.withSupport(rollerSupport);
        return builder.build();
    }

    private Joint createPinnedJointForGerbersBeam() {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints pinnedSupport = new Constraints.Builder().constrainTx().constrainTy().build();
        Joint.Builder builder = new Joint.Builder("pinned joint", new Vector(0, 0, 0), hinge);
        builder.withSupport(pinnedSupport);
        return builder.build();
    }

    private Joint createHingeJointForGerbersBeam() {
        Constraints constraints = new Constraints.Builder().constrainTx().constrainTy().build();
        Joint.Builder builder = new Joint.Builder("hinge joint", new Vector(0, 1, 0), constraints);
        builder.addConcentratedForce(concentratedForce);
        return builder.build();
    }

    private Beam createBeam1ForGerbersBeam() {
        return new Beam.Builder("beam 1").build();
    }

    private Beam createBeam2ForGerbersBeam() {
        Beam.Builder builder = new Beam.Builder("beam 2");
        builder.addMoment(moment);
        return builder.build();
    }

    private void thenResultHasCorrectLoadMappingForGerbersBeam() {
        Element convertedRollerJoint1 = new Element("roller joint 1", true);
        Element convertedRollerJoint2 = new Element("roller joint 2", true);
        Element convertedPinnedJoint = new Element("pinned joint", true);
        Element convertedHingeJoint = new Element("hinge joint", true);
        Element convertedBeam1 = new Element("beam 1", false);
        Element convertedBeam2 = new Element("beam 2", false);
        Effect expectedConcentratedForce = new org.gabs.statics.solver.model.ConcentratedForce(new org.gabs.statics.solver.data.Vector(0, 1, 0),
                new org.gabs.statics.solver.data.Vector(-2, -2, 0));
        Effect expectedConcentratedMoment = new org.gabs.statics.solver.model.ConcentratedMoment(new org.gabs.statics.solver.data.Vector(0, 0, 5));
        assertThat(result.getLoad().keySet(), containsInAnyOrder(convertedPinnedJoint, convertedRollerJoint1, convertedRollerJoint2,
                convertedHingeJoint, convertedBeam1, convertedBeam2));
        assertThat(result.getLoad().get(convertedPinnedJoint), empty());
        assertThat(result.getLoad().get(convertedHingeJoint), contains(expectedConcentratedForce));
        assertThat(result.getLoad().get(convertedRollerJoint1), empty());
        assertThat(result.getLoad().get(convertedRollerJoint2), empty());
        assertThat(result.getLoad().get(convertedBeam1), empty());
        assertThat(result.getLoad().get(convertedBeam2), contains(expectedConcentratedMoment));
    }

    private void thenResultHasCorrectStructureForGerbersBeam() {
        Element convertedRollerJoint1 = new Element("roller joint 1", true);
        Element convertedRollerJoint2 = new Element("roller joint 2", true);
        Element convertedPinnedJoint = new Element("pinned joint", true);
        Element convertedHingeJoint = new Element("hinge joint", true);
        Element convertedBeam1 = new Element("beam 1", false);
        Element convertedBeam2 = new Element("beam 2", false);
        Constraint expectedPinnedXSupport = new Constraint("pinned joint:ground", Dof.X, new org.gabs.statics.solver.data.Vector(0, 0, 0));
        Constraint expectedPinnedYSupport = new Constraint("pinned joint:ground", Dof.Y, new org.gabs.statics.solver.data.Vector(0, 0, 0));
        Constraint expectedPinnedX = new Constraint("pinned joint:beam 1", Dof.X, new org.gabs.statics.solver.data.Vector(0, 0, 0));
        Constraint expectedPinnedY = new Constraint("pinned joint:beam 1", Dof.Y, new org.gabs.statics.solver.data.Vector(0, 0, 0));
        Constraint expectedHinge1X = new Constraint("hinge joint:beam 1", Dof.X, new org.gabs.statics.solver.data.Vector(0, 1, 0));
        Constraint expectedHinge1Y = new Constraint("hinge joint:beam 1", Dof.Y, new org.gabs.statics.solver.data.Vector(0, 1, 0));
        Constraint expectedHinge2X = new Constraint("hinge joint:beam 2", Dof.X, new org.gabs.statics.solver.data.Vector(0, 1, 0));
        Constraint expectedHinge2Y = new Constraint("hinge joint:beam 2", Dof.Y, new org.gabs.statics.solver.data.Vector(0, 1, 0));
        Constraint expectedRoller1XSupport = new Constraint("roller joint 1:ground", Dof.X, new org.gabs.statics.solver.data.Vector(0, 2, 0));
        Constraint expectedRoller1X = new Constraint("roller joint 1:beam 2", Dof.X, new org.gabs.statics.solver.data.Vector(0, 2, 0));
        Constraint expectedRoller1Y = new Constraint("roller joint 1:beam 2", Dof.Y, new org.gabs.statics.solver.data.Vector(0, 2, 0));
        Constraint expectedRoller2XSupport = new Constraint("roller joint 2:ground", Dof.X, new org.gabs.statics.solver.data.Vector(0, 3, 0));
        Constraint expectedRoller2X = new Constraint("roller joint 2:beam 2", Dof.X, new org.gabs.statics.solver.data.Vector(0, 3, 0));
        Constraint expectedRoller2Y = new Constraint("roller joint 2:beam 2", Dof.Y, new org.gabs.statics.solver.data.Vector(0, 3, 0));

        assertThat(result.getStructure().getConnectedElementsOf(convertedPinnedJoint).size(), equalTo(2));
        assertThat(result.getStructure().getConstraintsBetween(convertedPinnedJoint, Element.GROUND), containsInAnyOrder(expectedPinnedXSupport,
                expectedPinnedYSupport));
        assertThat(result.getStructure().getConstraintsBetween(convertedPinnedJoint, convertedBeam1), containsInAnyOrder(expectedPinnedX,
                expectedPinnedY));
        assertThat(result.getStructure().getConnectedElementsOf(convertedHingeJoint).size(), equalTo(2));
        assertThat(result.getStructure().getConstraintsBetween(convertedHingeJoint, convertedBeam1), containsInAnyOrder(expectedHinge1X,
                expectedHinge1Y));
        assertThat(result.getStructure().getConstraintsBetween(convertedHingeJoint, convertedBeam2), containsInAnyOrder(expectedHinge2X,
                expectedHinge2Y));
        assertThat(result.getStructure().getConnectedElementsOf(convertedRollerJoint1).size(), equalTo(2));
        assertThat(result.getStructure().getConstraintsBetween(convertedRollerJoint1, Element.GROUND), contains(expectedRoller1XSupport));
        assertThat(result.getStructure().getConstraintsBetween(convertedRollerJoint1, convertedBeam2), containsInAnyOrder(expectedRoller1X,
                expectedRoller1Y));
        assertThat(result.getStructure().getConnectedElementsOf(convertedRollerJoint2).size(), equalTo(2));
        assertThat(result.getStructure().getConstraintsBetween(convertedRollerJoint2, Element.GROUND), containsInAnyOrder(expectedRoller2XSupport));
        assertThat(result.getStructure().getConstraintsBetween(convertedRollerJoint2, convertedBeam2), containsInAnyOrder(expectedRoller2X,
                expectedRoller2Y));
        assertThat(result.getStructure().getConnectedElementsOf(convertedBeam1).size(), equalTo(2));
        assertThat(result.getStructure().getConstraintsBetween(convertedBeam1, convertedPinnedJoint), containsInAnyOrder(expectedPinnedX,
                expectedPinnedY));
        assertThat(result.getStructure().getConstraintsBetween(convertedBeam1, convertedHingeJoint), containsInAnyOrder(expectedHinge1X,
                expectedHinge1Y));
        assertThat(result.getStructure().getConnectedElementsOf(convertedBeam2).size(), equalTo(3));
        assertThat(result.getStructure().getConstraintsBetween(convertedBeam2, convertedHingeJoint), containsInAnyOrder(expectedHinge2X,
                expectedHinge2Y));
        assertThat(result.getStructure().getConstraintsBetween(convertedBeam2, convertedRollerJoint1), containsInAnyOrder(expectedRoller1X,
                expectedRoller1Y));
        assertThat(result.getStructure().getConstraintsBetween(convertedBeam2, convertedRollerJoint2), containsInAnyOrder(expectedRoller2X,
                expectedRoller2Y));
    }

    private void whenCalledWithSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        result = converter.convert(createSimplySupportedBeamWithJointsInLocalReferenceFrame());
    }

    private org.gabs.statics.model.problem.Problem createSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        concentratedForce = new ConcentratedForce(new Vector(0, 1, 0), new Vector(-2, -2, 0));
        moment = new Moment(new Vector(0, 0, 5));
        rollerJoint1 = createRollerJointForSimplySupportedBeamWithJointsInLocalReferenceFrame();
        pinnedJoint = createPinnedJointForSimplySupportedBeamWithJointsInLocalReferenceFrame();
        beam1 = createBeamForSimplySupportedBeamWithJointsInLocalReferenceFrame();

        org.gabs.statics.model.problem.Problem.Builder builder = new org.gabs.statics.model.problem.Problem.Builder();
        builder.addBeam(rollerJoint1, beam1);
        builder.addBeam(pinnedJoint, beam1);
        return builder.build();
    }

    private Joint createRollerJointForSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints rollerSupport = new Constraints.Builder().constrainTx().inReferenceFrame(new Vector(SQRT2DIV2, SQRT2DIV2, 0)).build();
        Joint.Builder builder = new Joint.Builder("roller joint", new Vector(0, 0, 0), hinge);
        builder.withSupport(rollerSupport);
        return builder.build();
    }

    private Joint createPinnedJointForSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints pinnedSupport = new Constraints.Builder().constrainTx().constrainTy().inReferenceFrame(new Vector(SQRT2DIV2, -SQRT2DIV2,
                0)).build();
        Joint.Builder builder = new Joint.Builder("pinned joint", new Vector(0, 2, 0), hinge);
        builder.withSupport(pinnedSupport);
        return builder.build();
    }

    private Beam createBeamForSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        Beam.Builder builder = new Beam.Builder("beam");
        builder.addConcentratedForce(concentratedForce);
        builder.addMoment(moment);
        return builder.build();
    }

    private void thenResultHasCorrectStructureForSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        ReferenceFrame rollerJointReferenceFrame = new ReferenceFrame(new org.gabs.statics.solver.data.Vector(SQRT2DIV2, SQRT2DIV2, 0),
                new org.gabs.statics.solver.data.Vector(-SQRT2DIV2, SQRT2DIV2, 0), new org.gabs.statics.solver.data.Vector(0, 0, 1));
        ReferenceFrame pinnedJointReferenceFrame = new ReferenceFrame(new org.gabs.statics.solver.data.Vector(SQRT2DIV2, -SQRT2DIV2, 0),
                new org.gabs.statics.solver.data.Vector(SQRT2DIV2, SQRT2DIV2, 0), new org.gabs.statics.solver.data.Vector(0, 0, 1));
        Element convertedRollerJoint = new Element("roller joint", true);
        Element convertedPinnedJoint = new Element("pinned joint", true);
        Element convertedBeam = new Element("beam", false);
        Constraint expectedRollerXSupport = new Constraint("roller joint:ground", Dof.X, rollerJointReferenceFrame,
                new org.gabs.statics.solver.data.Vector(0, 0, 0));
        Constraint expectedRollerX = new Constraint("roller joint:beam", Dof.X, new org.gabs.statics.solver.data.Vector(0, 0, 0));
        Constraint expectedRollerY = new Constraint("roller joint:beam", Dof.Y, new org.gabs.statics.solver.data.Vector(0, 0, 0));
        Constraint expectedPinnedXSupport = new Constraint("pinned joint:ground", Dof.X, pinnedJointReferenceFrame, new org.gabs.statics.solver.data.Vector(0, 2, 0));
        Constraint expectedPinnedYSupport = new Constraint("pinned joint:ground", Dof.Y, pinnedJointReferenceFrame, new org.gabs.statics.solver.data.Vector(0, 2, 0));
        Constraint expectedPinnedX = new Constraint("pinned joint:beam", Dof.X, new org.gabs.statics.solver.data.Vector(0, 2, 0));
        Constraint expectedPinnedY = new Constraint("pinned joint:beam", Dof.Y, new org.gabs.statics.solver.data.Vector(0, 2, 0));

        assertThat(result.getStructure().getConnectedElementsOf(convertedRollerJoint).size(), equalTo(2));
        assertThat(result.getStructure().getConstraintsBetween(convertedRollerJoint, Element.GROUND), contains(expectedRollerXSupport));
        assertThat(result.getStructure().getConstraintsBetween(convertedRollerJoint, convertedBeam), containsInAnyOrder(expectedRollerX,
                expectedRollerY));
        assertThat(result.getStructure().getConnectedElementsOf(convertedPinnedJoint).size(), equalTo(2));
        assertThat(result.getStructure().getConstraintsBetween(convertedPinnedJoint, Element.GROUND), containsInAnyOrder(expectedPinnedXSupport,
                expectedPinnedYSupport));
        assertThat(result.getStructure().getConstraintsBetween(convertedPinnedJoint, convertedBeam), containsInAnyOrder(expectedPinnedX,
                expectedPinnedY));
        assertThat(result.getStructure().getConnectedElementsOf(convertedBeam).size(), equalTo(2));
        assertThat(result.getStructure().getConstraintsBetween(convertedBeam, convertedRollerJoint), containsInAnyOrder(expectedRollerX,
                expectedRollerY));
        assertThat(result.getStructure().getConstraintsBetween(convertedBeam, convertedPinnedJoint), containsInAnyOrder(expectedPinnedX,
                expectedPinnedY));
    }

    private void whenCalledWithSimplySupportedBeamWithCustomPrecision() {
        result = converter.convert(createSimplySupportedBeamWithCustomPrecision());
    }

    private org.gabs.statics.model.problem.Problem createSimplySupportedBeamWithCustomPrecision() {
        concentratedForce = new ConcentratedForce(new Vector(0, 1, 0), new Vector(-2, -2, 0));
        moment = new Moment(new Vector(0, 0, 5));
        rollerJoint1 = createRollerJointForSimplySupportedBeam();
        pinnedJoint = createPinnedJointForSimplySupportedBeam();
        beam1 = createBeamForSimplySupportedBeam();

        org.gabs.statics.model.problem.Problem.Builder builder = new org.gabs.statics.model.problem.Problem.Builder();
        builder.addBeam(rollerJoint1, beam1);
        builder.addBeam(pinnedJoint, beam1);
        builder.withPrecision(2);
        return builder.build();
    }

    private void thenResultHasCustomPrecision() {
        assertThat(result.getPrecision(), equalTo(2));
    }

}
