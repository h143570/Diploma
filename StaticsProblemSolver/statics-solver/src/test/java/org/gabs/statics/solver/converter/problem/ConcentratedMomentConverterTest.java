package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.model.ConcentratedMoment;
import org.gabs.statics.solver.model.Effect;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConcentratedMomentConverterTest {

    private static final Vector MOMENT_VECTOR = mock(Vector.class);
    private static final org.gabs.statics.solver.data.Vector CONVERTED_MOMENT_VECTOR = mock(org.gabs.statics.solver.data.Vector.class);

    @InjectMocks private ConcentratedMomentConverter converter;
    @Mock private Converter<Vector, org.gabs.statics.solver.data.Vector> vectorConverter;
    @Mock private Moment moment;
    private Effect result;

    @Test
    public void testConvertResultShouldBeAConcentratedForceInstance() {
        whenCalledWithConcentratedMoment();
        thenResultShouldBeAConcentratedMomentInstance();
    }

    @Test
    public void testConvertMomentShouldReturnCorrectVector() {
        whenCalledWithConcentratedMoment();
        thenResultShouldHaveCorrectVector();
    }

    private void whenCalledWithConcentratedMoment() {
        when(moment.getVector()).thenReturn(MOMENT_VECTOR);
        when(vectorConverter.convert(MOMENT_VECTOR)).thenReturn(CONVERTED_MOMENT_VECTOR);
        result = converter.convert(moment);
    }

    private void thenResultShouldBeAConcentratedMomentInstance() {
        assertThat(result, instanceOf(ConcentratedMoment.class));
    }

    private void thenResultShouldHaveCorrectVector() {
        ConcentratedMoment resultMoment = (ConcentratedMoment) result;
        assertThat(resultMoment.getResultant(), equalTo(CONVERTED_MOMENT_VECTOR));

    }

}
