package org.gabs.statics.solver;

import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.model.result.Result;
import org.gabs.statics.solver.analyzer.ProblemAnalyzer;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Analysis;
import org.gabs.statics.solver.matrix.builder.MatrixBuilder;
import org.gabs.statics.solver.matrix.extractor.SolutionExtractor;
import org.gabs.statics.solver.matrix.reducer.RowReducer;
import org.gabs.statics.solver.model.Determinacy;
import org.gabs.statics.solver.registry.ConstraintRegistry;
import org.gabs.statics.solver.registry.ConstraintRegistryFactory;
import org.gabs.statics.solver.resultbuilder.ResultBuilder;
import org.gabs.statics.solver.data.Error;
import org.gabs.statics.solver.validator.ProblemValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * {@link ProblemSolver} implementation.
 */
@Singleton
class ProblemSolverImpl implements ProblemSolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProblemSolverImpl.class);

    private ConstraintRegistryFactory constraintRegistryFactory;
    private ProblemValidator problemValidator;
    private Converter<Problem, org.gabs.statics.solver.model.Problem> problemConverter;
    private MatrixBuilder matrixBuilder;
    private RowReducer rowReducer;
    private ProblemAnalyzer problemAnalyzer;
    private SolutionExtractor solutionExtractor;
    private ResultBuilder resultBuilder;

    @Override
    public Result solve(Problem problem) {
        Result result;

        long start = System.currentTimeMillis();
        LOGGER.debug("Started solving problem {}", problem);

        Error error = problemValidator.validate(problem);
        if (error != null) {
            result = resultBuilder.build(error.getDescription());
        } else {
            org.gabs.statics.solver.model.Problem internalProblem = problemConverter.convert(problem);
            ConstraintRegistry constraintRegistry = constraintRegistryFactory.createFrom(internalProblem.getStructure());
            double[][] systemOfEquations = matrixBuilder.build(internalProblem, constraintRegistry);
            rowReducer.reduce(systemOfEquations);
            Analysis analysis = problemAnalyzer.analyze(systemOfEquations);
            if (Determinacy.DETERMINATE == analysis.getProblemDeterminacy()) {
                double[] solution = solutionExtractor.extractSolution(systemOfEquations);
                result = resultBuilder.build(solution, analysis, internalProblem, constraintRegistry);
            } else {
                result = resultBuilder.build(analysis);
            }
        }

        LOGGER.debug("Solved problem in {} ms. Problem: {}. Result: {}.", (System.currentTimeMillis() - start), problem, result);

        return result;
    }

    @Inject
    public void setConstraintRegistryFactory(ConstraintRegistryFactory constraintRegistryFactory) {
        this.constraintRegistryFactory = constraintRegistryFactory;
    }

    @Inject
    public void setProblemValidator(ProblemValidator problemValidator) {
        this.problemValidator = problemValidator;
    }

    @Inject
    public void setProblemConverter(Converter<Problem, org.gabs.statics.solver.model.Problem> problemConverter) {
        this.problemConverter = problemConverter;
    }

    @Inject
    public void setMatrixBuilder(MatrixBuilder matrixBuilder) {
        this.matrixBuilder = matrixBuilder;
    }

    @Inject
    public void setRowReducer(RowReducer rowReducer) {
        this.rowReducer = rowReducer;
    }

    @Inject
    public void setProblemAnalyzer(ProblemAnalyzer problemAnalyzer) {
        this.problemAnalyzer = problemAnalyzer;
    }

    @Inject
    public void setSolutionExtractor(SolutionExtractor solutionExtractor) {
        this.solutionExtractor = solutionExtractor;
    }

    @Inject
    public void setResultBuilder(ResultBuilder resultBuilder) {
        this.resultBuilder = resultBuilder;
    }

}
