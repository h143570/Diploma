package org.gabs.statics.solver.resultbuilder.postprocessor;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Names;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.List;

public class ReactionPostProcessorModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ReactionMappingPostProcessor.class).to(ChainedReactionMappingPostProcessor.class);
        bind(ReactionMappingPostProcessor.class).annotatedWith(Names.named("round")).to(RoundingPostProcessor.class);
        bind(ReactionMappingPostProcessor.class).annotatedWith(Names.named("filter")).to(FilteringPostProcessor.class);
        bind(RoundingStrategy.class).to(RoundingStrategyImpl.class);
    }

    @Provides
    @Singleton
    private List<ReactionMappingPostProcessor> provideReactionMappingPostProcessors(
            @Named("round") ReactionMappingPostProcessor roundingReactionMappingPostProcessor,
            @Named("filter") ReactionMappingPostProcessor filteringReactionMappingPostProcessor) {
        return Arrays.asList(roundingReactionMappingPostProcessor, filteringReactionMappingPostProcessor);
    }

}
