/**
 * Provides classes and interfaces to compose the equilibrium equations of a structural element.
 */
package org.gabs.statics.solver.matrix.builder.processor;