package org.gabs.statics.solver.matrix.extractor;

/**
 * {@link SolutionExtractor} implementation.
 */
class SolutionExtractorImpl implements SolutionExtractor {

    @Override
    public double[] extractSolution(double[][] augmentedMatrix) {
        int n = augmentedMatrix[0].length - 1;
        double[] result = new double[n];
        for (int i = n - 1; i >= 0; i--) {
            result[i] = augmentedMatrix[i][n];
            for (int j = i + 1; j < n; j++) {
                result[i] -= augmentedMatrix[i][j] * result[j];
            }
            result[i] /= augmentedMatrix[i][i];
        }
        return result;
    }

}
