package org.gabs.statics.solver.analyzer;

import org.gabs.statics.solver.data.Analysis;
import org.gabs.statics.solver.model.Determinacy;

/**
 * {@link ProblemAnalyzer} implementation.
 */
class ProblemAnalyzerImpl implements ProblemAnalyzer {

    @Override
    public Analysis analyze(double[][] augmentedMatrix) {
        Determinacy structureDeterminacy;
        Determinacy problemDeterminacy;
        if (augmentedMatrix.length == augmentedMatrix[0].length - 1) {
            structureDeterminacy = Determinacy.DETERMINATE;
            problemDeterminacy = Determinacy.DETERMINATE;
        } else if (augmentedMatrix.length >= augmentedMatrix[0].length) {
            structureDeterminacy = Determinacy.OVERDETERMINATE;
            if (hasExcessSolutions(augmentedMatrix)) {
                problemDeterminacy = Determinacy.OVERDETERMINATE;
            } else {
                problemDeterminacy = Determinacy.DETERMINATE;
            }
        } else {
            structureDeterminacy = Determinacy.INDETERMINATE;
            problemDeterminacy = Determinacy.INDETERMINATE;
        }
        return new Analysis(structureDeterminacy, problemDeterminacy);
    }

    private boolean hasExcessSolutions(double[][] augmentedMatrix) {
        boolean result = false;
        int lastCol = augmentedMatrix[0].length - 1;
        for (int i = lastCol; i < augmentedMatrix.length; i++) {
            if (augmentedMatrix[i][lastCol] != 0) {
                result = true;
                break;
            }
        }
        return result;
    }

}
