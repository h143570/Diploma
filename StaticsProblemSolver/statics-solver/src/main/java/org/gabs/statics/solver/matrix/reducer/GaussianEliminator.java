package org.gabs.statics.solver.matrix.reducer;

import static java.lang.Math.abs;

/**
 * An implementation of {@link RowReducer} that uses Gaussian elimination to transform the given matrix. Uses partial pivoting to reduce the numerical instability of the operation.
 */
class GaussianEliminator implements RowReducer {

    private static final double EPSILON = 1.0E-10;

    @Override
    public void reduce(double[][] augmentedMatrix) {
        if (augmentedMatrix.length >= augmentedMatrix[0].length - 1) {
            for (int k = 0; k < augmentedMatrix[0].length - 1; k++) {
                int pivotIdx = findPivot(augmentedMatrix, k);
                if (abs(augmentedMatrix[pivotIdx][k]) < EPSILON) {
                    throw new ArithmeticException("Matrix is singular.");
                }
                swap(augmentedMatrix, k, pivotIdx);
                pivot(augmentedMatrix, k);
            }
        }
    }

    private int findPivot(double[][] mat, int column) {
        int idx = column;
        double max = 0;
        for (int i = column; i < mat.length; i++) {
            double abs = abs(mat[i][column]);
            if (abs > max) {
                max = abs;
                idx = i;
            }
        }
        return idx;
    }

    private void swap(double[][] mat, int row1, int row2) {
        if (row1 != row2) {
            double[] temp = mat[row1];
            mat[row1] = mat[row2];
            mat[row2] = temp;
        }
    }

    private void pivot(double[][] mat, int k) {
        for (int i = k + 1; i < mat.length; i++) {
            double multiplier = mat[i][k] / mat[k][k];
            for (int j = k; j < mat[0].length; j++) {
                mat[i][j] = mat[i][j] - mat[k][j] * multiplier;
            }
        }
    }

}
