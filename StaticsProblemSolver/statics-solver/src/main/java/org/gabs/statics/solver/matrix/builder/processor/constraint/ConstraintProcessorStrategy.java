package org.gabs.statics.solver.matrix.builder.processor.constraint;

import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;

import java.util.Map;

/**
 * Interface for computing the coefficient of an unknown, for a constrained degree of freedom.
 */
interface ConstraintProcessorStrategy {

    /**
     * Adds the coefficient of an unknown for a constrained degree of freedom.
     * @param constraint a constraint of the processed element
     * @param index row index of the unknown representing the constraint
     * @param joint {@code true} when the processed element is a joint, {@code false} otherwise
     * @param coefficients a map associating row vectors to the constrained degree of freedom of each equation
     */
    void process(Constraint constraint, int index, boolean joint, Map<Dof, double[]> coefficients);

}
