package org.gabs.statics.solver.data;

import org.gabs.statics.solver.model.Determinacy;

import java.util.Objects;

/**
 * Represents the result of the determinacy analysis of a statics problem. Contains structural and problem determinacy information.
 */
public class Analysis {

    private Determinacy structureDeterminacy;
    private Determinacy problemDeterminacy;

    public Analysis(Determinacy structureDeterminacy, Determinacy problemDeterminacy) {
        this.structureDeterminacy = structureDeterminacy;
        this.problemDeterminacy = problemDeterminacy;
    }

    @Override
    public String toString() {
        return "Analysis{" + "structureDeterminacy=" + structureDeterminacy + ", problemDeterminacy=" + problemDeterminacy + '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(structureDeterminacy, problemDeterminacy);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Analysis other = (Analysis) obj;
        return Objects.equals(this.structureDeterminacy, other.structureDeterminacy) && Objects.equals(this.problemDeterminacy,
                other.problemDeterminacy);
    }

    public Determinacy getStructureDeterminacy() {
        return structureDeterminacy;
    }

    public Determinacy getProblemDeterminacy() {
        return problemDeterminacy;
    }

}
