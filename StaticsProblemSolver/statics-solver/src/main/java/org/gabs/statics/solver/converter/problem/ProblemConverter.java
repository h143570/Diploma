package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.model.Effect;
import org.gabs.statics.solver.model.Element;
import org.gabs.statics.solver.model.Problem;
import org.gabs.statics.solver.model.Structure;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Map;

/**
 * A {@link Converter} implementation that converts {@code StaticsApi} {@link org.gabs.statics.model.problem.Problem Problem}s to internal {@link Problem} representation.
 */
class ProblemConverter implements Converter<org.gabs.statics.model.problem.Problem, Problem> {

    private ElementMappingBuilder elementMappingBuilder;
    private LoadMappingBuilder loadMappingBuilder;
    private StructureBuilder structureBuilder;

    @Override
    public Problem convert(org.gabs.statics.model.problem.Problem source) {
        Map<org.gabs.statics.model.problem.Element, Element> elementMapping = elementMappingBuilder.build(source);
        Map<Element, Collection<Effect>> load = loadMappingBuilder.build(source, elementMapping);
        Structure structure = structureBuilder.build(source, elementMapping);
        return new Problem(structure, load, source.getPrecision());
    }

    @Inject
    public void setElementMappingBuilder(ElementMappingBuilder elementMappingBuilder) {
        this.elementMappingBuilder = elementMappingBuilder;
    }

    @Inject
    public void setLoadMappingBuilder(LoadMappingBuilder loadMappingBuilder) {
        this.loadMappingBuilder = loadMappingBuilder;
    }

    @Inject
    public void setStructureBuilder(StructureBuilder structureBuilder) {
        this.structureBuilder = structureBuilder;
    }

}
