package org.gabs.statics.solver.model;

/**
 * An enumeration of possible statical determinacies.
 */
public enum Determinacy {

    DETERMINATE,
    INDETERMINATE,
    OVERDETERMINATE

}
