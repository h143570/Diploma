package org.gabs.statics.solver.resultbuilder.postprocessor;

import org.gabs.statics.model.result.Reaction;

import java.util.Collection;
import java.util.Map;

/**
 * Interface for post-processing reaction mappings.
 */
public interface ReactionMappingPostProcessor {

    /**
     * Post-processes the given reaction mapping.
     * @param reactions a reaction mapping associating reactions to structural element IDs
     * @param precision number of decimals
     */
    void postProcess(Map<String, Collection<Reaction>> reactions, int precision);

}
