/**
 * Defines classes and interfaces to reduce matrices to row echelon form.
 */
package org.gabs.statics.solver.matrix.reducer;