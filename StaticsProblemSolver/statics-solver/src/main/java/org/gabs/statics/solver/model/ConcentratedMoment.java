package org.gabs.statics.solver.model;

import org.gabs.statics.solver.data.Vector;

import java.util.Objects;

/**
 * Internal representation of a concentrated moment.
 */
public class ConcentratedMoment implements Effect {

    private final Vector vector;

    /**
     * Constructs a new {@link ConcentratedForce} instance with the given vector.
     * @param vector moment vector defining the direction and magnitude of the moment
     */
    public ConcentratedMoment(Vector vector) {
        this.vector = vector;
    }

    @Override
    public int hashCode() {
        return Objects.hash(vector);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final ConcentratedMoment other = (ConcentratedMoment) obj;
        return Objects.equals(this.vector, other.vector);
    }

    @Override
    public String toString() {
        return "ConcentratedMoment{" + "vector=" + vector + '}';
    }

    @Override
    public Vector getResultant() {
        return vector;
    }

}
