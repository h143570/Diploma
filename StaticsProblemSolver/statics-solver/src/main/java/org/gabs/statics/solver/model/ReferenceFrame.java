package org.gabs.statics.solver.model;

import org.gabs.statics.solver.data.Vector;

import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;

/**
 * Represents a left-handed Cartesian coordinate system.
 */
public class ReferenceFrame {

    public static final ReferenceFrame GLOBAL = new ReferenceFrame(Vector.I, Vector.J, Vector.K);

    private final Vector i;
    private final Vector j;
    private final Vector k;
    private final Map<Dof, Vector> axisMap;

    /**
     * Constructs a new {@link ReferenceFrame} instance with the given axes.
     * @param i unit vector {@code i}
     * @param j unit vector {@code j}
     * @param k unit vector {@code k}
     */
    public ReferenceFrame(Vector i, Vector j, Vector k) {
        this.i = i;
        this.j = j;
        this.k = k;
        this.axisMap = createAxisMap(i, j, k);
    }

    @Override
    public int hashCode() {
        return Objects.hash(i, j, k);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final ReferenceFrame other = (ReferenceFrame) obj;
        return Objects.equals(this.i, other.i) && Objects.equals(this.j, other.j) && Objects.equals(this.k, other.k);
    }

    @Override
    public String toString() {
        return "ReferenceFrame{" + "i=" + i + ", j=" + j + ", k=" + k + ", axisMap=" + axisMap + '}';
    }

    /**
     * Returns the unit vector for the direction of the given displacement.
     * @param displacement defines the direction
     * @return the unit vector for the direction of the given displacement
     */
    public Vector getAxis(Dof displacement) {
        return axisMap.get(displacement);
    }

    private Map<Dof, Vector> createAxisMap(Vector i, Vector j, Vector k) {
        Map<Dof, Vector> result = new EnumMap<>(Dof.class);
        result.put(Dof.X, i);
        result.put(Dof.Y, j);
        result.put(Dof.Z, k);
        result.put(Dof.XX, i);
        result.put(Dof.YY, j);
        result.put(Dof.ZZ, k);
        return result;
    }

    public Vector getI() {
        return i;
    }

    public Vector getJ() {
        return j;
    }

    public Vector getK() {
        return k;
    }

}
