package org.gabs.statics.solver.matrix.builder.processor.load;

import org.gabs.statics.solver.model.ConcentratedForce;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.Effect;

import java.util.Map;

/**
 * An implementation of the {@link LoadProcessorStrategy} interface that adds the effect of concentrated forces to the equilibrium equations.
 */
class ConcentratedForceProcessorStrategy implements LoadProcessorStrategy {

    @Override
    public void process(Effect load, Map<Dof, double[]> coefficients, boolean joint) {
        if (load instanceof ConcentratedForce) {
            ConcentratedForce force = (ConcentratedForce) load;
            addXProjectionToXConstant(force, coefficients);
            addYProjectionToYConstant(force, coefficients);
            if (!joint) {
                addMomentToZZConstant(force, coefficients);
            }
        }
    }

    private void addXProjectionToXConstant(ConcentratedForce force, Map<Dof, double[]> coefficients) {
        double[] xCoefficients = coefficients.get(Dof.X);
        xCoefficients[xCoefficients.length - 1] = xCoefficients[xCoefficients.length - 1] - force.getResultant().getX();
    }

    private void addYProjectionToYConstant(ConcentratedForce force, Map<Dof, double[]> coefficients) {
        double[] yCoefficients = coefficients.get(Dof.Y);
        yCoefficients[yCoefficients.length - 1] = yCoefficients[yCoefficients.length - 1] - force.getResultant().getY();
    }

    private void addMomentToZZConstant(ConcentratedForce force, Map<Dof, double[]> coefficients) {
        double[] zzCoefficients = coefficients.get(Dof.ZZ);
        zzCoefficients[zzCoefficients.length - 1] = zzCoefficients[zzCoefficients.length - 1] - force.getPointOfApplication().crossProduct(force
                .getResultant()).getZ();
    }

}
