/**
 * Provides the classes necessary to validate statics problems.
 */
package org.gabs.statics.solver.validator;