/**
 * Defines classes and interfaces to analyze the determinacy of a statics problem.
 */
package org.gabs.statics.solver.analyzer;