package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.solver.model.Effect;
import org.gabs.statics.solver.model.Element;

import java.util.Collection;
import java.util.Map;

/**
 * Load mapping builder interface. Implementations build a mapping of structural elements and the {@link Effect load} affecting them.
 */
interface LoadMappingBuilder {

    /**
     * Builds the mapping between structural elements and the load affecting them.
     * @param problem a statics problem
     * @param elementMapping mapping between the {@code StaticsApi} and the internal representation of structural elements
     * @return the mapping between structural elements and the load affecting them
     */
    Map<Element, Collection<Effect>> build(Problem problem, Map<org.gabs.statics.model.problem.Element, Element> elementMapping);

}
