/**
 * Defines classes and interfaces to transform a {@code StaticsApi} problem to internal representation.
 */
package org.gabs.statics.solver.converter.problem;