package org.gabs.statics.solver.resultbuilder;

import com.google.inject.AbstractModule;

public class ResultBuilderModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ResultBuilder.class).to(ResultBuilderImpl.class);
        bind(ReactionMappingBuilder.class).to(ReactionMappingBuilderImpl.class);
        bind(ConcentratedForceMergingStrategy.class).to(ConcentratedForceMergingStrategyImpl.class);
    }

}
