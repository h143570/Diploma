/**
 * Defines classes and interface to compute the effect of load.
 */
package org.gabs.statics.solver.matrix.builder.processor.load;