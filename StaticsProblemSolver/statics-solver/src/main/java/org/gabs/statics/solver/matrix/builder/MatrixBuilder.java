package org.gabs.statics.solver.matrix.builder;

import org.gabs.statics.solver.model.Problem;
import org.gabs.statics.solver.registry.ConstraintRegistry;

/**
 * Interface for building the matrix representation of a system of linear equations.
 */
public interface MatrixBuilder {

    /**
     * Builds a matrix representing the equilibrium equations the given statics problem is decomposed to.
     * @param problem a statics problem
     * @param constraintRegistry registry of constraints, associating constraints to the column indices of unknowns representing them
     * @return the matrix representation of the equilibrium equations
     */
    double[][] build(Problem problem, ConstraintRegistry constraintRegistry);

}
