/**
 * Defines constraint registry related classes and interfaces.
 */
package org.gabs.statics.solver.registry;