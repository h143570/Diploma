package org.gabs.statics.solver.model;

import org.gabs.statics.solver.data.Vector;

import java.util.Objects;

/**
 * Represent a constraint of a joint. The constraint may be defined in a local reference frame.
 */
public class Constraint {

    private final String id;
    private final Dof constrainedDisplacement;
    private final ReferenceFrame localReferenceFrame;
    private final Vector position;

    /**
     * Constructs a new {@link Constraint} instance with the given constrained displacement, and position in the global reference frame.
     * @param id constraint ID
     * @param constrainedDisplacement constrained displacement
     * @param position position of the constraint
     */
    public Constraint(String id, Dof constrainedDisplacement, Vector position) {
        this(id, constrainedDisplacement, ReferenceFrame.GLOBAL, position);
    }

    /**
     * Constructs a new {@link Constraint} instance with the given constrained displacement, reference frame, and position.
     * @param id constraint ID
     * @param constrainedDisplacement constrained displacement
     * @param localReferenceFrame reference frame the constraint is defined in
     * @param position position of the constraint
     */
    public Constraint(String id, Dof constrainedDisplacement, ReferenceFrame localReferenceFrame, Vector position) {
        this.id = id;
        this.constrainedDisplacement = constrainedDisplacement;
        this.localReferenceFrame = localReferenceFrame;
        this.position = position;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, constrainedDisplacement, localReferenceFrame, position);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Constraint other = (Constraint) obj;
        return Objects.equals(this.id, other.id) && Objects.equals(this.constrainedDisplacement, other.constrainedDisplacement) && Objects.equals
                (this.localReferenceFrame, other.localReferenceFrame) && Objects.equals(this.position, other.position);
    }

    @Override
    public String toString() {
        return "Constraint{" + "id='" + id + '\'' + ", constrainedDisplacement=" + constrainedDisplacement + ", " +
                "localReferenceFrame=" + localReferenceFrame + ", position=" + position + '}';
    }

    public String getId() {
        return id;
    }

    public Dof getConstrainedDisplacement() {
        return constrainedDisplacement;
    }

    public ReferenceFrame getLocalReferenceFrame() {
        return localReferenceFrame;
    }

    public Vector getPosition() {
        return position;
    }

}
