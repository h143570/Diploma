package org.gabs.statics.solver.resultbuilder;

import org.gabs.statics.model.common.ConcentratedForce;

import java.util.Collection;

/**
 * Interface for merging projections of a concentrated force.
 */
interface ConcentratedForceMergingStrategy {

    /**
     * Merges the projections of a concentrated force.
     * @param concentratedForces projections of a concentrated force
     * @return the concentrated force that is the sum of its projections
     */
    ConcentratedForce merge(Collection<ConcentratedForce> concentratedForces);

}
