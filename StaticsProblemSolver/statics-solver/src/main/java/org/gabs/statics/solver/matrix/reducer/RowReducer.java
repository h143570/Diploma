package org.gabs.statics.solver.matrix.reducer;

/**
 * Interface to reduce matrices to row echelon form.
 */
public interface RowReducer {

    /**
     * Transforms the given matrix to row echelon form.
     * @param augmentedMatrix the augmented matrix representing the system of equilibrium equations
     */
    void reduce(double[][] augmentedMatrix);

}
