package org.gabs.statics.solver.matrix.builder.processor.load;

import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.Effect;

import java.util.Map;

/**
 * Interface for adding the effect of different types of load to equilibrium equations.
 */

interface LoadProcessorStrategy {

    /**
     * Adds the effect of the given load to the equilibrium equations.
     * @param load load of the processed element
     * @param coefficients a map associating row vectors to the constrained degree of freedom of each equation
     * @param joint {@code true} when the processed element is a joint, {@code false} otherwise
     */
    void process(Effect load, Map<Dof, double[]> coefficients, boolean joint);

}
