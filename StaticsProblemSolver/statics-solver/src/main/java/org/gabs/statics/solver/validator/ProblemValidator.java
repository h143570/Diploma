package org.gabs.statics.solver.validator;

import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.solver.data.Error;

/**
 * Problem validator interface. Validates a statics problem, and returns an error instance when the problem is not valid. Returns {@code null} when
 * the problem is valid.
 */
public interface ProblemValidator {

    /**
     * Validates the given statics problem.
     * @param problem a problem to validate
     * @return an error when the problem is not valid, {@code null} otherwise
     */
    Error validate(Problem problem);

}
