package org.gabs.statics.solver.resultbuilder.postprocessor;

import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.model.result.Reaction;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * {@link RoundingStrategy} implementation.
 */
class RoundingStrategyImpl implements RoundingStrategy {

    @Override
    public Reaction round(Reaction reaction, int precision) {
        String source = reaction.getSource();
        ConcentratedForce concentratedForce = roundConcentratedForce(reaction.getConcentratedForce(), precision);
        Moment moment = roundConcentratedMoment(reaction.getMoment(), precision);
        return new Reaction(source, concentratedForce, moment);
    }

    private ConcentratedForce roundConcentratedForce(ConcentratedForce concentratedForce, int precision) {
        return concentratedForce != null ? new ConcentratedForce(concentratedForce.getPointOfApplication(), round(concentratedForce.getVector(),
                precision)) : null;
    }

    private Moment roundConcentratedMoment(Moment moment, int precision) {
        return moment != null ? new Moment(round(moment.getVector(), precision)) : null;
    }

    private Vector round(Vector vector, int precision) {
        return new Vector(round(vector.getX(), precision), round(vector.getY(), precision), round(vector.getZ(), precision));
    }

    private double round(double value, int precision) {
        return new BigDecimal(value).setScale(precision, RoundingMode.HALF_EVEN).doubleValue();
    }

}
