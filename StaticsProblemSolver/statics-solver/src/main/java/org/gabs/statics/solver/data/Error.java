package org.gabs.statics.solver.data;

import java.util.Objects;

/**
 * Represents a problem validation error. Wraps an error message, describing the problem.
 */
public class Error {

    private String description;

    public Error(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        return Objects.hash(description);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Error other = (Error) obj;
        return Objects.equals(this.description, other.description);
    }

    @Override
    public String toString() {
        return "Error{" + "description='" + description + '\'' + '}';
    }

    public boolean isEmpty() {
        return description == null;
    }

    public String getDescription() {
        return description;
    }

}
