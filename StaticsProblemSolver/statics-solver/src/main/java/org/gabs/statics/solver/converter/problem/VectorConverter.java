package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.common.Vector;
import org.gabs.statics.solver.converter.Converter;

/**
 * A {@link Converter} implementation that converts {@code StaticsApi} {@link Vector}s to internal {@link org.gabs.statics.solver.data.Vector Vector} representation.
 */
class VectorConverter implements Converter<Vector, org.gabs.statics.solver.data.Vector> {

    @Override
    public org.gabs.statics.solver.data.Vector convert(Vector source) {
        return new org.gabs.statics.solver.data.Vector(source.getX(), source.getY(), source.getZ());
    }

}
