package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Vector;
import org.gabs.statics.solver.model.ReferenceFrame;

import javax.inject.Inject;

/**
 * A {@link Converter} implementation that converts {@code StaticsApi} {@link org.gabs.statics.model.common.Vector Vector}s to internal {@link ReferenceFrame} representation.
 * The given vector denotes the {@code x} axis of the local, left handed reference frame.
 */
class ReferenceFrameConverter implements Converter<org.gabs.statics.model.common.Vector, ReferenceFrame> {

    private Converter<org.gabs.statics.model.common.Vector, Vector> vectorConverter;

    @Override
    public ReferenceFrame convert(org.gabs.statics.model.common.Vector source) {
        ReferenceFrame result = ReferenceFrame.GLOBAL;
        if (source != null) {
            Vector localI = vectorConverter.convert(source).normalize();
            if (!isGlobal(localI)) {
                Vector localJ = new Vector(-localI.getY(), localI.getX(), 0);
                result = new ReferenceFrame(localI, localJ, Vector.K);
            }
        }
        return result;
    }

    private boolean isGlobal(Vector localI) {
        return ReferenceFrame.GLOBAL.getI().equals(localI);
    }

    @Inject
    public void setVectorConverter(Converter<org.gabs.statics.model.common.Vector, Vector> vectorConverter) {
        this.vectorConverter = vectorConverter;
    }

}
