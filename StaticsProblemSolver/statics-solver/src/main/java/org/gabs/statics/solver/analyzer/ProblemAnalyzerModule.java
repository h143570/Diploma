package org.gabs.statics.solver.analyzer;

import com.google.inject.AbstractModule;

public class ProblemAnalyzerModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ProblemAnalyzer.class).to(ProblemAnalyzerImpl.class);
    }

}
