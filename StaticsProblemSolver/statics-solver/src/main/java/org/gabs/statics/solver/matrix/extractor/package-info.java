/**
 * Defines classes and interfaces to extract the solution of a system of equations from a given matrix.
 */
package org.gabs.statics.solver.matrix.extractor;