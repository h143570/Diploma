package org.gabs.statics.solver.resultbuilder.postprocessor;

import org.gabs.statics.model.result.Reaction;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * Implementation of the {@link ReactionMappingPostProcessor} interface that rounds scalar components of reactions to the given number of decimals.
 */
class RoundingPostProcessor implements ReactionMappingPostProcessor {

    private RoundingStrategy roundingStrategy;

    @Override
    public void postProcess(Map<String, Collection<Reaction>> reactions, int precision) {
        for (Map.Entry<String, Collection<Reaction>> entry : reactions.entrySet()) {
            entry.setValue(roundReactions(entry.getValue(), precision));
        }
    }

    private Collection<Reaction> roundReactions(Collection<Reaction> reactions, int precision) {
        Collection<Reaction> result = new ArrayList<>(reactions.size());
        for (Reaction reaction : reactions) {
            result.add(roundingStrategy.round(reaction, precision));
        }
        return result;
    }

    @Inject
    public void setRoundingStrategy(RoundingStrategy roundingStrategy) {
        this.roundingStrategy = roundingStrategy;
    }

}
