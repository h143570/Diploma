package org.gabs.statics.solver.resultbuilder;

import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.common.ConcentratedForce;

import java.util.Collection;

/**
 * {@link ConcentratedForceMergingStrategy} implementation.
 */
class ConcentratedForceMergingStrategyImpl implements ConcentratedForceMergingStrategy {

    @Override
    public ConcentratedForce merge(Collection<ConcentratedForce> concentratedForces) {
        ConcentratedForce result = null;
        if (!concentratedForces.isEmpty()) {
            if (concentratedForces.size() > 1) {
                double x = 0;
                double y = 0;
                double z = 0;
                for (ConcentratedForce force : concentratedForces) {
                    x += force.getVector().getX();
                    y += force.getVector().getY();
                    z += force.getVector().getZ();
                }
                Vector pointOfApplication = concentratedForces.iterator().next().getPointOfApplication();
                result = new ConcentratedForce(pointOfApplication, new Vector(x, y, z));
            } else {
                result = concentratedForces.iterator().next();
            }
        }
        return result;
    }

}
