package org.gabs.statics.solver.model;

import org.gabs.statics.model.result.Ground;

import java.util.Objects;

/**
 * Inner representation of a structural element.
 * {@link Element#GROUND} is a distinguished element that represents an immobile object that supports the structure.
 */
public class Element {

    public static final Element GROUND = new Element(Ground.getId(), false);

    private String id;
    private boolean joint;

    /**
     * Constructs a new {@link Element} instance with the given ID.
     * @param id a unique ID
     * @param joint {@code true} when the element represents a joint, {@code false} otherwise
     */
    public Element(String id, boolean joint) {
        this.id = id;
        this.joint = joint;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, joint);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Element other = (Element) obj;
        return Objects.equals(this.id, other.id) && Objects.equals(this.joint, other.joint);
    }

    @Override
    public String toString() {
        return "Element{" + "id='" + id + '\'' + ", joint=" + joint + '}';
    }

    public String getId() {
        return id;
    }

    public boolean isJoint() {
        return joint;
    }

}
