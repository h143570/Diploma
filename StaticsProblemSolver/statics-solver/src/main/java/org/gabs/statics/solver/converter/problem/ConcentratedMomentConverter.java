package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.common.Moment;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Vector;
import org.gabs.statics.solver.model.Effect;

import javax.inject.Inject;

/**
 * A {Converter} implementation that converts {@code StaticsApi} {@link Moment}s to internal {@link Effect} representation.
 */
class ConcentratedMomentConverter implements Converter<Moment, Effect> {

    private Converter<org.gabs.statics.model.common.Vector, Vector> vectorConverter;

    @Override
    public Effect convert(Moment source) {
        Vector momentVector = vectorConverter.convert(source.getVector());
        return new org.gabs.statics.solver.model.ConcentratedMoment(momentVector);
    }

    @Inject
    public void setVectorConverter(Converter<org.gabs.statics.model.common.Vector, Vector> vectorConverter) {
        this.vectorConverter = vectorConverter;
    }

}
