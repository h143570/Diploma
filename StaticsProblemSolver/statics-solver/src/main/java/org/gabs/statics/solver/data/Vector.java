package org.gabs.statics.solver.data;

import org.gabs.statics.solver.model.Dof;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;

/**
 * Represents a three dimensional vector.
 */
public class Vector {

    public static final Vector NULL = new Vector(0, 0, 0);
    public static final Vector I = new Vector(1, 0, 0);
    public static final Vector J = new Vector(0, 1, 0);
    public static final Vector K = new Vector(0, 0, 1);

    private static final Map<Dof, Integer> coordinateMap = createCoordinateMap();

    private final double[] coordinates;

    private boolean hasLength = false;
    private double length = 0;

    /**
     * Constructs a {@link Vector} instance with the given projections on {@code x}, {@code y} and {@code z} directions.
     * @param x the vector's projection on {@code x} direction
     * @param y the vector's projection on {@code y} direction
     * @param z the vector's projection on {@code z} direction
     */
    public Vector(double x, double y, double z) {
        this.coordinates = new double[]{x, y, z};
    }

    private static Map<Dof, Integer> createCoordinateMap() {
        Map<Dof, Integer> result = new EnumMap<>(Dof.class);
        result.put(Dof.X, 0);
        result.put(Dof.Y, 1);
        result.put(Dof.Z, 2);
        result.put(Dof.XX, 0);
        result.put(Dof.YY, 1);
        result.put(Dof.ZZ, 2);
        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinates[0], coordinates[1], coordinates[2]);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Vector other = (Vector) obj;
        return Arrays.equals(this.coordinates, other.coordinates);
    }

    @Override
    public String toString() {
        return "Vector{" + "coordinates=" + Arrays.toString(coordinates) + '}';
    }

    public double getX() {
        return coordinates[0];
    }

    public double getY() {
        return coordinates[1];
    }

    public double getZ() {
        return coordinates[2];
    }

    /**
     * Normalizes the vector. Returns this instance if the vector is already normalized, returns a new instance otherwise.
     * @return the normalized vector
     */
    public Vector normalize() {
        Vector result;
        double len = length();
        if (Double.compare(len, 0) <= 0) {
            result = NULL;
        } else if (Double.compare(len, 1) != 0) {
            len = 1 / len;
            double x = getX() * len;
            double y = getY() * len;
            double z = getZ() * len;
            result = new Vector(x, y, z);
        } else {
            result = this;
        }
        return result;
    }

    /**
     * Returns the cross product with the given vector.
     * @param v a vector
     * @return the cross product with the given vector
     */
    public Vector crossProduct(Vector v) {
        double x = getY() * v.getZ() - getZ() * v.getY();
        double y = getZ() * v.getX() - getX() * v.getZ();
        double z = getX() * v.getY() - getY() * v.getX();
        return new Vector(x, y, z);
    }

    /**
     * Multiplies with the given scalar.
     * @param multiplier a scalar
     * @return a new {@link Vector} instance
     */
    public Vector multiply(double multiplier) {
        return new Vector(getX() * multiplier, getY() * multiplier, getZ() * multiplier);
    }

    /**
     * Returns the projection of the vector on the given direction.
     * @param dof a {@link Dof} instance defining a direction
     * @return the projection of the vector on the given direction
     */
    public double getCoordinate(Dof dof) {
        return coordinates[coordinateMap.get(dof)];
    }

    /**
     * Returns the length of the vector.
     * @return the length of the vector
     */
    public double length() {
        if (!hasLength) {
            length = computeLength();
            hasLength = true;
        }
        return length;
    }

    private double computeLength() {
        return Math.sqrt(getX() * getX() + getY() * getY() + getZ() * getZ());
    }

}
