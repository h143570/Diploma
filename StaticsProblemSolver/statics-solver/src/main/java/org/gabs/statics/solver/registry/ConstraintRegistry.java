package org.gabs.statics.solver.registry;

import org.gabs.statics.solver.model.Constraint;

/**
 * Constraint registry interface. Provides methods to register and query constraints.
 */
public interface ConstraintRegistry {

    /**
     * Registers a constraint.
     * @param constraint a constraint
     */
    void register(Constraint constraint);

    /**
     * Returns the column index of the unknown that represents the given constraint in equilibrium equations.
     * @param constraint a constraint
     * @return the column index of the unknown associated with the constraint
     */
    int getIndex(Constraint constraint);

    /**
     * Returns the size of the registry, that is, the number of registered constraints.
     * @return the size of the registry
     */
    int size();

}
