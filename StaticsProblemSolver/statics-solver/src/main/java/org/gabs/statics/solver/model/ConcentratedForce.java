package org.gabs.statics.solver.model;

import org.gabs.statics.solver.data.Vector;

import java.util.Objects;

/**
 * Internal representation of a concentrated force.
 */
public class ConcentratedForce implements Effect {

    private final Vector pointOfApplication;
    private final Vector vector;

    /**
     * Constructs a new {@link ConcentratedForce} instance with the given point of application and vector.
     * @param pointOfApplication point of application
     * @param vector force vector defining the direction and magnitude of the force
     */
    public ConcentratedForce(Vector pointOfApplication, Vector vector) {
        this.pointOfApplication = pointOfApplication;
        this.vector = vector;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pointOfApplication, vector);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final ConcentratedForce other = (ConcentratedForce) obj;
        return Objects.equals(this.pointOfApplication, other.pointOfApplication) && Objects.equals(this.vector, other.vector);
    }

    @Override
    public String toString() {
        return "ConcentratedForce{" + "pointOfApplication=" + pointOfApplication + ", vector=" + vector + '}';
    }

    @Override
    public Vector getResultant() {
        return vector;
    }

    public Vector getPointOfApplication() {
        return pointOfApplication;
    }

}
