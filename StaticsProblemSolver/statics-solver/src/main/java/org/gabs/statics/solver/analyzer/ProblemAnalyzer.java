package org.gabs.statics.solver.analyzer;

import org.gabs.statics.solver.data.Analysis;

/**
 * Interface for analyzing the determinacy of a statics problem.
 */
public interface ProblemAnalyzer {

    /**
     * Returns the result of the determinacy analysis, consisting of structural and problem determinacy.
     * @param augmentedMatrix the augmented matrix representing the system of equilibrium equations, in row echelon form
     * @return the result of the determinacy analysis
     */
    Analysis analyze(double[][] augmentedMatrix);

}
