package org.gabs.statics.solver.validator;

import org.gabs.statics.model.problem.Element;
import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.solver.data.Error;

import java.util.HashSet;
import java.util.Set;

/**
 * A {@link ProblemValidator} implementation that checks if every structural element ID is unique within the given problem.
 */
class IdUniquenessValidator implements ProblemValidator {

    private static final String ERROR = "Element IDs must be unique.";

    @Override
    public Error validate(Problem problem) {
        Error result = null;
        Set<String> ids = new HashSet<>();
        if (!hasUniqueIds(problem.getBeams(), ids)) {
            result = new Error(ERROR);
        }
        if (!hasUniqueIds(problem.getJoints(), ids)) {
            result = new Error(ERROR);
        }
        return result;
    }

    private boolean hasUniqueIds(Set<? extends Element> elements, Set<String> ids) {
        boolean result = true;
        for (Element element : elements) {
            if (!ids.add(element.getId())) {
                result = false;
                break;
            }
        }
        return result;
    }

}
