/**
 * Provides classes and interfaces to compute the coefficients of unknowns representing constraints.
 */
package org.gabs.statics.solver.matrix.builder.processor.constraint;