package org.gabs.statics.solver.validator;

import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.solver.data.Error;

import javax.inject.Inject;
import java.util.List;

/**
 * A {@link ProblemValidator} implementation that validates the given problem using a list of validators.
 * Validation halts on the first error.
 */
class ChainedProblemValidator implements ProblemValidator {

    private List<ProblemValidator> validators;

    @Override
    public Error validate(Problem problem) {
        Error result = null;
        for (ProblemValidator validator : validators) {
            Error error = validator.validate(problem);
            if (error != null) {
                result = error;
                break;
            }
        }
        return result;
    }

    @Inject
    public void setValidators(List<ProblemValidator> validators) {
        this.validators = validators;
    }

}
