package org.gabs.statics.solver.matrix.builder.processor.constraint;

import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;

import java.util.Map;

/**
 * Implementation of the {@link ConstraintProcessorStrategy} interface that processes constraints on the direction of X translation, defined in a local reference frame.
 */
class LocalXConstraintProcessorStrategy implements ConstraintProcessorStrategy {

    @Override
    public void process(Constraint constraint, int index, boolean joint, Map<Dof, double[]> coefficients) {
        if (constraint.getConstrainedDisplacement().isLinear()) {
            double projectionOnX = constraint.getLocalReferenceFrame().getAxis(constraint.getConstrainedDisplacement()).getCoordinate(Dof.X);
            double coefficient = joint ? projectionOnX : -projectionOnX;
            coefficients.get(Dof.X)[index] = coefficient;
        }
    }

}
