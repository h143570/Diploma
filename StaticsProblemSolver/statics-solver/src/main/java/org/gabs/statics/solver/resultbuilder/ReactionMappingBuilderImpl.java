package org.gabs.statics.solver.resultbuilder;

import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.model.result.Reaction;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Pair;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Problem;
import org.gabs.statics.solver.model.Structure;
import org.gabs.statics.solver.registry.ConstraintRegistry;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * {@link ReactionMappingBuilder} implementation.
 */
class ReactionMappingBuilderImpl implements ReactionMappingBuilder {

    private Converter<Pair<Constraint, Double>, ConcentratedForce> concentratedForceConverter;
    private Converter<Pair<Constraint, Double>, Moment> concentratedMomentConverter;
    private ConcentratedForceMergingStrategy concentratedForceMergingStrategy;

    @Override
    public Map<String, Collection<Reaction>> build(double[] solution, Problem problem, ConstraintRegistry constraintRegistry) {
        Map<String, Collection<Reaction>> result = new HashMap<>(problem.getStructure().getElements().size(), 1);
        for (org.gabs.statics.solver.model.Element element : problem.getStructure().getElements()) {
            result.put(element.getId(), createReactions(solution, problem.getStructure(), element, constraintRegistry));
        }
        return result;
    }

    private Collection<Reaction> createReactions(double[] solution, Structure structure, org.gabs.statics.solver.model.Element element,
                                                 ConstraintRegistry constraintRegistry) {
        Collection<Reaction> result = new ArrayList<>(structure.getConnectedElementsOf(element).size());
        for (org.gabs.statics.solver.model.Element source : structure.getConnectedElementsOf(element)) {
            Collection<Constraint> constraints = structure.getConstraintsBetween(element, source);
            if (constraints != null) {
                result.add(createReaction(solution, source.getId(), constraints, constraintRegistry, element.isJoint()));
            }
        }
        return result;
    }

    private Reaction createReaction(double[] solution, String sourceId, Collection<Constraint> constraints, ConstraintRegistry constraintRegistry,
                                    boolean joint) {
        List<ConcentratedForce> forces = new ArrayList<>(2);
        Moment moment = null;
        for (Constraint constraint : constraints) {
            int idx = constraintRegistry.getIndex(constraint);
            double value = joint ? solution[idx] : -solution[idx];
            if (constraint.getConstrainedDisplacement().isLinear()) {
                forces.add(concentratedForceConverter.convert(new Pair<>(constraint, value)));
            } else {
                moment = concentratedMomentConverter.convert(new Pair<>(constraint, value));
            }
        }
        ConcentratedForce force = concentratedForceMergingStrategy.merge(forces);
        return new Reaction(sourceId, force, moment);
    }

    @Inject
    public void setConcentratedForceConverter(Converter<Pair<Constraint, Double>, ConcentratedForce> concentratedForceConverter) {
        this.concentratedForceConverter = concentratedForceConverter;
    }

    @Inject
    public void setConcentratedMomentConverter(Converter<Pair<Constraint, Double>, Moment> concentratedMomentConverter) {
        this.concentratedMomentConverter = concentratedMomentConverter;
    }

    @Inject
    public void setConcentratedForceMergingStrategy(ConcentratedForceMergingStrategy concentratedForceMergingStrategy) {
        this.concentratedForceMergingStrategy = concentratedForceMergingStrategy;
    }

}
