package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.problem.Element;
import org.gabs.statics.model.problem.Beam;
import org.gabs.statics.model.problem.Joint;
import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.solver.converter.Converter;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

/**
 * An {@link ElementMappingBuilder} implementation that converts the structural {@link Element}s of the given {@code StaticsApi} problem, and builds the mapping to the internal
 * {@link org.gabs.statics.solver.model.Element Element} representations.
 */
class ElementMappingBuilderImpl implements ElementMappingBuilder {

    private Converter<Element, org.gabs.statics.solver.model.Element> elementConverter;

    @Override
    public Map<Element, org.gabs.statics.solver.model.Element> build(Problem problem) {
        Map<Element, org.gabs.statics.solver.model.Element> result = new HashMap<>();
        for (Joint joint : problem.getJoints()) {
            result.put(joint, elementConverter.convert(joint));
        }
        for (Beam beam : problem.getBeams()) {
            result.put(beam, elementConverter.convert(beam));
        }
        return result;
    }

    @Inject
    public void setElementConverter(Converter<Element, org.gabs.statics.solver.model.Element> elementConverter) {
        this.elementConverter = elementConverter;
    }

}
