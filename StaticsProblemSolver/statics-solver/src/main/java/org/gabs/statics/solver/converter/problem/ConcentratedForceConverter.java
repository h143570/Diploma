package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Vector;
import org.gabs.statics.solver.model.Effect;

import javax.inject.Inject;

/**
 * A {@link Converter} implementation that converts {@code StaticsApi}
 * {@link ConcentratedForce}s to internal {@link Effect} representation.
 */
class ConcentratedForceConverter implements Converter<ConcentratedForce, Effect> {

    private Converter<org.gabs.statics.model.common.Vector, Vector> vectorConverter;

    @Override
    public Effect convert(ConcentratedForce source) {
        Vector pointOfApplication = vectorConverter.convert(source.getPointOfApplication());
        Vector forceVector = vectorConverter.convert(source.getVector());
        return new org.gabs.statics.solver.model.ConcentratedForce(pointOfApplication, forceVector);
    }

    @Inject
    public void setVectorConverter(Converter<org.gabs.statics.model.common.Vector, Vector> vectorConverter) {
        this.vectorConverter = vectorConverter;
    }

}
