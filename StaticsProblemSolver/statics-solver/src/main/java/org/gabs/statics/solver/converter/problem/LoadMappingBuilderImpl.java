package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.problem.*;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.model.Effect;
import org.gabs.statics.solver.model.Element;

import javax.inject.Inject;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A {@link LoadMappingBuilder} implementation that converts the loads of the given {@code StaticsApi} problem, and maps them to structural {@link Element}s.
 */
class LoadMappingBuilderImpl implements LoadMappingBuilder {

    private Converter<ConcentratedForce, Effect> forceConverter;
    private Converter<Moment, Effect> momentConverter;

    @Override
    public Map<Element, Collection<Effect>> build(Problem problem, Map<org.gabs.statics.model.problem.Element, Element> elementMapping) {
        Map<Element, Collection<Effect>> result = new HashMap<>();
        for (Joint joint : problem.getJoints()) {
            result.put(elementMapping.get(joint), getLoad(joint));
        }
        for (Beam beam : problem.getBeams()) {
            result.put(elementMapping.get(beam), getLoad(beam));
        }
        return result;
    }

    private Set<Effect> getLoad(org.gabs.statics.model.problem.Element element) {
        Set<Effect> result = new HashSet<>();
        if (element.getConcentratedForces() != null) {
            for (ConcentratedForce force : element.getConcentratedForces()) {
                result.add(forceConverter.convert(force));
            }
        }
        if (element.getMoments() != null) {
            for (Moment moment : element.getMoments()) {
                result.add(momentConverter.convert(moment));
            }
        }
        return result;
    }

    @Inject
    public void setForceConverter(Converter<ConcentratedForce, Effect> forceConverter) {
        this.forceConverter = forceConverter;
    }

    @Inject
    public void setMomentConverter(Converter<Moment, Effect> momentConverter) {
        this.momentConverter = momentConverter;
    }
}
