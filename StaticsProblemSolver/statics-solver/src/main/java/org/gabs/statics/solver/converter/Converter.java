package org.gabs.statics.solver.converter;

/**
 * Converter interface. Implementations convert objects of type {@code S} to type {@code T}.
 * @param <S> source type
 * @param <T> target type
 */
public interface Converter<S, T> {

    /**
     * Converts the source object to type {@code T}.
     * @param source an object to convert
     * @return the converted object
     */
    T convert(S source);

}
