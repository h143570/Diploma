package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.solver.model.Element;
import org.gabs.statics.solver.model.Structure;

import java.util.Map;

/**
 * Structure builder interface. Implementations compile the table representing the connections of structural elements,
 * used in the internal representation of statics {@link Problem}s.
 */
interface StructureBuilder {

    /**
     * Builds the internal representation of the structure defined in the given problem.
     * @param problem a statics problem
     * @param elementMapping mapping between the {@code StaticsApi} and the internal representation of structural elements
     * @return the table representing the connections of structural elements
     */
    Structure build(Problem problem, Map<org.gabs.statics.model.problem.Element, Element> elementMapping);

}
