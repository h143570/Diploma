package org.gabs.statics.solver.resultbuilder.postprocessor;

import org.gabs.statics.model.result.Reaction;

/**
 * Interface for rounding the scalar components of a reaction to the given number of decimals.
 */
interface RoundingStrategy {

    /**
     * Rounds the scalar components of a reaction to the given number of decimals.
     * @param reaction a reaction
     * @param precision number of decimals
     * @return a reaction with rounded scalars
     */
    Reaction round(Reaction reaction, int precision);

}
