package org.gabs.statics.solver.validator;

import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.solver.data.Error;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * A {@link ProblemValidator} implementation that checks if the structure is continuous, e.g. it doesn't contain separate sub-structures.
 */
class StructuralContinuityValidator implements ProblemValidator {

    private static final String ERROR = "Structure must be continuous.";

    @Override
    public Error validate(Problem problem) {
        Error result = null;
        Collection<Set<String>> groups = getGroups(problem);
        boolean keepMerging = true;
        while (keepMerging) {
            keepMerging = mergeContinuousGroups(groups);
        }
        if (groups.size() > 1) {
            result = new Error(ERROR);
        }
        return result;
    }

    private Collection<Set<String>> getGroups(Problem problem) {
        Collection<Set<String>> result = new HashSet<>(problem.getConnections().size(), 1);
        for (Set<String> group : problem.getConnections().values()) {
            result.add(new HashSet<>(group));
        }
        return result;
    }

    private boolean mergeContinuousGroups(Collection<Set<String>> groups) {
        boolean result = false;
        if (!groups.isEmpty()) {
            Iterator<Set<String>> iterator = groups.iterator();
            Set<String> firstGroup = iterator.next();
            while (iterator.hasNext()) {
                Set<String> group = iterator.next();
                if (intersects(firstGroup, group)) {
                    firstGroup.addAll(group);
                    iterator.remove();
                    result = true;
                }
            }
        }
        return result;
    }

    private boolean intersects(Set<String> set1, Set<String> set2) {
        Set<String> intersection = new HashSet<>(set1);
        intersection.retainAll(set2);
        return !intersection.isEmpty();
    }

}
