package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.solver.model.Element;

import java.util.Map;

/**
 * Element mapping builder interface. Implementations build a mapping of {@code StaticsApi} and internal elements,
 * used in the internal representation of statics {@link Problem}s.
 */
interface ElementMappingBuilder {

    /**
     * Builds the mapping between the {@code StaticsApi} and the internal representation of structural elements.
     * @param problem a statics problem
     * @return the mapping between the {@code StaticsApi} and the internal representation of structural elements
     */
    Map<org.gabs.statics.model.problem.Element, Element> build(Problem problem);

}
