/**
 * Provides collections and other classes and interfaces representing data and associations of data related to the process of solving statics
 * problems.
 */
package org.gabs.statics.solver.data;