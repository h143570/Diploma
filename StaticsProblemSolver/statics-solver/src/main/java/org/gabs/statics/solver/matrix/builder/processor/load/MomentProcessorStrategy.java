package org.gabs.statics.solver.matrix.builder.processor.load;

import org.gabs.statics.solver.model.ConcentratedMoment;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.Effect;

import java.util.Map;

/**
 * An implementation of the {@link LoadProcessorStrategy} interface that adds the effect of concentrated moments to the equilibrium equations.
 */
class MomentProcessorStrategy implements LoadProcessorStrategy {

    @Override
    public void process(Effect load, Map<Dof, double[]> coefficients, boolean joint) {
        if (load instanceof ConcentratedMoment) {
            ConcentratedMoment moment = (ConcentratedMoment) load;
            double[] zzCoefficients = coefficients.get(Dof.ZZ);
            zzCoefficients[zzCoefficients.length - 1] = zzCoefficients[zzCoefficients.length - 1] - moment.getResultant().getZ();
        }
    }
}
