package org.gabs.statics.solver.converter.problem;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.problem.*;
import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Pair;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Effect;
import org.gabs.statics.solver.model.Problem;
import org.gabs.statics.solver.model.ReferenceFrame;

import java.util.Collection;

public class ProblemConverterModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ElementMappingBuilder.class).to(ElementMappingBuilderImpl.class);
        bind(StructureBuilder.class).to(StructureBuilderImpl.class);
        bind(LoadMappingBuilder.class).to(LoadMappingBuilderImpl.class);
        bind(new TypeLiteral<Converter<ConcentratedForce, Effect>>() {}).to(ConcentratedForceConverter.class);
        bind(new TypeLiteral<Converter<Moment, Effect>>() {}).to(ConcentratedMomentConverter.class);
        bind(new TypeLiteral<Converter<Pair<Joint, Beam>, Collection<Constraint>>>() {}).to(ConstraintsConverter.class);
        bind(new TypeLiteral<Converter<Element, org.gabs.statics.solver.model.Element>>() {}).to(ElementConverter.class);
        bind(new TypeLiteral<Converter<org.gabs.statics.model.problem.Problem, Problem>>() {}).to(ProblemConverter.class);
        bind(new TypeLiteral<Converter<org.gabs.statics.model.common.Vector, ReferenceFrame>>() {}).to(ReferenceFrameConverter.class);
        bind(new TypeLiteral<Converter<Vector, org.gabs.statics.solver.data.Vector>>() {}).to(VectorConverter.class);
    }

}
