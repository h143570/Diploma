/**
 * Provides classes and interfaces that represent a statics problem.
 */
package org.gabs.statics.solver.model;