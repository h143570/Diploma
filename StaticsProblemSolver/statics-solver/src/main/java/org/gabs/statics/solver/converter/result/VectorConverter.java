package org.gabs.statics.solver.converter.result;

import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Vector;

/**
 * A {@link Converter} implementation that converts internal {@link Vector} representations {@code StaticsApi} {@link org.gabs.statics.model.common.Vector Vector}s.
 */
class VectorConverter implements Converter<Vector, org.gabs.statics.model.common.Vector> {

    @Override
    public org.gabs.statics.model.common.Vector convert(Vector source) {
        return new org.gabs.statics.model.common.Vector(source.getX(), source.getY(), source.getZ());
    }

}
