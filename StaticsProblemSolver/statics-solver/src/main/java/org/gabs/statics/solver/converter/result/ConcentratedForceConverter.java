package org.gabs.statics.solver.converter.result;

import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Pair;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;

import javax.inject.Inject;

/**
 * A {@link Converter} implementation that converts an internal representation of load to a {@code StaticsApi} {@link ConcentratedForce}.
 */
class ConcentratedForceConverter implements Converter<Pair<Constraint, Double>, ConcentratedForce> {

    private Converter<org.gabs.statics.solver.data.Vector, Vector> vectorConverter;

    @Override
    public ConcentratedForce convert(Pair<Constraint, Double> source) {
        return new ConcentratedForce(getPointOfApplication(source), getForceVector(source));
    }

    private Vector getPointOfApplication(Pair<Constraint, Double> source) {
        return vectorConverter.convert(source.getLeft().getPosition());
    }

    private Vector getForceVector(Pair<Constraint, Double> source) {
        Dof constrainedDisplacement = source.getLeft().getConstrainedDisplacement();
        org.gabs.statics.solver.data.Vector direction = source.getLeft().getLocalReferenceFrame().getAxis(constrainedDisplacement);
        return vectorConverter.convert(direction.multiply(source.getRight()));
    }

    @Inject
    public void setVectorConverter(Converter<org.gabs.statics.solver.data.Vector, Vector> vectorConverter) {
        this.vectorConverter = vectorConverter;
    }

}
