package org.gabs.statics.solver.matrix.extractor;

/**
 * Interface for extracting the solution of a system of equations from a given matrix, which is in row echelon form.
 */
public interface SolutionExtractor {

    /**
     * Extract the solution of the system of equations from the given matrix.
     * @param augmentedMatrix a matrix in row echelon form
     * @return the vector containing the solved value of unknowns
     */
    double[] extractSolution(double[][] augmentedMatrix);

}
