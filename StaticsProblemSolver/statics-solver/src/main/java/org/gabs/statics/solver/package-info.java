/**
 * Defines classes and interfaces to solve a statics problem.
 */
package org.gabs.statics.solver;