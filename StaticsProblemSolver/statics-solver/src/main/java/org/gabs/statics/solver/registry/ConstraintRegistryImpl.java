package org.gabs.statics.solver.registry;

import org.gabs.statics.solver.model.Constraint;

import java.util.HashMap;
import java.util.Map;

/**
 * {@link java.util.HashMap} backed implementation of the {@link ConstraintRegistry} interface.
 */
class ConstraintRegistryImpl implements ConstraintRegistry {

    private Map<Constraint, Integer> indices = new HashMap<>();

    @Override
    public void register(Constraint constraint) {
        Integer index = indices.get(constraint);
        if (index == null) {
            index = indices.size();
            indices.put(constraint, index);
        }
    }

    @Override
    public int getIndex(Constraint constraint) {
        return indices.get(constraint);
    }

    @Override
    public int size() {
        return indices.size();
    }

}
