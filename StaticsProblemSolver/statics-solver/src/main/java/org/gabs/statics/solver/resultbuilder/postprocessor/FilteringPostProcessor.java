package org.gabs.statics.solver.resultbuilder.postprocessor;

import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.result.Reaction;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/**
 * Implementation of the {@link ReactionMappingPostProcessor} interface that removes zero reactions from the reaction mapping.
 */
class FilteringPostProcessor implements ReactionMappingPostProcessor {

    @Override
    public void postProcess(Map<String, Collection<Reaction>> reactions, int precision) {
        for (Collection<Reaction> value : reactions.values()) {
            filter(value);
        }
    }

    private void filter(Collection<Reaction> reactions) {
        Iterator<Reaction> iterator = reactions.iterator();
        while (iterator.hasNext()) {
            if (hasNullResultants(iterator.next())) {
                iterator.remove();
            }
        }
    }

    private boolean hasNullResultants(Reaction reaction) {
        return hasNullForce(reaction) && hasNullMoment(reaction);
    }

    private boolean hasNullForce(Reaction reaction) {
        return reaction.getConcentratedForce() == null || Vector.NULL.equals(reaction.getConcentratedForce().getVector());
    }

    private boolean hasNullMoment(Reaction reaction) {
        return reaction.getMoment() == null || Vector.NULL.equals(reaction.getMoment().getVector());
    }

}
