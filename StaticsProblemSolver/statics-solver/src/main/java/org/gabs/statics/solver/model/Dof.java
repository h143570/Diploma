package org.gabs.statics.solver.model;

/**
 * Enumeration of possible degrees of freedom (translations and rotations) in three dimensions.
 */
public enum Dof {

    X(true),
    Y(true),
    Z(true),
    XX(false),
    YY(false),
    ZZ(false);

    private boolean isLinear;

    private Dof(boolean isLinear) {
        this.isLinear = isLinear;
    }

    /**
     * Returns {@code true} if the degree of freedom is associated with translation, {@code false} otherwise.
     * @return {@code true} if the degree of freedom is associated with translation, {@code false} otherwise
     */
    public boolean isLinear() {
        return isLinear;
    }

}
