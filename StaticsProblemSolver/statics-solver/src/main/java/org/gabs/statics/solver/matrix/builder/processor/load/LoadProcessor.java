package org.gabs.statics.solver.matrix.builder.processor.load;

import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.Effect;

import java.util.Map;

/**
 * Interface for computing the effect of load. Adds constants to the equilibrium equations.
 */
public interface LoadProcessor {

    /**
     * Computes the constants of the equilibrium equations from the load of the processed structural element. Adds the constant to the provided
     * {@code coefficients} map. Constants are the last elements of row vectors.
     * The map associates row vectors describing a linear equation, to a degree of freedom.
     * @param load load of the processed element
     * @param coefficients a map associating row vectors to the constrained degree of freedom of each equation
     * @param joint {@code true} when the processed element is a joint, {@code false} otherwise
     */
    void process(Effect load, Map<Dof, double[]> coefficients, boolean joint);

}
