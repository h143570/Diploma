package org.gabs.statics.solver;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.gabs.statics.solver.analyzer.ProblemAnalyzerModule;
import org.gabs.statics.solver.converter.problem.ProblemConverterModule;
import org.gabs.statics.solver.converter.result.ResultConverterModule;
import org.gabs.statics.solver.matrix.builder.MatrixBuilderModule;
import org.gabs.statics.solver.matrix.builder.processor.constraint.ConstraintProcessorModule;
import org.gabs.statics.solver.matrix.builder.processor.ElementProcessorModule;
import org.gabs.statics.solver.matrix.builder.processor.load.LoadProcessorModule;
import org.gabs.statics.solver.matrix.extractor.SolutionExtractorModule;
import org.gabs.statics.solver.matrix.reducer.RowReducerModule;
import org.gabs.statics.solver.registry.ConstraintRegistryModule;
import org.gabs.statics.solver.resultbuilder.ResultBuilderModule;
import org.gabs.statics.solver.resultbuilder.postprocessor.ReactionPostProcessorModule;
import org.gabs.statics.solver.validator.ProblemValidatorModule;

/**
 * {@link ProblemSolverFactory} implementation. The returned {@link ProblemSolver} instance is a singleton.
 */
public class ProblemSolverFactoryImpl implements ProblemSolverFactory {

    @Override
    public ProblemSolver create() {
        Injector injector = Guice.createInjector(new ProblemSolverModule(), new ProblemValidatorModule(), new ProblemConverterModule(),
                new ResultConverterModule(), new MatrixBuilderModule(), new ConstraintProcessorModule(), new LoadProcessorModule(),
                new ElementProcessorModule(), new RowReducerModule(), new SolutionExtractorModule(), new ConstraintRegistryModule(),
                new ResultBuilderModule(), new ReactionPostProcessorModule(), new ProblemAnalyzerModule());
        return injector.getInstance(ProblemSolver.class);
    }

}
