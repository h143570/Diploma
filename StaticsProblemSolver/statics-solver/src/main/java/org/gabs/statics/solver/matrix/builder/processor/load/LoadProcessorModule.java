package org.gabs.statics.solver.matrix.builder.processor.load;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;

import java.util.Arrays;
import java.util.List;

public class LoadProcessorModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(LoadProcessor.class).to(LoadProcessorImpl.class);
        bind(new TypeLiteral<List<LoadProcessorStrategy>>() {}).toInstance(Arrays.asList(new ConcentratedForceProcessorStrategy(),
                new MomentProcessorStrategy()));
    }

}
