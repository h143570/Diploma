/**
 * Defines classes and interfaces to post-process a reaction mapping.
 */
package org.gabs.statics.solver.resultbuilder.postprocessor;