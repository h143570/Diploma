package org.gabs.statics.model.problem;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * Represents a statics problem. Defines joints and beams the structure consists of, and the connections between them. Joint and beam IDs must be
 * unique. Connections are defined as a map that associates beam and joint IDs.
 * The precision of the solution (number of decimals) is defined explicitly by the {@code precision} field. Default precision is 12.
 * Use {@link Problem.Builder} to instantiate.
 */
public class Problem {

    private static final int DEFAULT_PRECISION = 12;

    private Set<Joint> joints;
    private Set<Beam> beams;
    private Map<String, Set<String>> connections;
    private Integer precision;

    private Problem(Builder builder) {
        joints = builder.joints;
        beams = builder.beams;
        connections = builder.connections;
        precision = builder.precision;
    }

    @Override
    public int hashCode() {
        return Objects.hash(joints, beams, connections, precision);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Problem other = (Problem) obj;
        return Objects.equals(this.joints, other.joints) && Objects.equals(this.beams, other.beams) && Objects.equals(this.connections,
                other.connections) && Objects.equals(this.precision, other.precision);
    }

    @Override
    public String toString() {
        return "Problem{" + "joints=" + joints + ", beams=" + beams + ", connections=" + connections + ", precision=" + precision + '}';
    }

    public Set<Beam> getConnectedBeamsOf(String jointId) {
        Set<Beam> result = new HashSet<>();
        for (String connectedBeamId : connections.get(jointId)) {
            for (Beam beam : beams) {
                if (connectedBeamId.equals(beam.getId())) {
                    result.add(beam);
                }
            }
        }
        return result;
    }

    public Set<Joint> getJoints() {
        return joints;
    }

    public Set<Beam> getBeams() {
        return beams;
    }

    public Map<String, Set<String>> getConnections() {
        return connections;
    }

    public Integer getPrecision() {
        return precision;
    }

    /**
     * Static builder class to build a {@link Problem}.
     */
    public static class Builder {

        private Set<Joint> joints = new HashSet<>();
        private Set<Beam> beams = new HashSet<>();
        private Map<String, Set<String>> connections = new HashMap<>();
        private Integer precision = DEFAULT_PRECISION;

        public Builder() {
            // Default constructor.
        }

        /**
         * Connects a beam to a joint.
         * @param joint a joint
         * @param beam a beam
         * @return the builder instance
         */
        public Builder addBeam(Joint joint, Beam beam) {
            requireNonNull(joint, "Joint must not be null.");
            requireNonNull(beam, "Beam must not be null.");
            joints.add(joint);
            beams.add(beam);
            getConnectionsOfJoint(joint).add(beam.getId());
            return this;
        }

        /**
         * Connects a collection of beams to a joint.
         * @param joint a joint
         * @param beams a collection of beams
         * @return the builder instance
         */
        public Builder addBeams(Joint joint, Beam... beams) {
            requireNonNull(joint, "Joint must not be null.");
            requireNonNull(beams, "Beams must not be null.");
            joints.add(joint);
            Set<String> connectionsOfJoint = getConnectionsOfJoint(joint);
            for (Beam beam : beams) {
                requireNonNull(beam, "Beam must not be null.");
                this.beams.add(beam);
                connectionsOfJoint.add(beam.getId());
            }
            return this;
        }

        /**
         * Specifies the expected precision of the solution.
         * @param precision number of decimals
         * @return the builder instance
         */
        public Builder withPrecision(int precision) {
            this.precision = precision;
            return this;
        }

        /**
         * Builds a {@link Problem} instance.
         * @return a {@link Problem}
         */
        public Problem build() {
            return new Problem(this);
        }

        private Set<String> getConnectionsOfJoint(Joint joint) {
            Set<String> result = connections.get(joint.getId());
            if (result == null) {
                result = new HashSet<>();
                connections.put(joint.getId(), result);
            }
            return result;
        }

    }

}
