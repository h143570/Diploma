package org.gabs.statics.solver;

import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.model.result.Result;

/**
 * Problem solver interface. Implementations solve statics problems.
 * Use a {@link ProblemSolverFactory} to instantiate.
 */
public interface ProblemSolver {

    /**
     * Solves the given statics problem.
     * @param problem a statics problem
     * @return a result
     */
    Result solve(Problem problem);

}
