package org.gabs.statics.model.result;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Represents the result of a statics problem. A result consists of reactions applied on structural elements in case of a determinate problem;
 * structural and problem determinacy; an error message in case of an invalid problem.
 * Reactions are defined as a map that associates reactions to element IDs. External reaction source is identified by
 * {@link org.gabs.statics.model.result.Ground}.
 */
public class Result {

    private Map<String, Collection<Reaction>> reactions;
    private StaticalDeterminacy structureDeterminacy;
    private StaticalDeterminacy problemDeterminacy;
    private String error;

    /**
     * Creates a new {@link Result} instance.
     * @param reactions reactions applied on structural elements
     * @param structureDeterminacy structural determinacy
     * @param problemDeterminacy problem determinacy
     */
    public Result(Map<String, Collection<Reaction>> reactions, StaticalDeterminacy structureDeterminacy, StaticalDeterminacy problemDeterminacy) {
        this.reactions = requireNonNull(reactions, "Reactions must not be null.");
        this.structureDeterminacy = requireNonNull(structureDeterminacy, "Structure determinacy must not be null.");
        this.problemDeterminacy = requireNonNull(problemDeterminacy, "Problem determinacy must not be null.");
    }

    /**
     * Creates a new {@link Result} instance.
     * @param structureDeterminacy structural determinacy
     * @param problemDeterminacy problem determinacy
     */
    public Result(StaticalDeterminacy structureDeterminacy, StaticalDeterminacy problemDeterminacy) {
        this.structureDeterminacy = requireNonNull(structureDeterminacy, "Structure determinacy must not be null.");
        this.problemDeterminacy = requireNonNull(problemDeterminacy, "Problem determinacy must not be null.");
    }

    /**
     * Creates a new {@link Result} instance.
     * @param error an error message
     */
    public Result(String error) {
        this.error = requireNonNull(error, "Error must not be null.");
    }

    @Override
    public int hashCode() {
        return Objects.hash(reactions, structureDeterminacy, problemDeterminacy, error);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Result other = (Result) obj;
        return Objects.equals(this.reactions, other.reactions) && Objects.equals(this.structureDeterminacy,
                other.structureDeterminacy) && Objects.equals(this.problemDeterminacy, other.problemDeterminacy) && Objects.equals(this.error,
                other.error);
    }

    @Override
    public String toString() {
        return "Result{" + "reactions=" + reactions + ", structureDeterminacy=" + structureDeterminacy + ", " +
                "problemDeterminacy=" + problemDeterminacy + ", error='" + error + '\'' + '}';
    }

    public Map<String, Collection<Reaction>> getReactions() {
        return reactions;
    }

    public StaticalDeterminacy getStructureDeterminacy() {
        return structureDeterminacy;
    }

    public StaticalDeterminacy getProblemDeterminacy() {
        return problemDeterminacy;
    }

    public String getError() {
        return error;
    }

}
