package org.gabs.statics.model.problem;

import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.common.Moment;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * Represents a beam.
 * Use {@link Beam.Builder} to instantiate.
 */
public class Beam implements Element {

    private String id;
    private Set<ConcentratedForce> concentratedForces;
    private Set<Moment> moments;

    private Beam(Builder builder) {
        id = builder.id;
        concentratedForces = builder.concentratedForces;
        moments = builder.moments;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Set<ConcentratedForce> getConcentratedForces() {
        return concentratedForces;
    }

    @Override
    public Set<Moment> getMoments() {
        return moments;
    }

    @Override
    public String toString() {
        return "Beam{" + "id='" + id + '\'' + ", concentratedForces=" + concentratedForces + ", concentratedMoments=" + moments + '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, concentratedForces, moments);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Beam other = (Beam) obj;
        return Objects.equals(this.id, other.id) && Objects.equals(this.concentratedForces, other.concentratedForces) && Objects.equals(this
                .moments, other.moments);
    }

    public static class Builder {

        private String id;
        private Set<ConcentratedForce> concentratedForces = new HashSet<>();
        private Set<Moment> moments = new HashSet<>();

        /**
         * Creates a new builder with the given beam ID. ID must be unique within the structure.
         * @param id ID of the beam
         */
        public Builder(String id) {
            this.id = requireNonNull(id, "ID must not be null.");
        }

        /**
         * Places a concentrated force on the beam.
         * @param concentratedForce a concentrated force
         * @return the builder instance
         */
        public Builder addConcentratedForce(ConcentratedForce concentratedForce) {
            requireNonNull(concentratedForce, "Concentrated force must not be null.");
            this.concentratedForces.add(concentratedForce);
            return this;
        }

        /**
         * Places a moment on the beam.
         * @param moment a moment
         * @return the builder instance
         */
        public Builder addMoment(Moment moment) {
            requireNonNull(moment, "Moment must not be null.");
            this.moments.add(moment);
            return this;
        }

        /**
         * Builds a {@link Beam} instance.
         * @return a {@link Beam} instance
         */
        public Beam build() {
            return new Beam(this);
        }

    }

}
