package org.gabs.statics.model.common;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Represents a concentrated moment. Direction and magnitude of the moment is described by the {@code vector} property.
 */
public class Moment {

    private Vector vector;

    /**
     * Creates a new concentrated moment with the given vector.
     * @param vector the moment vector, describing the direction and magnitude of the moment
     */
    public Moment(Vector vector) {
        this.vector = requireNonNull(vector, "Vector must not be null.");
    }

    @Override
    public int hashCode() {
        return Objects.hash(vector);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Moment other = (Moment) obj;
        return Objects.equals(this.vector, other.vector);
    }

    @Override
    public String toString() {
        return "ConcentratedMoment{" + "vector=" + vector + '}';
    }

    public Vector getVector() {
        return vector;
    }

}
