package org.gabs.statics.solver;

/**
 * {@link ProblemSolver} factory interface.
 */
public interface ProblemSolverFactory {

    /**
     * Returns a {@link ProblemSolver} instance.
     * @return a {@link ProblemSolver} instance
     */
    ProblemSolver create();

}
