package org.gabs.statics.model.result;

/**
 * Represents the ground, supporting a structure.
 */
public class Ground {

    private static final String ID = "ground";

    public static String getId() {
        return ID;
    }

}
