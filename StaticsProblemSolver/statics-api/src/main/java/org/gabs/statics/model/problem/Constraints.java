package org.gabs.statics.model.problem;

import org.gabs.statics.model.common.Vector;

import java.util.Objects;

/**
 * Represents the constraints on a joint. Constraints may be defined in a local reference frame.
 * Use {@link Constraints.Builder} to instantiate.
 */
public class Constraints {

    private boolean constrainedTx;
    private boolean constrainedTy;
    private boolean constrainedRz;
    private Vector localReferenceFrameI;

    private Constraints(Builder builder) {
        this.constrainedTx = builder.constrainedTx;
        this.constrainedTy = builder.constrainedTy;
        this.constrainedRz = builder.constrainedRz;
        this.localReferenceFrameI = builder.localReferenceFrameI;
    }

    @Override
    public int hashCode() {
        return Objects.hash(constrainedTx, constrainedTy, constrainedRz, localReferenceFrameI);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Constraints other = (Constraints) obj;
        return Objects.equals(this.constrainedTx, other.constrainedTx) && Objects.equals(this.constrainedTy,
                other.constrainedTy) && Objects.equals(this.constrainedRz, other.constrainedRz) && Objects.equals(this.localReferenceFrameI,
                other.localReferenceFrameI);
    }

    @Override
    public String toString() {
        return "Constraints{" + "constrainedTx=" + constrainedTx + ", constrainedTy=" + constrainedTy + ", constrainedRz=" + constrainedRz + ", " +
                "localReferenceFrameI=" + localReferenceFrameI + '}';
    }

    public boolean isConstrainedTx() {
        return constrainedTx;
    }

    public boolean isConstrainedTy() {
        return constrainedTy;
    }

    public boolean isConstrainedRz() {
        return constrainedRz;
    }

    public Vector getLocalReferenceFrameI() {
        return localReferenceFrameI;
    }

    public static class Builder {

        private boolean constrainedTx = false;
        private boolean constrainedTy = false;
        private boolean constrainedRz = false;
        private Vector localReferenceFrameI;

        /**
         * Constrains translations in the direction of unit vector {@code i}.
         * @return the builder instance
         */
        public Builder constrainTx() {
            constrainedTx = true;
            return this;
        }

        /**
         * Constrains translations in the direction of unit vector {@code j}.
         * @return the builder instance
         */
        public Builder constrainTy() {
            constrainedTy = true;
            return this;
        }

        /**
         * Constrains rotations in the direction of unit vector {@code k}.
         * @return the builder instance
         */
        public Builder constrainRz() {
            constrainedRz = true;
            return this;
        }

        /**
         * Defines a local reference frame.
         * @param i direction of the {@code i} unit vector.
         * @return the builder instance
         */
        public Builder inReferenceFrame(Vector i) {
            localReferenceFrameI = i;
            return this;
        }

        /**
         * Builds a {@link Constraints} instance.
         * @return a {@link Constraints} instance
         */
        public Constraints build() {
            return new Constraints(this);
        }

    }

}
