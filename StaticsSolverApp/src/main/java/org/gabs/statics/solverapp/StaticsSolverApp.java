package org.gabs.statics.solverapp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.model.result.Result;
import org.gabs.statics.solver.ProblemSolver;
import org.gabs.statics.solver.ProblemSolverFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

public class StaticsSolverApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(StaticsSolverApp.class);

    private static ProblemSolver solver = new ProblemSolverFactoryImpl().create();
    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public static void main(String[] args) {
        checkArgs(args);

        Path source = FileSystems.getDefault().getPath(args[0]);
        Path target = FileSystems.getDefault().getPath(args[1]);
        try {
            String serializedProblem = new String(Files.readAllBytes(source));
            Problem problem = gson.fromJson(serializedProblem, Problem.class);
            Result result = solver.solve(problem);
            String serializedResult = gson.toJson(result);
            Files.write(target, serializedResult.getBytes());
            LOGGER.info("Created result at {}", target);
        } catch (IOException e) {
            LOGGER.error("Failed to read or write file.", e);
        } catch (Exception e) {
            LOGGER.error("An error occurred during solving a statics problem.", e);
        }
    }

    private static void checkArgs(String[] args) {
        LOGGER.debug("Application has been invoked with args {}", Arrays.toString(args));
        if (args.length != 2) {
            System.err.println("Expected two arguments.");
            System.out.println("Usage: statics-solver-app.jar problem_path result_path");
            System.exit(1);
        }
    }

}
